﻿using SQLExecutor.ViewModels;

namespace SQLExecutor
{
    /// <summary>
    /// View Model Locator class
    /// </summary>
    public class ViewModelLocator
    {
        // List of available view models to navigate: location navigation service in ctors of view models
        public SelectInterfaceViewModel SelectInterfaceViewModel => new SelectInterfaceViewModel(App.Navigation, App.MessageBoxService);

        public ICSISyteLineInterfaceViewModel ICSISyteLineInterfaceViewModel => new ICSISyteLineInterfaceViewModel(App.Navigation, App.MessageBoxService);

        public MSSQLServerViewModel MSSQLServerViewModel => new MSSQLServerViewModel(App.Navigation, App.MessageBoxService);

        public UserSettingsViewModel UserSettingsViewModel => new UserSettingsViewModel(App.Navigation, App.MessageBoxService);

        public AddEditDatabaseViewModel AddEditDatabaseViewModel => new AddEditDatabaseViewModel(App.Navigation, App.MessageBoxService);
    }
}