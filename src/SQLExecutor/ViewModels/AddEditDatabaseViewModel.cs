﻿using BaseMVVM;
using ErrorsAndExceptions;
using InterfaceToDatabase;
using LocalizationData;
using SQLExecutor.ComponentClasses;
using SQLExecutor.Services;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Tools;
using TreeViewItems;

namespace SQLExecutor.ViewModels
{
    /// <summary>
    /// View model of add and edit database page
    /// </summary>
    public class AddEditDatabaseViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IMessageBoxService _messageBoxService;
        private ResourceStrings _resourceStrings = new PublicStrings();
        private ObservableCollection<IInterfaceButton> _toolboxButtonList;
        private ObservableCollection<IBindableMenuItem> _menuItemsCollection;
        private IActionExpander _actionExpander;
        private IDatabaseModelService _databaseModelService;

        /// <summary>
        /// Ctor of add edit database view model
        /// </summary>
        /// <param name="navigationService"></param>
        /// <param name="messageBoxService"></param>
        public AddEditDatabaseViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
        {
            _navigationService = navigationService;
            _messageBoxService = messageBoxService;
            _databaseModelService = GetDatabaseModelService();
            InitViewModelBindingCommands();
            InitMenuItemsCollection();
            InitToolboxButtonList();
            InitActionExpander();
        }

        /// <summary>
        /// List of menus
        /// </summary>
        public ObservableCollection<IBindableMenuItem> MenuItemsCollection
        {
            get
            {
                if (_menuItemsCollection == null)
                    _menuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _menuItemsCollection;
            }
            set
            {
                Set(ref _menuItemsCollection, value);
            }
        }

        /// <summary>
        /// List of toolbox buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> ToolboxButtonList
        {
            get
            {
                if (_toolboxButtonList == null)
                    _toolboxButtonList = new ObservableCollection<IInterfaceButton>();
                return _toolboxButtonList;
            }
            set
            {
                Set(ref _toolboxButtonList, value);
            }
        }

        public IActionExpander ActionExpander
        {
            get
            {
                if (_actionExpander == null)
                    _actionExpander = new ActionExpander();
                return _actionExpander;
            }
            set
            {
                Set(ref _actionExpander, value);
            }
        }

        /// <summary>
        /// Selected database type string value in action expander
        /// </summary>
        public string SelectedDatabaseType
        {
            get
            {
                var databaseModelProperty = ActionExpander.StaticDatabaseModelPropertyList.Where(s => s.Property
                == DatabaseModelPropertyEnum.DatabaseType).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.DisplayedItem.Value;
                return null;
            }
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        public ICommand GoBackCommand { get; set; }

        public ICommand GoToUserSettingsPageCommand { get; set; }

        public ICommand ShowAboutCommand { get; set; }

        public ICommand ExitApplicationCommand { get; set; }

        public ICommand AddDatabaseCommand { get; set; }

        public ICommand CheckNewConnectionCommand { get; set; }

        public ICommand SaveChangesCommand { get; set; }

        public ICommand CheckEditedConnectionCommand { get; set; }

        /// <summary>
        /// Navigation go back command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoBack(object notUse)
        {
            if (_navigationService.CanGoBack())
                _navigationService.GoBack();
            else
                _navigationService.Navigate(ResourceStrings.SelectInterfacePageName);
        }

        /// <summary>
        /// GoTo User Settings page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToUserSettingsPage(object notUse)
        {
            _navigationService.Navigate(ResourceStrings.UserSettingsPageName);
        }

        /// <summary>
        /// Show about command
        /// </summary>
        /// <param name="notUse"></param>
        public void ShowAbout(object notUse)
        {
            var message = SQLExecutorInfo.GetSQLExecutorAboutInfo();
            _messageBoxService.DisplayMessage(message);
        }

        /// <summary>
        /// Exit application command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExitApplication(object notUse)
        {
            if (_messageBoxService.DisplayQuestion(ResourceStrings.ButtonExitQuestion))
                Application.Current.Shutdown();
        }

        /// <summary>
        /// Add database model command
        /// </summary>
        /// <param name="notUse"></param>
        public void AddDatabaseModel(object notUse)
        {
            if (!_databaseModelService.SaveDatabaseModel(ActionExpander))
                GetAndDisplayError();
            else
            {
                _messageBoxService.DisplayMessage(ResourceStrings.CorrectAddedDatabaseModel);

                GoToMSSQLInterfacePage();
            }
        }

        /// <summary>
        /// Check new connection command
        /// </summary>
        /// <param name="notUse"></param>
        public void CheckNewConnection(object notUse)
        {
            if (!_databaseModelService.CheckConnectionToDatabase(ActionExpander))
                GetAndDisplayError();
            else
                _messageBoxService.DisplayMessage(ResourceStrings.CorrectConnection);
        }

        /// <summary>
        /// Check edited connection command
        /// </summary>
        /// <param name="notUse"></param>
        public void CheckEditedConnection(object notUse)
        {
            if (!_databaseModelService.CheckConnectionToDatabase(ActionExpander))
                GetAndDisplayError();
            else
                _messageBoxService.DisplayMessage(ResourceStrings.CorrectConnection);
        }

        /// <summary>
        /// Save changes command
        /// </summary>
        /// <param name="notUse"></param>
        public void SaveChanges(object notUse)
        {
            if (!_databaseModelService.UpdateDatabaseModel(ActionExpander, EditDatabaseModelService.GetDatabaseModelToEdit().ID))
                GetAndDisplayError();
            else
            {
                _messageBoxService.DisplayMessage(ResourceStrings.UpdateDatabaseModelOk);
                GoToMSSQLInterfacePage();
            }
        }        

        /// <summary>
        /// Initialize all view model binding commands
        /// </summary>
        private void InitViewModelBindingCommands()
        {
            GoBackCommand = new RelayCommand<object>(GoBack);
            GoToUserSettingsPageCommand = new RelayCommand<object>(GoToUserSettingsPage);
            ShowAboutCommand = new RelayCommand<object>(ShowAbout);
            ExitApplicationCommand = new RelayCommand<object>(ExitApplication);
            AddDatabaseCommand = new RelayCommand<object>(AddDatabaseModel);
            CheckNewConnectionCommand = new RelayCommand<object>(CheckNewConnection);
            CheckEditedConnectionCommand = new RelayCommand<object>(CheckEditedConnection);
            SaveChangesCommand = new RelayCommand<object>(SaveChanges);
        }

        /// <summary>
        /// Navigate to MSSQL Interface page
        /// </summary>
        private void GoToMSSQLInterfacePage()
        {
            _navigationService.Navigate(ResourceStrings.MSSQLServerPageName);
        }

        /// <summary>
        /// Add go back interface button to toolbox button list
        /// </summary>
        private void AddGoBackInterfaceButton()
        {
            ToolboxButtonList.Add(GoBackInterfaceButton());
        }

        /// <summary>
        /// Return go back interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton GoBackInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonGoBack,
                Command = GoBackCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Initialize interface buttons in toolbox button list
        /// </summary>
        private void InitToolboxButtonList()
        {
            ClearToolboxButtonList();
            AddGoBackInterfaceButton();
        }

        /// <summary>
        /// Remove all elements from toolbox button list
        /// </summary>
        private void ClearToolboxButtonList()
        {
            if (ToolboxButtonList != null)
                ToolboxButtonList.Clear();
        }

        /// <summary>
        /// Initialize items in menu items collection
        /// </summary>
        private void InitMenuItemsCollection()
        {
            ClearMenuItemsCollection();
            AddSettingsMenuItemsList();
            AddHelpMenuItemsList();
        }

        /// <summary>
        /// Remove all elements from menu items collection
        /// </summary>
        private void ClearMenuItemsCollection()
        {
            if (MenuItemsCollection != null)
                MenuItemsCollection.Clear();
        }

        /// <summary>
        /// Add Settings Menu items list to menu items collection
        /// </summary>
        private void AddSettingsMenuItemsList()
        {
            MenuItemsCollection.Add(SettingsMenuItemsList());
        }

        /// <summary>
        /// Return Settings Menu item with items submenu list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem SettingsMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.SettingsMenuItem,
                Command = null,
                CommandParameters = null,
                SubMenuItems = SettingsMenuItems()
            };
        }

        /// <summary>
        /// Return Settings Menu items list
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> SettingsMenuItems()
        {
            var buttonList = new ObservableCollection<IBindableMenuItem>();
            buttonList.Add(UserSettingsMenuItem());

            return buttonList;
        }

        /// <summary>
        /// Return User Settings menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem UserSettingsMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonUserSettings,
                Command = GoToUserSettingsPageCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add Help Menu items list to menu items collection
        /// </summary>
        private void AddHelpMenuItemsList()
        {
            MenuItemsCollection.Add(HelpMenuItemsList());
        }

        /// <summary>
        /// Return Help Menu item with submenu items list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem HelpMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ItemHelp,
                SubMenuItems = HelpMenuItems()
            };
        }

        /// <summary>
        /// Return Help Menu items collection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> HelpMenuItems()
        {
            var helpMenuItems = new ObservableCollection<IBindableMenuItem>();
            helpMenuItems.Add(ShowAboutItem());
            helpMenuItems.Add(ExitMenuItem());

            return helpMenuItems;
        }

        /// <summary>
        /// Return show about menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ShowAboutItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAbout,
                Command = ShowAboutCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Return exit application menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ExitMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonExitApplication,
                Command = ExitApplicationCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Initialize action expander
        /// </summary>
        private void InitActionExpander()
        {
            if (EditDatabaseModelService.IsModelToEdit())
                InitEditDatabaseActionExpander();
            else
                InitAddDatabaseActionExpander();
        }

        /// <summary>
        /// Initialize add database action expander
        /// </summary>
        private void InitAddDatabaseActionExpander()
        {
            ActionExpander = new ActionExpander()
            {
                Header = ResourceStrings.AddDatabaseExpanderHeader,
                IsExpanded = true,
                StaticDatabaseModelPropertyList = AddDatabaseStaticDatabaseModelPropertyList(),
                DynamicDatabaseModelPropertyList = null,
                ActionButtonList = AddDatabaseInterfaceButtonList()
            };
        }

        /// <summary>
        /// Initialize edit dtbase action expander
        /// </summary>
        private void InitEditDatabaseActionExpander()
        {
            var actionExpander = new ActionExpander()
            {
                Header = ResourceStrings.EditDatabaseExpanderHeader,
                IsExpanded = true,
                StaticDatabaseModelPropertyList = EditDatabaseStaticDatabaseModelPropertyList(),
                DynamicDatabaseModelPropertyList = null,
                ActionButtonList = EditDatabaseInterfaceButtonList()
            };
            ActionExpander = actionExpander;
        }

        /// <summary>
        /// Return static property list with property from selected database
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IDatabaseModelProperty> EditDatabaseStaticDatabaseModelPropertyList()
        {
            ObservableCollection<IDatabaseModelProperty> databaseModeStaticPropertyList = new ObservableCollection<IDatabaseModelProperty>();

            var selectedDatabaseModel = EditDatabaseModelService.GetDatabaseModelToEdit();
            var selectedDatabaseType = selectedDatabaseModel.DatabaseType;
            var indexOfDatabaseTypeToSet = DatabaseTypeEnumList.GetIndexOfEnum(selectedDatabaseType);

            databaseModeStaticPropertyList.Add(DatabaseTypeProperty(indexOfDatabaseTypeToSet));

            return databaseModeStaticPropertyList;
        }

        /// <summary>
        /// Return add database static database model property collection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IDatabaseModelProperty> AddDatabaseStaticDatabaseModelPropertyList()
        {
            ObservableCollection<IDatabaseModelProperty> databaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();

            databaseModelPropertyList.Add(DatabaseTypeProperty());

            return databaseModelPropertyList;
        }

        /// <summary>
        /// Return database model database type property component class
        /// </summary>
        /// <returns></returns>
        private IDatabaseModelProperty DatabaseTypeProperty(int currentSelectedIndex = 0)
        {
            var databaseTypeProperty = new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.DatabaseType,
                PropertyName = ResourceStrings.PropertyDatabaseType,
                PropertyValue = string.Empty,
                IsComboBox = true,
                IsTextBox = false,
                IsPasswordTextBox = false,
                DisplayedItem = DatabaseTypeEnumList.GetEnumList().ElementAt(currentSelectedIndex),
                CurrentSelectedIndex = currentSelectedIndex,
                EnumList = DatabaseTypeEnumList.GetEnumList()
            };

            databaseTypeProperty.PropertyChanged += DatabaseModelProperty_PropertyChanged;

            return databaseTypeProperty;
        }

        /// <summary>
        /// Database model dynamic property based on database type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseModelProperty_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            DatabaseType selectedDatabaseType = GetSelectedDatabaseType();
            ActionExpander.DynamicDatabaseModelPropertyList = AddDatabaseModelPropertyListBasedOnType(selectedDatabaseType);
            SetDynamicPropertiesIfIsEditedDatabase(selectedDatabaseType);
        }

        /// <summary>
        /// Check if is edit database mode and set properties of edited database to view
        /// </summary>
        /// <param name="selectedDatabaseType"></param>
        private void SetDynamicPropertiesIfIsEditedDatabase(DatabaseType selectedDatabaseType)
        {
            if (EditDatabaseModelService.IsModelToEdit() &&
                EditDatabaseModelService.GetDatabaseModelToEdit().DatabaseType == selectedDatabaseType)
                SetEditedDatabaseDynamicProperties();
        }

        /// <summary>
        /// Set properties of edited database to view
        /// </summary>
        private void SetEditedDatabaseDynamicProperties()
        {
            var editedDatabase = EditDatabaseModelService.GetDatabaseModelToEdit();

            SetClientNameProperty(editedDatabase);
            SetDatabaseNameProperty(editedDatabase);
            SetDatabaseAliasNameProperty(editedDatabase);
            SetServerAddressProperty(editedDatabase);
            SetUserNameProperty(editedDatabase);
            SetPasswordProperty(editedDatabase);
            SetDatabasePathProperty(editedDatabase);
            SetConnectionTimeoutProperty(editedDatabase);
        }

        /// <summary>
        /// Set client name property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetClientNameProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.ClientName).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.ClientName;
        }

        /// <summary>
        /// Set database name property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetDatabaseNameProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.DatabaseName).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.DatabaseName;
        }

        /// <summary>
        /// Set database alias name property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetDatabaseAliasNameProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.DatabaseAliasName).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.DatabaseAliasName;
        }

        /// <summary>
        /// Set server address property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetServerAddressProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.ServerAddress).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.ServerAddress;
        }

        /// <summary>
        /// Set user name property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetUserNameProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.UserName).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.UserName;
        }

        /// <summary>
        /// Set password property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetPasswordProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.Password).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.Password;
        }

        /// <summary>
        /// Set database path property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetDatabasePathProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.DatabasePath).FirstOrDefault();
            if (databaseModelProperty != null)
                databaseModelProperty.PropertyValue = editedDatabase.DatabasePath;
        }

        /// <summary>
        /// Set connection timeout property from edited database
        /// </summary>
        /// <param name="editedDatabase"></param>
        private void SetConnectionTimeoutProperty(IDatabaseModel editedDatabase)
        {
            var databaseModelProperty = ActionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property == DatabaseModelPropertyEnum.ConnectionTimeout).FirstOrDefault();
            if (databaseModelProperty != null)
            {
                databaseModelProperty.CurrentSelectedIndex = ConnectionTimeoutEnumList.GetIndexOfEnum(editedDatabase.ConnectionTimeout);
                databaseModelProperty.DisplayedItem = ConnectionTimeoutEnumList.GetEnumList().ElementAt(databaseModelProperty.CurrentSelectedIndex);
            }
        }

        /// <summary>
        /// Return selected database type
        /// </summary>
        /// <returns></returns>
        private DatabaseType GetSelectedDatabaseType()
        {
            var selectedDatabaseType = (from DatabaseType dbTypes in Enum.GetValues(typeof(DatabaseType))
                                        where dbTypes.ToString() == SelectedDatabaseType
                                        select dbTypes).FirstOrDefault();
            return selectedDatabaseType;
        }

        /// <summary>
        /// Return database model property list based on selected database type
        /// </summary>
        private ObservableCollection<IDatabaseModelProperty> AddDatabaseModelPropertyListBasedOnType(DatabaseType selectedDatabaseType)
        {
            switch (selectedDatabaseType)
            {
                case DatabaseType.UNDEFINED:
                    return null;

                case DatabaseType.MSSQL:
                    return DatabaseModelPropertyService.GetMSSQLPropertyList();

                case DatabaseType.SSRS:
                    return DatabaseModelPropertyService.GetSSRSPropertyList();

                case DatabaseType.SQLite:
                    return DatabaseModelPropertyService.GetSQLitePropertyList();

                default:
                    return null;
            };
        }

        /// <summary>
        /// Return instance of interface button with content and command
        /// </summary>
        /// <param name="content"> Button content </param>
        /// <param name="buttonCommand"> Button command </param>
        /// <param name="commandParameters"> Optional command parameters </param>
        /// <returns></returns>
        private IInterfaceButton InterfaceButtonInstance(string content, ICommand buttonCommand, string commandParameters = null)
        {
            return new InterfaceButton()
            {
                Content = content,
                Command = buttonCommand,
                CommandParameters = commandParameters
            };
        }

        /// <summary>
        /// Return interface button list with add database, copy from selected and check connection buttons
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IInterfaceButton> AddDatabaseInterfaceButtonList()
        {
            ObservableCollection<IInterfaceButton> addDatabaseInterfaceButtonList = new ObservableCollection<IInterfaceButton>();

            addDatabaseInterfaceButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonAddDatabase, AddDatabaseCommand));
            addDatabaseInterfaceButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonCheckConnection, CheckNewConnectionCommand));

            return addDatabaseInterfaceButtonList;
        }

        /// <summary>
        /// Returns interface button list with edit and save edited database buttons
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IInterfaceButton> EditDatabaseInterfaceButtonList()
        {
            ObservableCollection<IInterfaceButton> editDatabaseInterfaceButtonList = new ObservableCollection<IInterfaceButton>();

            editDatabaseInterfaceButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonSaveChanges, SaveChangesCommand));
            editDatabaseInterfaceButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonCheckConnection, CheckEditedConnectionCommand));

            return editDatabaseInterfaceButtonList;
        }

        /// <summary>
        /// Return new instance of database model service class
        /// </summary>
        /// <returns></returns>
        private IDatabaseModelService GetDatabaseModelService()
        {
            return new DatabaseModelService();
        }

        /// <summary>
        /// Get error from error service and display
        /// Error string is cleared in service after displayed
        /// </summary>
        private void GetAndDisplayError()
        {
            var errorString = ErrorsAndExceptionsService.Instance.GetException();

            _messageBoxService.DisplayError(errorString);

            ErrorsAndExceptionsService.Instance.ClearErrorsAndExceptions();
        }
    }
}