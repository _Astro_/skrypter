﻿using BaseMVVM;
using ErrorsAndExceptions;
using LocalizationData;
using SQLExecutor.ComponentClasses;
using SQLExecutor.Services;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace SQLExecutor.ViewModels
{
    /// <summary>
    /// View model of ICSI SyteLine Interface Page
    /// </summary>
    public class ICSISyteLineInterfaceViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IMessageBoxService _messageBoxService;
        private ResourceStrings _resourceStrings = new PublicStrings();
        private ObservableCollection<IInterfaceButton> _toolboxButtonList;

        /// <summary>
        /// Ctor of ICSI SyteLine Interface view model class
        /// </summary>
        /// <param name="navigationService"></param>
        public ICSISyteLineInterfaceViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
        {
            _navigationService = navigationService;
            _messageBoxService = messageBoxService;
            InitViewModelBindingCommands();
            InitToolboxButtonList();
        }

        /// <summary>
        /// List of toolbox buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> ToolboxButtonList
        {
            get
            {
                if (_toolboxButtonList == null)
                    _toolboxButtonList = new ObservableCollection<IInterfaceButton>();
                return _toolboxButtonList;
            }
            set
            {
                Set(ref _toolboxButtonList, value);
            }
        }

        public ICommand GoBackCommand { get; set; }

        /// <summary>
        /// Navigation go back command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoBack(object notUse)
        {
            if (_navigationService.CanGoBack())
                _navigationService.GoBack();
            else
                _navigationService.Navigate(ResourceStrings.SelectInterfacePageName);
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        /// <summary>
        /// Initialize all view model binding commands
        /// </summary>
        private void InitViewModelBindingCommands()
        {
            GoBackCommand = new RelayCommand<object>(GoBack);
        }

        /// <summary>
        /// Add go back interface button to toolbox button list
        /// </summary>
        private void AddGoBackInterfaceButton()
        {
            ToolboxButtonList.Add(GoBackInterfaceButton());
        }

        /// <summary>
        /// Return go back interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton GoBackInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonGoBack,
                Command = GoBackCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Initialize interface buttons in toolbox button list
        /// </summary>
        private void InitToolboxButtonList()
        {
            ClearToolboxButtonList();
            AddGoBackInterfaceButton();
        }

        /// <summary>
        /// Remove all elements from toolbox button list
        /// </summary>
        private void ClearToolboxButtonList()
        {
            if (ToolboxButtonList != null)
                ToolboxButtonList.Clear();
        }
    }
}