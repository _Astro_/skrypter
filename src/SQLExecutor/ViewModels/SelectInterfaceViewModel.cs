﻿using BaseMVVM;
using ErrorsAndExceptions;
using LocalizationData;
using SQLExecutor.ComponentClasses;
using SQLExecutor.Services;
using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Tools;
using TreeViewItems;

namespace SQLExecutor.ViewModels
{
    /// <summary>
    /// View model of Select Interface Page
    /// </summary>
    public class SelectInterfaceViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IMessageBoxService _messageBoxService;
        private ResourceStrings _resourceStrings = new PublicStrings();
        private ObservableCollection<IInterfaceButton> _interfaceButtonList;
        private ObservableCollection<IBindableMenuItem> _menuItemsCollection;

        /// <summary>
        /// Ctor of Selected Interface View Model class
        /// </summary>
        /// <param name="navigationService"></param>
        public SelectInterfaceViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
        {
            _navigationService = navigationService;
            _messageBoxService = messageBoxService;
            InitViewModelBindingCommands();
            InitInterfaceButtonList();
            InitMenuItemsCollection();
        }

        /// <summary>
        /// List of interface buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> InterfaceButtonList
        {
            get
            {
                if (_interfaceButtonList == null)
                    _interfaceButtonList = new ObservableCollection<IInterfaceButton>();
                return _interfaceButtonList;
            }
            set
            {
                Set(ref _interfaceButtonList, value);
            }
        }

        /// <summary>
        /// List of menu expanders
        /// </summary>
        public ObservableCollection<IBindableMenuItem> MenuItemsCollection
        {
            get
            {
                if (_menuItemsCollection == null)
                    _menuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _menuItemsCollection;
            }
            set
            {
                Set(ref _menuItemsCollection, value);
            }
        }

        public ICommand GoToICSISyteLinePageCommand { get; set; }

        public ICommand GoToMSSQLServerPageCommand { get; set; }

        public ICommand ShowAboutCommand { get; set; }

        public ICommand ExitApplicationCommand { get; set; }

        public ICommand GoToUserSettingsCommand { get; set; }

        /// <summary>
        /// GoTo ICSI SyteLine interface page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToICSISyteLinePage(object notUse)
        {
            _navigationService.Navigate(ResourceStrings.ICSISyteLinePageName);
        }

        /// <summary>
        /// GoTo MS SQL Server interface page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToMSSQLServerPage(object notUse)
        {
            _navigationService.Navigate(ResourceStrings.MSSQLServerPageName);
        }

        /// <summary>
        /// Show about command
        /// </summary>
        /// <param name="notUse"></param>
        public void ShowAbout(object notUse)
        {
            var message = SQLExecutorInfo.GetSQLExecutorAboutInfo();
            _messageBoxService.DisplayMessage(message);
        }

        /// <summary>
        /// Exit application command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExitApplication(object notUse)
        {
            if (_messageBoxService.DisplayQuestion(ResourceStrings.ButtonExitQuestion))
                Application.Current.Shutdown();
        }

        /// <summary>
        /// GoTo User Settings page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToUserSettingsPage(object notUse)
        {
            _navigationService.Navigate(ResourceStrings.UserSettingsPageName);
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        /// <summary>
        /// Initialize all view model binding command
        /// </summary>
        private void InitViewModelBindingCommands()
        {
            GoToICSISyteLinePageCommand = new RelayCommand<object>(GoToICSISyteLinePage);
            GoToMSSQLServerPageCommand = new RelayCommand<object>(GoToMSSQLServerPage);
            ShowAboutCommand = new RelayCommand<object>(ShowAbout);
            ExitApplicationCommand = new RelayCommand<object>(ExitApplication);
            GoToUserSettingsCommand = new RelayCommand<object>(GoToUserSettingsPage);
        }

        /// <summary>
        /// Initialize buttons in interface button list
        /// </summary>
        private void InitInterfaceButtonList()
        {
            ClearInterfaceButtonList();
            //AddICSISyteLineInterfaceButton();
            AddMSSQLServerInterfaceButton();
            AddExitButton();
        }

        /// <summary>
        /// Initialize items in menu items collection
        /// </summary>
        private void InitMenuItemsCollection()
        {
            ClearMenuItemsCollection();
            AddSettingsMenuItems();
            AddHelpMenuItems();
        }

        /// <summary>
        /// Add ICSI SyteLine Interface button to interface button list
        /// </summary>
        private void AddICSISyteLineInterfaceButton()
        {
            InterfaceButtonList.Add(ICSISyteLineInterfaceButton());
        }

        /// <summary>
        /// Return ICSI SyteLine Interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton ICSISyteLineInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonICSISyteLineInterface,
                Command = GoToICSISyteLinePageCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add MS SQL Server Interface button to interface button list
        /// </summary>
        private void AddMSSQLServerInterfaceButton()
        {
            InterfaceButtonList.Add(MSSQLServerInterfaceButton());
        }

        /// <summary>
        /// Return MS SQL Server Interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton MSSQLServerInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonMSSQLInterface,
                Command = GoToMSSQLServerPageCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add exit aplication button to interface button list
        /// </summary>
        private void AddExitButton()
        {
            InterfaceButtonList.Add(ExitButton());
        }

        /// <summary>
        /// Return exit application interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton ExitButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonExitApplication,
                Command = ExitApplicationCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add Help Menu items to menu items collection
        /// </summary>
        private void AddHelpMenuItems()
        {
            MenuItemsCollection.Add(HelpMenuItemsList());
        }

        /// <summary>
        /// Return Help Menu items with submenu items list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem HelpMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ItemHelp,
                Command = null,
                CommandParameters = null,
                SubMenuItems = HelpMenuItems()
            };
        }

        /// <summary>
        /// Return Help Menu items collection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> HelpMenuItems()
        {
            var helpMenuItemsList = new ObservableCollection<IBindableMenuItem>();
            helpMenuItemsList.Add(ShowAboutItem());
            helpMenuItemsList.Add(ExitMenuItem());            

            return helpMenuItemsList;
        }

        /// <summary>
        /// Return show about menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ShowAboutItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAbout,
                Command = ShowAboutCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Return exit application menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ExitMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonExitApplication,
                Command = ExitApplicationCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add Settings Menu items to menu items collection
        /// </summary>
        private void AddSettingsMenuItems()
        {
            MenuItemsCollection.Add(SettingsMenuItemsList());
        }

        /// <summary>
        /// Return Settings Menu item with items submenu list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem SettingsMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.SettingsMenuItem,
                Command = null,
                CommandParameters = null,
                SubMenuItems = SettingsMenuItems()
            };
        }

        /// <summary>
        /// Return Settings Menu items list
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> SettingsMenuItems()
        {
            var buttonList = new ObservableCollection<IBindableMenuItem>();
            buttonList.Add(UserSettingsMenuItem());

            return buttonList;
        }

        /// <summary>
        /// Return User Settings menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem UserSettingsMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonUserSettings,
                Command = GoToUserSettingsCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Remove all elements from interface button list
        /// </summary>
        private void ClearInterfaceButtonList()
        {
            if (InterfaceButtonList != null)
                InterfaceButtonList.Clear();
        }

        /// <summary>
        /// Remove all elements from menu items collection
        /// </summary>
        private void ClearMenuItemsCollection()
        {
            if (MenuItemsCollection != null)
                MenuItemsCollection.Clear();
        }
    }
}