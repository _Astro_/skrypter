﻿using BaseMVVM;
using ErrorsAndExceptions;
using InterfaceToDatabase.SQLExecutor;
using LocalizationData;
using Scripts;
using SQLExecutor.ComponentClasses;
using SQLExecutor.Services;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Tools;
using TreeViewItems;

namespace SQLExecutor.ViewModels
{
    /// <summary>
    /// User Settings page view model
    /// </summary>
    public class UserSettingsViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IMessageBoxService _messageBoxService;
        private ISQLExecutorSettingsService _sqlExecutorSettingsService;
        private ISQLExecutorSettings _sqlExecutorSettings;
        private ResourceStrings _resourceStrings = new PublicStrings();
        private ObservableCollection<IInterfaceButton> _toolboxButtonList;
        private ObservableCollection<IBindableMenuItem> _menuItemsCollection;
        private ObservableCollection<string> _interfacePagesCollection;
        private ObservableCollection<string> _encodingCollection;

        /// <summary>
        /// Ctor of User Settings view model class
        /// </summary>
        /// <param name="navigationService"></param>
        public UserSettingsViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
        {
            _navigationService = navigationService;
            _messageBoxService = messageBoxService;
            _sqlExecutorSettingsService = GetSQLExecutorSettingsService();
            _sqlExecutorSettings = _sqlExecutorSettingsService.GetSQLExecutorSettings();
            InitViewModelBindingCommands();
            InitMenuItemsCollection();
            InitToolboxButtonList();
        }

        /// <summary>
        /// Settings of SQL Executor
        /// </summary>
        public ISQLExecutorSettings SQLExecutorSettings
        {
            get
            {
                if (_sqlExecutorSettings == null)
                    _sqlExecutorSettings = new SQLExecutorSettings();
                return _sqlExecutorSettings;
            }
            set
            {
                Set(ref _sqlExecutorSettings, value);
            }
        }

        /// <summary>
        /// List of menus
        /// </summary>
        public ObservableCollection<IBindableMenuItem> MenuItemsCollection
        {
            get
            {
                if (_menuItemsCollection == null)
                    _menuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _menuItemsCollection;
            }
            set
            {
                Set(ref _menuItemsCollection, value);
            }
        }

        /// <summary>
        /// List of toolbox buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> ToolboxButtonList
        {
            get
            {
                if (_toolboxButtonList == null)
                    _toolboxButtonList = new ObservableCollection<IInterfaceButton>();
                return _toolboxButtonList;
            }
            set
            {
                Set(ref _toolboxButtonList, value);
            }
        }

        public ObservableCollection<string> InterfacePagesCollection
        {
            get
            {
                _interfacePagesCollection = InterfacePagesEnumService.GetInterfacePagesCollection();
                return _interfacePagesCollection;
            }
            set
            {
                Set(ref _interfacePagesCollection, value);
            }
        }

        public ObservableCollection<string> EncodingCollection
        {
            get
            {
                _encodingCollection = EncodingStreamReaderService.GetEncodingStringCollectionForViewModel();
                return _encodingCollection;
            }
            set
            {
                Set(ref _encodingCollection, value);
            }
        }

        public ICommand GoBackCommand { get; set; }

        public ICommand ShowAboutCommand { get; set; }

        public ICommand ExitApplicationCommand { get; set; }

        public ICommand SaveSettingsChangesCommand { get; set; }

        /// <summary>
        /// Navigation go back command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoBack(object notUse)
        {
            if (_navigationService.CanGoBack())
                _navigationService.GoBack();
            else
                _navigationService.Navigate(ResourceStrings.SelectInterfacePageName);
        }

        /// <summary>
        /// Show about command
        /// </summary>
        /// <param name="notUse"></param>
        public void ShowAbout(object notUse)
        {
            var message = SQLExecutorInfo.GetSQLExecutorAboutInfo();
            _messageBoxService.DisplayMessage(message);
        }

        /// <summary>
        /// Exit application command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExitApplication(object notUse)
        {
            if (_messageBoxService.DisplayQuestion(ResourceStrings.ButtonExitQuestion))
                Application.Current.Shutdown();
        }

        /// <summary>
        /// Save changes of SQL Executor settings
        /// </summary>
        /// <param name="notUse"></param>
        public void SaveSettingsChanges(object notUse)
        {
            if (_sqlExecutorSettingsService.UpdateSQLExecutorSettings(SQLExecutorSettings))
                _messageBoxService.DisplayMessage(ResourceStrings.SQLExecutorSettingsUpdateSuccessful);
            else
                GetAndDisplayError();
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        /// <summary>
        /// Initialize all view model binding commands
        /// </summary>
        private void InitViewModelBindingCommands()
        {
            GoBackCommand = new RelayCommand<object>(GoBack);
            ShowAboutCommand = new RelayCommand<object>(ShowAbout);
            ExitApplicationCommand = new RelayCommand<object>(ExitApplication);
            SaveSettingsChangesCommand = new RelayCommand<object>(SaveSettingsChanges);
        }

        /// <summary>
        /// Add go back interface button to toolbox button list
        /// </summary>
        private void AddGoBackInterfaceButton()
        {
            ToolboxButtonList.Add(GoBackInterfaceButton());
        }

        /// <summary>
        /// Return go back interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton GoBackInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonGoBack,
                Command = GoBackCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add Help Menu items list to menu items collection
        /// </summary>
        private void AddHelpMenuItemsList()
        {
            MenuItemsCollection.Add(HelpMenuItemsList());
        }

        /// <summary>
        /// Return Help Menu item with submenu items list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem HelpMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ItemHelp,
                SubMenuItems = HelpMenuItems()
            };
        }

        /// <summary>
        /// Return Help Menu items collection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> HelpMenuItems()
        {
            var helpMenuItems = new ObservableCollection<IBindableMenuItem>();
            helpMenuItems.Add(ShowAboutItem());
            helpMenuItems.Add(ExitMenuItem());

            return helpMenuItems;
        }

        /// <summary>
        /// Return show about menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ShowAboutItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAbout,
                Command = ShowAboutCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Return exit application menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ExitMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonExitApplication,
                Command = ExitApplicationCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Initialize items in menu items collection
        /// </summary>
        private void InitMenuItemsCollection()
        {
            ClearMenuItemsCollection();
            AddHelpMenuItemsList();
        }

        /// <summary>
        /// Remove all elements from menu items collection
        /// </summary>
        private void ClearMenuItemsCollection()
        {
            if (MenuItemsCollection != null)
                MenuItemsCollection.Clear();
        }

        /// <summary>
        /// Initialize interface buttons in toolbox button list
        /// </summary>
        private void InitToolboxButtonList()
        {
            ClearToolboxButtonList();
            AddGoBackInterfaceButton();
        }

        /// <summary>
        /// Remove all elements from toolbox button list
        /// </summary>
        private void ClearToolboxButtonList()
        {
            if (ToolboxButtonList != null)
                ToolboxButtonList.Clear();
        }

        /// <summary>
        /// Get error from error service and display
        /// Error string is cleared in service after displayed
        /// </summary>
        private void GetAndDisplayError()
        {
            var errorString = ErrorsAndExceptionsService.Instance.GetException();

            _messageBoxService.DisplayError(errorString);

            ErrorsAndExceptionsService.Instance.ClearErrorsAndExceptions();
        }

        /// <summary>
        /// Return new SQLExecutor settings service instance
        /// </summary>
        /// <returns></returns>
        private ISQLExecutorSettingsService GetSQLExecutorSettingsService()
        {
            return new SQLExecutorSettingsService();
        }
    }
}