﻿using BaseClientData;
using BaseMVVM;
using BGProcessing;
using ErrorsAndExceptions;
using InterfaceToDatabase;
using LocalizationData;
using SQLExecutor.ComponentClasses;
using SQLExecutor.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tools;
using TreeViewItems;

namespace SQLExecutor.ViewModels
{
    /// <summary>
    /// View model of MS SQL Server Interface Page
    /// </summary>
    public class MSSQLServerViewModel : ObservableObject
    {
        private readonly INavigationService _navigationService;
        private readonly IMessageBoxService _messageBoxService;
        private ResourceStrings _resourceStrings = new PublicStrings();
        private ObservableCollection<IInterfaceButton> _toolboxButtonList;
        private ObservableCollection<IBindableMenuItem> _menuItemsCollection;
        private ObservableCollection<IBindableMenuItem> _scriptsContextMenuItemsCollection;
        private ObservableCollection<IBindableMenuItem> _reportsContextMenuItemsCollection;
        private ObservableCollection<IClientDatabaseInterface> _clientDatabaseInterfaceList;
        private ObservableCollection<IInterfaceButton> _clientDatabaseControlButtonList;
        private ObservableCollection<IClientDatabaseCollection> _clientScriptsDatabaseCollectionList;
        private ObservableCollection<IClientDatabaseCollection> _clientReportsDatabaseCollectionList;
        private ObservableCollection<ITreeViewItem> _scriptsTreeViewItemCollection;
        private ObservableCollection<ITreeViewItem> _reportsTreeViewItemCollection;
        private ObservableCollection<ITabTreeView> _tabTreeViewCollection;
        private ITabTreeView _selectedTabTreeView;
        private IDatabaseModelService _databaseModelService;
        private const int ONE_DATABASE = 1;

        /// <summary>
        /// Ctor of MS SQL Server Interface view model class
        /// </summary>
        /// <param name="navigationService"></param>
        public MSSQLServerViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
        {
            _navigationService = navigationService;
            _messageBoxService = messageBoxService;
            _databaseModelService = GetDatabaseModelService();
            InitViewModelBindingCommands();            
            InitMenuItemsCollection();
            InitToolboxButtonList();
            // Right site
            InitScriptsContextMenuItemsCollection();
            InitReportsContextMenuItemsCollection();
            InitTabTreeViewCollection();
            // Left site
            InitClientDatabaseControlButtonList();
            InitClientDatabaseCollectionList();
            InitClientDatabaseInterfaceList();
        }

        /// <summary>
        /// Menu
        /// </summary>
        public ObservableCollection<IBindableMenuItem> MenuItemsCollection
        {
            get
            {
                if (_menuItemsCollection == null)
                    _menuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _menuItemsCollection;
            }
            set
            {
                Set(ref _menuItemsCollection, value);
            }
        }

        /// <summary>
        /// List of toolbox buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> ToolboxButtonList
        {
            get
            {
                if (_toolboxButtonList == null)
                    _toolboxButtonList = new ObservableCollection<IInterfaceButton>();
                return _toolboxButtonList;
            }
            set
            {
                Set(ref _toolboxButtonList, value);
            }
        }

        /// <summary>
        /// List of client database interfaces
        /// </summary>
        public ObservableCollection<IClientDatabaseInterface> ClientDatabaseInterfaceList
        {
            get
            {
                if (_clientDatabaseInterfaceList == null)
                    _clientDatabaseInterfaceList = new ObservableCollection<IClientDatabaseInterface>();

                return _clientDatabaseInterfaceList;
            }
            set
            {
                Set(ref _clientDatabaseInterfaceList, value);
            }
        }

        /// <summary>
        /// Collection of client database control buttons
        /// </summary>
        public ObservableCollection<IInterfaceButton> ClientDatabaseControlButtonList
        {
            get
            {
                if (_clientDatabaseControlButtonList == null)
                    _clientDatabaseControlButtonList = new ObservableCollection<IInterfaceButton>();
                return _clientDatabaseControlButtonList;
            }
            set
            {
                Set(ref _clientDatabaseControlButtonList, value);
            }
        }        

        /// <summary>
        /// Collection of tabTreeView items
        /// </summary>
        public ObservableCollection<ITabTreeView> TabTreeViewCollection
        {
            get
            {
                if (_tabTreeViewCollection == null)
                    _tabTreeViewCollection = new ObservableCollection<ITabTreeView>();

                return _tabTreeViewCollection;
            }
            set
            {
                Set(ref _tabTreeViewCollection, value);
            }
        }

        /// <summary>
        /// Current selected tabTreeView
        /// </summary>
        public ITabTreeView SelectedTabTreeView
        {
            get
            {
                if (_selectedTabTreeView == null)
                    _selectedTabTreeView = new TabTreeView();

                return _selectedTabTreeView;
            }
            set
            {
                Set(ref _selectedTabTreeView, value);
            }
        }

        public ICommand GoBackCommand { get; set; }

        public ICommand GoToUserSettingsPageCommand { get; set; }

        public ICommand ShowAboutCommand { get; set; }

        public ICommand ExitApplicationCommand { get; set; }

        public ICommand SelectDeselectAllDatabaseCommand { get; set; }

        public ICommand ExpandCollapseAllDatabaseCommand { get; set; }

        public ICommand DeleteAllSelectedDatabaseCommand { get; set; }

        public ICommand GoToAddDatabasePageCommand { get; set; }

        public ICommand EditDatabaseCommand { get; set; }

        public ICommand ClearAllTreeViewItemsCommand { get; set; }

        public ICommand AddSelectedFilesCommand { get; set; }

        public ICommand AddSelectedFolderCommand { get; set; }

        public ICommand DeleteAllSelectedItemsCommand { get; set; }

        public ICommand CopyFromClipboardCommand { get; set; }

        public ICommand TreeViewDropCommand { get; set; }

        public ICommand ExpandTreeViewCommand { get; set; }

        public ICommand CollapseTreeViewCommand { get; set; }

        public ICommand SelectAllScriptsCommand { get; set; }

        public ICommand DeselectAllScriptsCommand { get; set; }

        public ICommand ReloadSelectedWithEncodingCommand { get; set; }

        public ICommand PreviewEditScriptCommand { get; set; }

        public ICommand ExecuteItemsCommand { get; set; }

        public ICommand ChangeSelectedTabTreeViewItemCommand { get; set; }

        /// <summary>
        /// Navigation go back command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoBack(object notUse)
        {
            if (_navigationService.CanGoBack())
                _navigationService.GoBack();
            else
                _navigationService.Navigate(ResourceStrings.SelectInterfacePageName);
        }

        /// <summary>
        /// GoTo User Settings page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToUserSettingsPage(object notUse)
        {
            _navigationService.Navigate(ResourceStrings.UserSettingsPageName);
        }

        /// <summary>
        /// Show about command
        /// </summary>
        /// <param name="notUse"></param>
        public void ShowAbout(object notUse)
        {
            var message = SQLExecutorInfo.GetSQLExecutorAboutInfo();
            _messageBoxService.DisplayMessage(message);
        }

        /// <summary>
        /// Exit application command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExitApplication(object notUse)
        {
            if (_messageBoxService.DisplayQuestion(ResourceStrings.ButtonExitQuestion))
                Application.Current.Shutdown();
        }

        /// <summary>
        /// Delete all selected databases command
        /// </summary>
        /// <param name="notUse"></param>
        public void DeleteAllSelectedDatabase(object notUse)
        {
            var firstSelectedDatabaseModel = 
                ClientDatabaseCollectionService.GetFirstSelectedDatabaseModel(GetDatabaseCollectionForCurrentTreeViewItem());

            if (firstSelectedDatabaseModel != null)
            {
                if (_messageBoxService.DisplayQuestion(ResourceStrings.DeleteAllSelectedDatabaseQuestion))
                {
                    _databaseModelService.DeleteSelectedDatabases(GetDatabaseCollectionForCurrentTreeViewItem());
                    RefreshClientDatabaseCollectionList();
                }
            }
            else
            {
                _messageBoxService.DisplayError(ResourceStrings.NoSelectedDatabase);
            }
        }

        /// <summary>
        /// Go to add database page command
        /// </summary>
        /// <param name="notUse"></param>
        public void GoToAddDatabasePage(object notUse)
        {
            SetAddDatabaseMode();
            _navigationService.Navigate(ResourceStrings.AddEditDatabasePageName);
        }

        /// <summary>
        /// Edit database command
        /// </summary>
        /// <param name="notUse"></param>
        public void EditDatabase(object notUse)
        {
            var firstSelectedDatabaseModel = 
                ClientDatabaseCollectionService.GetFirstSelectedDatabaseModel(GetDatabaseCollectionForCurrentTreeViewItem());

            if (firstSelectedDatabaseModel == null)
            {
                _messageBoxService.DisplayError(ResourceStrings.NoSelectedDatabase);
                return;
            }

            var numberOfSelected = 
                ClientDatabaseCollectionService.GetNumberOfSelectedDatabase(GetDatabaseCollectionForCurrentTreeViewItem());

            if (numberOfSelected > ONE_DATABASE)
                _messageBoxService.DisplayWarning(ResourceStrings.CopyFromFirstSelected);

            GoToEditDatabasePage();
        }

        /// <summary>
        /// Remove all treeView items from current selected collection
        /// </summary>
        /// <param name="notUse"></param>
        public void ClearAllTreeViewItems(object notUse)
        {
            SelectedTabTreeView.TreeViewItemCollection.Clear();
        }

        /// <summary>
        /// Add selected files from open file dialog command
        /// </summary>
        /// <param name="notUse"></param>
        public void AddSelectedFiles(object notUse)
        {
            var selectedFiles = new OpenDialogService().AddCollectionFromFilesDialog();

            if (selectedFiles != null && selectedFiles.Count > 0)
                foreach (ITreeViewItem scriptModel in selectedFiles)
                    GetCurrentTreeViewItemCollection().Add(scriptModel);
        }

        /// <summary>
        /// Add selected folder with content from folder browser dialog command
        /// </summary>
        /// <param name="notUse"></param>
        public void AddSelectedFolder(object notUse)
        {
            var selectedFolder = new OpenDialogService().AddCollectionFromFolderDialog();

            if (selectedFolder != null)
                GetCurrentTreeViewItemCollection().Add(selectedFolder);
        }

        /// <summary>
        /// Delete all selected items with subitems command
        /// </summary>
        /// <param name="notUse"></param>
        public void DeleteAllSelectedItems(object notUse)
        {
            if (GetCurrentTreeViewItemCollection().Count == 0)
            {
                _messageBoxService.DisplayError(ResourceStrings.EmptyTreeViewScriptModelCollection);
                return;
            }

            if (_messageBoxService.DisplayQuestion(ResourceStrings.ButtonDeleteSelectedQuestion))
            {
                foreach (ITreeViewItem item in GetCurrentTreeViewItemCollection().ToList())
                    DeleteScriptFromTreeView(item);
            }
        }

        /// <summary>
        /// Copy from clipboard command
        /// </summary>
        /// <param name="notUse"></param>
        public void CopyFromClipboard(object notUse)
        {
            var clipboardTreeViewItem = new OpenDialogService().GetTreeViewItemFromClipboard();

            if (clipboardTreeViewItem != null)
                GetCurrentTreeViewItemCollection().Add(clipboardTreeViewItem);
        }

        /// <summary>
        /// TreeView drop command
        /// </summary>
        /// <param name="dragEventArgs"></param>
        public void TreeViewDrop(DragEventArgs dragEventArgs)
        {
            var dropTreeViewItemCollection = new OpenDialogService().GetCollectionFromDropEventArg(dragEventArgs);

            if (dropTreeViewItemCollection != null && dropTreeViewItemCollection.Count > 0)
                foreach (ITreeViewItem item in dropTreeViewItemCollection)
                    GetCurrentTreeViewItemCollection().Add(item);
        }

        /// <summary>
        /// Expand treeViewItem command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExpandTreeView(object notUse)
        {
            TreeViewItemService.ExpandAll(GetCurrentTreeViewItemCollection());
        }

        /// <summary>
        /// Collapse treeViewItem command
        /// </summary>
        /// <param name="notUse"></param>
        public void CollapseTreeView(object notUse)
        {
            TreeViewItemService.CollapseAll(GetCurrentTreeViewItemCollection());
        }

        /// <summary>
        /// Select all scripts command
        /// </summary>
        /// <param name="notUse"></param>
        public void SelectAllScripts(object notUse)
        {
            TreeViewItemService.ChangeCheckedStatus(true, GetCurrentTreeViewItemCollection());
        }

        /// <summary>
        /// Deselect all scripts command
        /// </summary>
        /// <param name="notUse"></param>
        public void DeselectAllScripts(object notUse)
        {
            TreeViewItemService.ChangeCheckedStatus(false, GetCurrentTreeViewItemCollection());
        }

        /// <summary>
        /// Reoad scripts with settings encoding command
        /// </summary>
        /// <param name="notUse"></param>
        public void ReloadSelectedWithEncoding(object notUse)
        {
            TreeViewItemService.ReloadWithEncodingFromSettings(GetCurrentTreeViewItemCollection());
            _messageBoxService.DisplayMessage(ResourceStrings.FilesReloaded);
        }

        /// <summary>
        /// Execute items command
        /// </summary>
        /// <param name="notUse"></param>
        public void ExecuteItems(object notUse)
        {
            var firstSelectedDatabaseModel =
                ClientDatabaseCollectionService.GetFirstSelectedDatabaseModel(GetDatabaseCollectionForCurrentTreeViewItem());

            if (firstSelectedDatabaseModel == null)
            {
                _messageBoxService.DisplayError(ResourceStrings.NoSelectedDatabase);
                return;
            }

            var numberOfSelectedScripts =
                TreeViewItemService.GetListOfAllSelectedItems(GetCurrentTreeViewItemCollection()).Count();

            if (numberOfSelectedScripts == 0)
            {
                _messageBoxService.DisplayError(ResourceStrings.NoSelectedItems);
                return;
            }

            BGService.Instance.OpenProgressBar();
            BGService.Instance.ExecuteItems(GetDatabaseCollectionForCurrentTreeViewItem(), GetCurrentTreeViewItemCollection());
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings      

        /// <summary>
        /// Collection of scripts in treeView
        /// </summary>
        private ObservableCollection<ITreeViewItem> ScriptsTreeViewItemCollection
        {
            get
            {
                _scriptsTreeViewItemCollection =
                    TabTreeViewCollection.Where(t => t.TreeViewType == TabTreeViewType.SCRIPTS).FirstOrDefault().TreeViewItemCollection;
                if (_scriptsTreeViewItemCollection == null)
                    _scriptsTreeViewItemCollection = new ObservableCollection<ITreeViewItem>();

                return _scriptsTreeViewItemCollection;
            }
            set
            {
                Set(ref _scriptsTreeViewItemCollection, value);
            }
        }

        /// <summary>
        /// Collection of reporst in treeView
        /// </summary>
        private ObservableCollection<ITreeViewItem> ReportsTreeViewItemCollection
        {
            get
            {
                _reportsTreeViewItemCollection =
                    TabTreeViewCollection.Where(t => t.TreeViewType == TabTreeViewType.REPORTS).FirstOrDefault().TreeViewItemCollection;
                if (_reportsTreeViewItemCollection == null)
                    _reportsTreeViewItemCollection = new ObservableCollection<ITreeViewItem>();

                return _reportsTreeViewItemCollection;
            }
            set
            {
                Set(ref _reportsTreeViewItemCollection, value);
            }
        }

        /// <summary>
        /// Scripts context menu
        /// </summary>
        private ObservableCollection<IBindableMenuItem> ScriptsContextMenuItemsCollection
        {
            get
            {
                if (_scriptsContextMenuItemsCollection == null)
                    _scriptsContextMenuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _scriptsContextMenuItemsCollection;
            }
            set
            {
                Set(ref _scriptsContextMenuItemsCollection, value);
            }
        }

        /// <summary>
        /// Repotrs contect menu
        /// </summary>
        private ObservableCollection<IBindableMenuItem> ReportsContextMenuItemsCollection
        {
            get
            {
                if (_reportsContextMenuItemsCollection == null)
                    _reportsContextMenuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _reportsContextMenuItemsCollection;
            }
            set
            {
                Set(ref _reportsContextMenuItemsCollection, value);
            }
        }

        /// <summary>
        /// List of client scripts databases collections
        /// </summary>
        private ObservableCollection<IClientDatabaseCollection> ClientScriptsDatabaseCollectionList
        {
            get
            {
                if (_clientScriptsDatabaseCollectionList == null)
                    _clientScriptsDatabaseCollectionList = new ObservableCollection<IClientDatabaseCollection>();
                return _clientScriptsDatabaseCollectionList;
            }
            set
            {
                Set(ref _clientScriptsDatabaseCollectionList, value);
            }
        }

        /// <summary>
        /// List of client reports databases collections
        /// </summary>
        private ObservableCollection<IClientDatabaseCollection> ClientReportsDatabaseCollectionList
        {
            get
            {
                if (_clientReportsDatabaseCollectionList == null)
                    _clientReportsDatabaseCollectionList = new ObservableCollection<IClientDatabaseCollection>();

                return _clientReportsDatabaseCollectionList;
            }
            set
            {
                Set(ref _clientReportsDatabaseCollectionList, value);
            }
        }

        /// <summary>
        /// Initialize all view model binding commands
        /// </summary>
        private void InitViewModelBindingCommands()
        {
            GoBackCommand = new RelayCommand<object>(GoBack);
            GoToUserSettingsPageCommand = new RelayCommand<object>(GoToUserSettingsPage);
            ShowAboutCommand = new RelayCommand<object>(ShowAbout);
            ExitApplicationCommand = new RelayCommand<object>(ExitApplication);
            SelectDeselectAllDatabaseCommand = new RelayCommand<object>(SelectDeselectAllDatabase);
            ExpandCollapseAllDatabaseCommand = new RelayCommand<object>(ExpandCollapseAllDatabase);
            DeleteAllSelectedDatabaseCommand = new RelayCommand<object>(DeleteAllSelectedDatabase);
            GoToAddDatabasePageCommand = new RelayCommand<object>(GoToAddDatabasePage);
            EditDatabaseCommand = new RelayCommand<object>(EditDatabase);
            ClearAllTreeViewItemsCommand = new RelayCommand<object>(ClearAllTreeViewItems);
            AddSelectedFilesCommand = new RelayCommand<object>(AddSelectedFiles);
            AddSelectedFolderCommand = new RelayCommand<object>(AddSelectedFolder);
            DeleteAllSelectedItemsCommand = new RelayCommand<object>(DeleteAllSelectedItems);
            CopyFromClipboardCommand = new RelayCommand<object>(CopyFromClipboard);
            TreeViewDropCommand = new RelayCommand<DragEventArgs>(TreeViewDrop);
            ExpandTreeViewCommand = new RelayCommand<object>(ExpandTreeView);
            CollapseTreeViewCommand = new RelayCommand<object>(CollapseTreeView);
            SelectAllScriptsCommand = new RelayCommand<object>(SelectAllScripts);
            DeselectAllScriptsCommand = new RelayCommand<object>(DeselectAllScripts);
            ReloadSelectedWithEncodingCommand = new RelayCommand<object>(ReloadSelectedWithEncoding);
            PreviewEditScriptCommand = new RelayCommand<object>(PreviewEditScript);
            ExecuteItemsCommand = new RelayCommand<object>(ExecuteItems);
            ChangeSelectedTabTreeViewItemCommand = new RelayCommand<SelectionChangedEventArgs>(ChangeSelectedTabTreeViewItem);
        }

        /// <summary>
        /// Change selected TabTreeViewItem method
        /// </summary>
        /// <param name="commandObject"></param>
        private void ChangeSelectedTabTreeViewItem(SelectionChangedEventArgs commandObject)
        {
            var removedObject = commandObject.RemovedItems;
            // Return if it is an initialization 
            if (removedObject is object[] && !((object[])removedObject).Any()) return;

            ChangeVisibilityInClientDatabaseInterfaceList();
        }

        /// <summary>
        /// Reverse visibility of all ClientDatabaseInterface items
        /// </summary>
        private void ChangeVisibilityInClientDatabaseInterfaceList()
        {
            foreach (IClientDatabaseInterface clientDatabaseInterface in ClientDatabaseInterfaceList)
                clientDatabaseInterface.IsVisible = !clientDatabaseInterface.IsVisible;
        }

        /// <summary>
        /// Open preview/edit script window
        /// </summary>
        /// <param name="notUse"></param>
        private void PreviewEditScript(object notUse)
        {
            var scriptList = TreeViewItemService.GetListOfAllSelectedItems(_scriptsTreeViewItemCollection);
            if (scriptList != null && scriptList.Count > 0)
            {
                var editedScriptModel = scriptList.First();
                TreeViewItemService.PreviewEditScript(editedScriptModel);
            }
        }        

        /// <summary>
        /// Go to edit database page
        /// </summary>
        private void GoToEditDatabasePage()
        {
            SetEditDatabaseMode();
            _navigationService.Navigate(ResourceStrings.AddEditDatabasePageName);
        }

        /// <summary>
        /// Select/Deselect all database in client database collection list
        /// </summary>
        /// <param name="notUse"></param>
        private void SelectDeselectAllDatabase(object notUse)
        {
            var numberOfScripts = GetDatabaseCollectionForCurrentTreeViewItem().Count;
            if (numberOfScripts == 0) return;

            var firstSelectedDatabaseModel = 
                ClientDatabaseCollectionService.GetFirstSelectedDatabaseModel(GetDatabaseCollectionForCurrentTreeViewItem());

            if (firstSelectedDatabaseModel == null)
                ChangeAllDatabaseStatus(true);
            else
                ChangeAllDatabaseStatus(false);
        }

        /// <summary>
        /// Change isSelected properties of all databases to input status
        /// </summary>
        /// <param name="status"> Input bool status </param>
        private void ChangeAllDatabaseStatus(bool status)
        {
            foreach (IClientDatabaseCollection clientDatabaseCollection in GetDatabaseCollectionForCurrentTreeViewItem())
            {
                clientDatabaseCollection.ClientDatabaseList.Where(c => c.IsSelected != status).ToList().ForEach(s => s.IsSelected = status);
            }
        }

        /// <summary>
        /// Expand/Collapse all database in client database collection list
        /// </summary>
        /// <param name="notUse"></param>
        private void ExpandCollapseAllDatabase(object notUse)
        {
            if (GetDatabaseCollectionForCurrentTreeViewItem().Count == 0) return;

            if (!CheckIsAnyClientCollectionExpanded())
                ChangeClientCollectionExpandedStatus(true);
            else
                ChangeClientCollectionExpandedStatus(false);
        }

        /// <summary>
        /// Change all client collection expanders expanded to input status
        /// </summary>
        /// <param name="status"></param>
        private void ChangeClientCollectionExpandedStatus(bool status)
        {
            GetDatabaseCollectionForCurrentTreeViewItem().Where(c => c.IsExpanded != status).ToList().ForEach(s => s.IsExpanded = status);
        }

        /// <summary>
        /// CHeck is any client database collection expander in expanded state
        /// </summary>
        /// <returns></returns>
        private bool CheckIsAnyClientCollectionExpanded()
        {
            var expandedList = (from collection in GetDatabaseCollectionForCurrentTreeViewItem()
                                where collection.IsExpanded == true
                                select collection).ToList();

            if (expandedList.Count > 0)
                return true;
            return false;
        }

        /// <summary>
        /// Refresh client database collections in client database collection list
        /// </summary>
        private void RefreshClientDatabaseCollectionList()
        {
            InitClientDatabaseCollectionList();
        }

        /// <summary>
        /// Recursive delete all selected items from collection
        /// </summary>
        /// <param name="treeViewItem"></param>
        private void DeleteScriptFromTreeView(ITreeViewItem treeViewItem)
        {
            if (treeViewItem.IsChecked.HasValue && treeViewItem.IsChecked == true)
            {
                DeletetreeViewItem(treeViewItem);
            }
            else
            {
                if (treeViewItem.Children != null && treeViewItem.Children.Count > 0)
                    foreach (ITreeViewItem item in treeViewItem.Children.ToList())
                        DeleteScriptFromTreeView(item);
            }
        }

        /// <summary>
        /// Delete treeViewItem
        /// </summary>
        /// <param name="treeViewItem"></param>
        private void DeletetreeViewItem(ITreeViewItem treeViewItem)
        {
            if (treeViewItem.Parent != null)
            {
                var parentTreeViewItem = treeViewItem.Parent;
                parentTreeViewItem.Children.Remove(treeViewItem);
                UpdateTreeViewCheckedStatus(parentTreeViewItem);
            }
            else
            {
                GetCurrentTreeViewItemCollection().Remove(treeViewItem);
            }
        }

        /// <summary>
        /// Update checked status of treeViewItem parents only
        /// </summary>
        /// <param name="treeViewItem"></param>
        private void UpdateTreeViewCheckedStatus(ITreeViewItem treeViewItem)
        {
            treeViewItem.UpdateChildren = false;
            treeViewItem.IsChecked = false;
            treeViewItem.UpdateChildren = true;
        }

        /// <summary>
        /// Add go back interface button to toolbox button list
        /// </summary>
        private void AddGoBackInterfaceButton()
        {
            ToolboxButtonList.Add(GoBackInterfaceButton());
        }

        /// <summary>
        /// Return go back interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton GoBackInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonGoBack,
                Command = GoBackCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add execute scripts interface button to toolbox button list
        /// </summary>
        private void AddExecuteScripsInterfaceButton()
        {
            ToolboxButtonList.Add(ExecuteScriptsInterfaceButton());
        }

        /// <summary>
        /// Return execute srtipts interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton ExecuteScriptsInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonExecute,
                Command = ExecuteItemsCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Add close application interface button to toolbox button list
        /// </summary>
        private void AddCloseInterfaceButton()
        {
            ToolboxButtonList.Add(CloseInterfaceButton());
        }

        /// <summary>
        /// Return close application interface button
        /// </summary>
        /// <returns></returns>
        private IInterfaceButton CloseInterfaceButton()
        {
            return new InterfaceButton()
            {
                Content = ResourceStrings.ButtonExit,
                Command = ExitApplicationCommand,
                CommandParameters = null
            };
        }

        /// <summary>
        /// Initialize interface buttons in toolbox button list
        /// </summary>
        private void InitToolboxButtonList()
        {
            ClearToolboxButtonList();
            AddGoBackInterfaceButton();
            AddExecuteScripsInterfaceButton();
            AddCloseInterfaceButton();
        }

        /// <summary>
        /// Remove all elements from toolbox button list
        /// </summary>
        private void ClearToolboxButtonList()
        {
            if (ToolboxButtonList != null)
                ToolboxButtonList.Clear();
        }

        /// <summary>
        /// Initialize items in menu items collection
        /// </summary>
        private void InitMenuItemsCollection()
        {
            ClearMenuItemsCollection();
            AddDatabaseMenuItemsList();
            AddScriptsMenuItemsList();
            AddSettingsMenuItemsList();
            AddHelpMenuItemsList();
        }

        /// <summary>
        /// Initialize items in scripts context menu items collection
        /// </summary>
        private void InitScriptsContextMenuItemsCollection()
        {
            ClearContextMenuItemsCollection(ScriptsContextMenuItemsCollection);
            AddClearAllItems(ScriptsContextMenuItemsCollection);
            AddOpenFileDialogMenuItem(ScriptsContextMenuItemsCollection);
            AddFolderBrowserDialogItem(ScriptsContextMenuItemsCollection);
            AddDeleteAllSelectedItem(ScriptsContextMenuItemsCollection);
            AddCopyFromClipboardItem(ScriptsContextMenuItemsCollection);
            AddSelectAllItem(ScriptsContextMenuItemsCollection);
            AddDeselectAllItem(ScriptsContextMenuItemsCollection);
            AddExpandTreeViewItem(ScriptsContextMenuItemsCollection);
            AddCollapseTreeViewItem(ScriptsContextMenuItemsCollection);
            AddReloadSelectedScriptsWithEncodingItem();
            AddPreviewEditScriptItem();
        }

        /// <summary>
        /// Initialize items in reports context menu items collection
        /// </summary>
        private void InitReportsContextMenuItemsCollection()
        {
            ClearContextMenuItemsCollection(ReportsContextMenuItemsCollection);
            AddClearAllItems(ReportsContextMenuItemsCollection);
            AddOpenFileDialogMenuItem(ReportsContextMenuItemsCollection);
            AddFolderBrowserDialogItem(ReportsContextMenuItemsCollection);
            AddDeleteAllSelectedItem(ReportsContextMenuItemsCollection);
            AddCopyFromClipboardItem(ReportsContextMenuItemsCollection);
            AddSelectAllItem(ReportsContextMenuItemsCollection);
            AddDeselectAllItem(ReportsContextMenuItemsCollection);
            AddExpandTreeViewItem(ReportsContextMenuItemsCollection);
            AddCollapseTreeViewItem(ReportsContextMenuItemsCollection);
        }

        /// <summary>
        /// Add Database Menu items list to menu items collection
        /// </summary>
        private void AddDatabaseMenuItemsList()
        {
            MenuItemsCollection.Add(DatabaseMenuItemsList());
        }

        /// <summary>
        /// Return Database Menu item with submenu items list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem DatabaseMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.DatabaseString,
                Command = null,
                CommandParameters = null,
                SubMenuItems = DatabaseMenuItems()
            };
        }

        /// <summary>
        /// Return Database Menu items list
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> DatabaseMenuItems()
        {
            var buttonList = new ObservableCollection<IBindableMenuItem>
            {
                AddDatabaseMenuItem(),
                DeleteAllSelectedDatabaseMenuItem(),
                EditSelectedDatabaseMenuItem()
            };

            return buttonList;
        }

        /// <summary>
        /// Add database menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem AddDatabaseMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAddDatabase,
                Command = GoToAddDatabasePageCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Delete all selected database menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem DeleteAllSelectedDatabaseMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonDeleteSelected,
                Command = DeleteAllSelectedDatabaseCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Edit selected database menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem EditSelectedDatabaseMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonEditSelected,
                Command = EditDatabaseCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add Scripts Menu items to menu items collection
        /// </summary>
        private void AddScriptsMenuItemsList()
        {
            MenuItemsCollection.Add(ScriptsMenuItemsList());
        }

        /// <summary>
        /// Return Scripts Menu item with items submenu list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ScriptsMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ScriptsReportsMenuItem,
                Command = null,
                CommandParameters = null,
                SubMenuItems = ScriptsMenuItems()
            };
        }

        /// <summary>
        /// Return Scripts menu items list
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> ScriptsMenuItems()
        {
            var buttonList = new ObservableCollection<IBindableMenuItem>
            {
                OpenFileDialogMenuItem(),
                FolderBrowserDialogItem(),
                DeleteAllSelectedScriptsItem(),
                CopyFromClipboardItem(),
                SelectAllScriptsItem(),
                DeselectAllScriptsItem(),
                ExpandTreeViewItem(),
                CollapseTreeViewItem(),
                ReloadSelectedWithEncodingItem(),
                PreviewEditScriptItem()
            };

            return buttonList;
        }

        /// <summary>
        /// Add Settings Menu items list to menu items collection
        /// </summary>
        private void AddSettingsMenuItemsList()
        {
            MenuItemsCollection.Add(SettingsMenuItemsList());
        }

        /// <summary>
        /// Return Settings Menu item with items submenu list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem SettingsMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.SettingsMenuItem,
                Command = null,
                CommandParameters = null,
                SubMenuItems = SettingsMenuItems()
            };
        }

        /// <summary>
        /// Return Settings Menu items list
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> SettingsMenuItems()
        {
            var buttonList = new ObservableCollection<IBindableMenuItem>
            {
                UserSettingsMenuItem()
            };

            return buttonList;
        }

        /// <summary>
        /// Return User Settings menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem UserSettingsMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonUserSettings,
                Command = GoToUserSettingsPageCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add Help Menu items list to menu items collection
        /// </summary>
        private void AddHelpMenuItemsList()
        {
            MenuItemsCollection.Add(HelpMenuItemsList());
        }

        /// <summary>
        /// Return Help Menu item with submenu items list
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem HelpMenuItemsList()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ItemHelp,
                SubMenuItems = HelpMenuItems()
            };
        }

        /// <summary>
        /// Return Help Menu items collection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IBindableMenuItem> HelpMenuItems()
        {
            var helpMenuItems = new ObservableCollection<IBindableMenuItem>
            {
                ShowAboutMenuItem(),
                ExitMenuItem()
            };

            return helpMenuItems;
        }

        /// <summary>
        /// Return exit application menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ExitMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonExitApplication,
                Command = ExitApplicationCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Return show about menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ShowAboutMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAbout,
                Command = ShowAboutCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add clear all menu item to context menu
        /// </summary>
        /// <param name="contextMenuItemCollection"></param>
        private void AddClearAllItems(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(ClearAllItems());
        }

        /// <summary>
        /// Return clear all menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ClearAllItems()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonClearAll,
                Command = ClearAllTreeViewItemsCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add open file dialog menu item to context menu
        /// </summary>
        private void AddOpenFileDialogMenuItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(OpenFileDialogMenuItem());
        }

        /// <summary>
        /// Return open file dialog menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem OpenFileDialogMenuItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAddFiles,
                Command = AddSelectedFilesCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add folder browser dialog menu item to context menu
        /// </summary>
        private void AddFolderBrowserDialogItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(FolderBrowserDialogItem());
        }

        /// <summary>
        /// Return folder browser dialog menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem FolderBrowserDialogItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonAddFolder,
                Command = AddSelectedFolderCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add delete all selected item to context menu
        /// </summary>
        private void AddDeleteAllSelectedItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(DeleteAllSelectedScriptsItem());
        }

        /// <summary>
        /// Return delete all selected scripts item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem DeleteAllSelectedScriptsItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonDeleteSelected,
                Command = DeleteAllSelectedItemsCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add copy from clipboard item to context menu
        /// </summary>
        private void AddCopyFromClipboardItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(CopyFromClipboardItem());
        }

        /// <summary>
        /// Return copy from clipboard menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem CopyFromClipboardItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.CopyFromClipboard,
                Command = CopyFromClipboardCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add select all menu item to context menu
        /// </summary>
        private void AddSelectAllItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(SelectAllScriptsItem());
        }

        /// <summary>
        /// Return select all scripts menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem SelectAllScriptsItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonSelectAll,
                Command = SelectAllScriptsCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add deselect all menu item to context menu
        /// </summary>
        private void AddDeselectAllItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(DeselectAllScriptsItem());
        }

        /// <summary>
        /// Return deselect all scripts menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem DeselectAllScriptsItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonDeselectAllScripts,
                Command = DeselectAllScriptsCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add expand treeView item to context menu
        /// </summary>
        private void AddExpandTreeViewItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(ExpandTreeViewItem());
        }

        /// <summary>
        /// Return expand treeView item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ExpandTreeViewItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ExpandTreeViewCollection,
                Command = ExpandTreeViewCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add collapse treeView item to context menu
        /// </summary>
        private void AddCollapseTreeViewItem(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            contextMenuItemCollection.Add(CollapseTreeViewItem());
        }

        /// <summary>
        /// Return collapse treeView item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem CollapseTreeViewItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.CollapseTreeViewCollection,
                Command = CollapseTreeViewCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add reload selected scripts with encoding item to context menu item
        /// </summary>
        private void AddReloadSelectedScriptsWithEncodingItem()
        {
            ScriptsContextMenuItemsCollection.Add(ReloadSelectedWithEncodingItem());
        }

        /// <summary>
        /// Return reload selected scripts with encoding menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem ReloadSelectedWithEncodingItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ReloadSelectedWithEncoding,
                Command = ReloadSelectedWithEncodingCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Add previe/edit script menu item to context menu item
        /// </summary>
        private void AddPreviewEditScriptItem()
        {
            ScriptsContextMenuItemsCollection.Add(PreviewEditScriptItem());
        }

        /// <summary>
        /// Return preview/edit script menu item
        /// </summary>
        /// <returns></returns>
        private IBindableMenuItem PreviewEditScriptItem()
        {
            return new BindableMenuItem()
            {
                Header = ResourceStrings.ButtonPreviewEditScript,
                Command = PreviewEditScriptCommand,
                CommandParameters = null,
                SubMenuItems = null
            };
        }

        /// <summary>
        /// Return instance of interface button with content and command
        /// </summary>
        /// <param name="content"> Button content </param>
        /// <param name="buttonCommand"> Button command </param>
        /// <param name="commandParameters"> Optional command parameters </param>
        /// <returns></returns>
        private IInterfaceButton InterfaceButtonInstance(string content, ICommand buttonCommand, string commandParameters = null)
        {
            return new InterfaceButton()
            {
                Content = content,
                Command = buttonCommand,
                CommandParameters = commandParameters
            };
        }

        /// <summary>
        /// Remove all elements from menu items collection
        /// </summary>
        private void ClearMenuItemsCollection()
        {
            if (MenuItemsCollection != null)
                MenuItemsCollection.Clear();
        }

        /// <summary>
        /// Remove all elements from context menu items collection
        /// </summary>
        private void ClearContextMenuItemsCollection(ObservableCollection<IBindableMenuItem> contextMenuItemCollection)
        {
            if (contextMenuItemCollection != null)
                contextMenuItemCollection.Clear();
        }

        /// <summary>
        /// Initialize client database control buttons list
        /// </summary>
        private void InitClientDatabaseControlButtonList()
        {
            ClearClientDatabaseControlButtonList();
            AddSelectDeselectAllInterfaceButton();
            AddExpandCollapseAllInterfaceButton();
        }

        /// <summary>
        /// Add select/deselect all interface button to client database control button list
        /// </summary>
        private void AddSelectDeselectAllInterfaceButton()
        {
            ClientDatabaseControlButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonSelectDeselectAll, SelectDeselectAllDatabaseCommand));
        }

        /// <summary>
        /// Add expand/collapse all interface button to client database control button list
        /// </summary>
        private void AddExpandCollapseAllInterfaceButton()
        {
            ClientDatabaseControlButtonList.Add(InterfaceButtonInstance(ResourceStrings.ButtonExpandCollapseAll, ExpandCollapseAllDatabaseCommand));
        }

        /// <summary>
        /// Remove all elements from client database control buttons list
        /// </summary>
        private void ClearClientDatabaseControlButtonList()
        {
            if (ClientDatabaseControlButtonList != null)
                ClientDatabaseControlButtonList.Clear();
        }

        /// <summary>
        /// Initialize client database collections in client database collection list
        /// </summary>
        private void InitClientDatabaseCollectionList()
        {
            ClearDatabaseCollectionList();
            InitClientScriptsDatabaseCollectionList();
            InitClientReportsDatabaseCollectionList();
        }

        /// <summary>
        /// Initialize client scripts database collections
        /// </summary>
        private void InitClientScriptsDatabaseCollectionList()
        {
            var allClientCollectionList = ClientDatabaseCollectionService.GetCollectionOfAllClients(_databaseModelService.GetAllDatabaseModel());

            foreach (IClientDatabaseCollection clientDatabaseCollection in allClientCollectionList)
            {
                ClientScriptsDatabaseCollectionList.Add(clientDatabaseCollection);
            }
        }

        /// <summary>
        /// Initialize client reports database collections
        /// </summary>
        private void InitClientReportsDatabaseCollectionList()
        {
            var allClientCollectionList = ClientDatabaseCollectionService.GetCollectionOfAllClients(_databaseModelService.GetAllReportsModel());

            foreach (IClientDatabaseCollection clientDatabaseCollection in allClientCollectionList)
            {
                ClientReportsDatabaseCollectionList.Add(clientDatabaseCollection);
            }
        }

        /// <summary>
        /// Remove all elements from client database collection list
        /// </summary>
        private void ClearDatabaseCollectionList()
        {
            if (ClientScriptsDatabaseCollectionList != null)
                ClientScriptsDatabaseCollectionList.Clear();

            if (ClientReportsDatabaseCollectionList != null)
                ClientReportsDatabaseCollectionList.Clear();
        }

        /// <summary>
        /// Initialize list of client database interfaces
        /// </summary>
        private void InitClientDatabaseInterfaceList()
        {
            ClearClientDatabaseInterfaceList();
            AddClientScriptsDatabaseInterface();
            AddClientReportsDatabaseInterface();
        }

        /// <summary>
        /// Add client scripts database interface to client database interface list
        /// </summary>
        private void AddClientScriptsDatabaseInterface()
        {
            ClientDatabaseInterfaceList.Add(ClientScriptsDatabaseInterface());
        }

        /// <summary>
        /// Return client scripts database interface
        /// </summary>
        /// <returns></returns>
        private IClientDatabaseInterface ClientScriptsDatabaseInterface()
        {
            return new ClientDatabaseInterface()
            {
                IsVisible = (SelectedTabTreeView.TreeViewType == TabTreeViewType.SCRIPTS),
                ClientDatabaseCollectionList = ClientScriptsDatabaseCollectionList,
                ClientDatabaseControlButtonList = ClientDatabaseControlButtonList
            };
        }

        /// <summary>
        /// Add client reports database interface to client database interface list
        /// </summary>
        private void AddClientReportsDatabaseInterface()
        {
            ClientDatabaseInterfaceList.Add(ClientReportsDatabaseInterface());
        }

        /// <summary>
        /// Return client reports database interface
        /// </summary>
        /// <returns></returns>
        private IClientDatabaseInterface ClientReportsDatabaseInterface()
        {
            return new ClientDatabaseInterface()
            {
                IsVisible = (SelectedTabTreeView.TreeViewType == TabTreeViewType.REPORTS),
                ClientDatabaseCollectionList = ClientReportsDatabaseCollectionList,
                ClientDatabaseControlButtonList = ClientDatabaseControlButtonList
            };
        }

        /// <summary>
        /// Remove all elements from client database interface list
        /// </summary>
        private void ClearClientDatabaseInterfaceList()
        {
            if (ClientDatabaseInterfaceList != null)
                ClientDatabaseInterfaceList.Clear();
        }

        /// <summary>
        /// Initialize tab treeView collection
        /// </summary>
        private void InitTabTreeViewCollection()
        {
            ClearTabTreeViewCollection();
            AddScriptTabTreeView();
            AddReportTabTreeView();
        }

        /// <summary>
        /// Add script tab treeView to tab treeView collection
        /// </summary>
        private void AddScriptTabTreeView()
        {
            TabTreeViewCollection.Add(ScriptTabTreeView());
        }

        /// <summary>
        /// Return script tab treeView
        /// </summary>
        /// <returns></returns>
        private ITabTreeView ScriptTabTreeView()
        {
            return new TabTreeView()
            {
                TreeViewType = TabTreeViewType.SCRIPTS,
                Header = ResourceStrings.TabTreeViewScripts,
                TreeViewItemCollection = null,
                ContextMenuItemsCollection = ScriptsContextMenuItemsCollection
            };
        }

        /// <summary>
        /// Add report tab treeView to bat treeView collection
        /// </summary>
        private void AddReportTabTreeView()
        {
            TabTreeViewCollection.Add(ReportTabTreeView());
        }

        /// <summary>
        /// Return report tab treeView
        /// </summary>
        /// <returns></returns>
        private ITabTreeView ReportTabTreeView()
        {
            return new TabTreeView()
            {
                TreeViewType = TabTreeViewType.REPORTS,
                Header = ResourceStrings.TabTreeViewReports,
                TreeViewItemCollection = null,
                ContextMenuItemsCollection = ReportsContextMenuItemsCollection
            };
        }

        /// <summary>
        /// Remove all elements from tab treeView collection
        /// </summary>
        private void ClearTabTreeViewCollection()
        {
            if (TabTreeViewCollection != null)
                TabTreeViewCollection.Clear();
        }

        /// <summary>
        /// Set add database mode and set null to edit database model in
        /// edit database model service
        /// </summary>
        private void SetAddDatabaseMode()
        {
            EditDatabaseModelService.SetDatabaseModelToEdit(null);
        }

        /// <summary>
        /// Set edit database mode and set model to edit in
        /// edit database model service
        /// </summary>
        private void SetEditDatabaseMode()
        {
            var databaseToEdit = ClientDatabaseCollectionService.GetFirstSelectedDatabaseModel(GetDatabaseCollectionForCurrentTreeViewItem());
            EditDatabaseModelService.SetDatabaseModelToEdit(databaseToEdit);
        }

        /// <summary>
        /// Return current selected TreeViewItemCollection from TabTreeViewItemCollection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<ITreeViewItem> GetCurrentTreeViewItemCollection()
        {
            switch (SelectedTabTreeView.TreeViewType)
            {
                case TabTreeViewType.SCRIPTS:
                    return ScriptsTreeViewItemCollection;
                case TabTreeViewType.REPORTS:
                    return ReportsTreeViewItemCollection;
                default:
                    return default(ObservableCollection<ITreeViewItem>);
            }
        }

        /// <summary>
        /// Return client database list for current selected TreeViewItemCollection
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<IClientDatabaseCollection> GetDatabaseCollectionForCurrentTreeViewItem()
        {
            switch (SelectedTabTreeView.TreeViewType)
            {
                case TabTreeViewType.SCRIPTS:
                    return ClientScriptsDatabaseCollectionList;
                case TabTreeViewType.REPORTS:
                    return ClientReportsDatabaseCollectionList;
                default:
                    return default(ObservableCollection<IClientDatabaseCollection>);
            }
        }

        /// <summary>
        /// Return new instance of database model service class
        /// </summary>
        /// <returns></returns>
        private IDatabaseModelService GetDatabaseModelService()
        {
            return new DatabaseModelService();
        }

        /// <summary>
        /// Return new instance of bgprogressbar view model class
        /// </summary>
        /// <returns></returns>
        private BGProgressBarViewModel GetBGProgressBarViewModel()
        {
            return new BGProgressBarViewModel();
        }
    }
}