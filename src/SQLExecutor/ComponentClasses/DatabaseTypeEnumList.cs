﻿using InterfaceToDatabase;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Database type enumeration list class
    /// </summary>
    public static class DatabaseTypeEnumList
    {
        private static int INDEX_ZERO = 0;

        /// <summary>
        /// Return observable collection of database type enumeration
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<IDisplayedEnum> GetEnumList()
        {
            ObservableCollection<IDisplayedEnum> displayedEnumList = new ObservableCollection<IDisplayedEnum>();

            displayedEnumList.Add(EmptyDisplayedEnum());

            foreach (DatabaseType databaseType in Enum.GetValues(typeof(DatabaseType)))
            {
                displayedEnumList.Add(new DisplayedEnum()
                {
                    Name = databaseType.ToString(),
                    Value = databaseType.ToString()
                });
            }

            return displayedEnumList;
        }

        /// <summary>
        /// Return index of database type from collection
        /// </summary>
        /// <param name="databaseType"> Input database type </param>
        /// <returns></returns>
        public static int GetIndexOfEnum(DatabaseType databaseType)
        {
            int indexOfSelected = INDEX_ZERO;
            var collection = GetEnumList();

            var selectedType = (from item in collection
                                where item.Value == databaseType.ToString()
                                select item).FirstOrDefault();

            if (selectedType != null)
                indexOfSelected = collection.IndexOf(selectedType);

            return indexOfSelected;
        }

        /// <summary>
        /// Empty displayed enum member
        /// </summary>
        /// <returns></returns>
        private static IDisplayedEnum EmptyDisplayedEnum()
        {
            return new DisplayedEnum()
            {
                Name = string.Empty,
                Value = string.Empty
            };
        }
    }
}