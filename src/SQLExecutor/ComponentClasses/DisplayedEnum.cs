﻿using BaseMVVM;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Displayed enumeration model class
    /// </summary>
    public class DisplayedEnum : ObservableObject, IDisplayedEnum
    {
        private string _name = string.Empty;
        private string _value = string.Empty;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                Set(ref _name, value);
            }
        }

        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                Set(ref _value, value);
            }
        }
    }
}