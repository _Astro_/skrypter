﻿using InterfaceToDatabase;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Connection timeout enumeration list class
    /// </summary>
    public static class ConnectionTimeoutEnumList
    {
        private static int INDEX_ZERO = 0;

        /// <summary>
        /// Return observable collection of connection timeout enumeration
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<IDisplayedEnum> GetEnumList()
        {
            ObservableCollection<IDisplayedEnum> connectionTimeoutEnumList = new ObservableCollection<IDisplayedEnum>();

            connectionTimeoutEnumList.Add(EmptyDisplayedEnum());

            foreach (ConnectionTimeoutEnum connectionTimeout in Enum.GetValues(typeof(ConnectionTimeoutEnum)))
            {
                connectionTimeoutEnumList.Add(new DisplayedEnum()
                {
                    Name = connectionTimeout.ToString(),
                    Value = ((int)connectionTimeout).ToString()
                });
            }

            return connectionTimeoutEnumList;
        }

        /// <summary>
        /// Return index of database type from collection
        /// </summary>
        /// <param name="databaseType"> Input database type </param>
        /// <returns></returns>
        public static int GetIndexOfEnum(int connectionTimeout)
        {
            int indexOfSelected = INDEX_ZERO;
            var collection = GetEnumList();

            var selectedType = (from item in collection
                                where item.Value == connectionTimeout.ToString()
                                select item).FirstOrDefault();

            if (selectedType != null)
                indexOfSelected = collection.IndexOf(selectedType);

            return indexOfSelected;
        }

        /// <summary>
        /// Empty displayed enum member
        /// </summary>
        /// <returns></returns>
        private static IDisplayedEnum EmptyDisplayedEnum()
        {
            return new DisplayedEnum()
            {
                Name = string.Empty,
                Value = string.Empty
            };
        }
    }
}