﻿using System.Collections.ObjectModel;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Interface of action expander component class
    /// </summary>
    public interface IActionExpander
    {
        string Header { get; set; }

        bool IsExpanded { get; set; }

        ObservableCollection<IDatabaseModelProperty> StaticDatabaseModelPropertyList { get; set; }

        ObservableCollection<IDatabaseModelProperty> DynamicDatabaseModelPropertyList { get; set; }

        ObservableCollection<IInterfaceButton> ActionButtonList { get; set; }
    }
}