﻿using InterfaceToDatabase;
using System.Collections.ObjectModel;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Interface of database model property component class
    /// </summary>
    public interface IDatabaseModelProperty
    {
        DatabaseModelPropertyEnum Property { get; set; }

        string PropertyName { get; set; }

        string PropertyValue { get; set; }

        string WatermarkText { get; set; }

        bool IsTextBox { get; set; }

        bool IsPasswordTextBox { get; set; }

        bool IsComboBox { get; set; }

        ObservableCollection<IDisplayedEnum> EnumList { get; set; }

        IDisplayedEnum DisplayedItem { get; set; }

        int CurrentSelectedIndex { get; set; }
    }
}