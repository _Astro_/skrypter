﻿using BaseClientData;
using System.Collections.ObjectModel;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Client name enumeration list class
    /// </summary>
    public static class ClientNameEnumList
    {
        /// <summary>
        /// Return observable collection of client name from input list
        /// </summary>
        /// <param name="clientDatabaseCollectionList"></param>
        /// <returns></returns>
        public static ObservableCollection<IDisplayedEnum> GetEnumList(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList)
        {
            ObservableCollection<IDisplayedEnum> displayedEnumList = new ObservableCollection<IDisplayedEnum>();

            // Add empty list member
            displayedEnumList.Add(EmptyDisplayedEnum());

            foreach (IClientDatabaseCollection clientDatabaseCollection in clientDatabaseCollectionList)
            {
                displayedEnumList.Add(new DisplayedEnum()
                {
                    Name = clientDatabaseCollection.Header,
                    Value = clientDatabaseCollection.Header
                });
            }

            return displayedEnumList;
        }

        /// <summary>
        /// Empty displayed enum member
        /// </summary>
        /// <returns></returns>
        private static IDisplayedEnum EmptyDisplayedEnum()
        {
            return new DisplayedEnum()
            {
                Name = string.Empty,
                Value = string.Empty
            };
        }
    }
}