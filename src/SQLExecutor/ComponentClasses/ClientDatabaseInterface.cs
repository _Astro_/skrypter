﻿using BaseClientData;
using BaseMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Client Database Interface class
    /// </summary>
    public class ClientDatabaseInterface : ObservableObject, IClientDatabaseInterface
    {
        private bool _isVisible;
        private ObservableCollection<IInterfaceButton> _clientDatabaseControlButtonList;
        private ObservableCollection<IClientDatabaseCollection> _clientDatabaseCollectionList;

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                Set(ref _isVisible, value);
            }
        }

        public ObservableCollection<IInterfaceButton> ClientDatabaseControlButtonList
        {
            get
            {
                if (_clientDatabaseControlButtonList == null)
                    _clientDatabaseControlButtonList = new ObservableCollection<IInterfaceButton>();

                return _clientDatabaseControlButtonList;
            }
            set
            {
                Set(ref _clientDatabaseControlButtonList, value);
            }
        }

        public ObservableCollection<IClientDatabaseCollection> ClientDatabaseCollectionList
        {
            get
            {
                if (_clientDatabaseCollectionList == null)
                    _clientDatabaseCollectionList = new ObservableCollection<IClientDatabaseCollection>();

                return _clientDatabaseCollectionList;
            }
            set
            {
                Set(ref _clientDatabaseCollectionList, value);
            }
        }
    }
}
