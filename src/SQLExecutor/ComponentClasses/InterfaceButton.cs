﻿using BaseMVVM;
using System.Windows.Input;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Interface Button model class
    /// </summary>
    public class InterfaceButton : ObservableObject, IInterfaceButton
    {
        public string Content { get; set; }

        public ICommand Command { get; set; }

        public string CommandParameters { get; set; }
    }
}