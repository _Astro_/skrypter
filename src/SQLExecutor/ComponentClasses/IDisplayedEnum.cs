﻿namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Interface of displayed enum component class
    /// </summary>
    public interface IDisplayedEnum
    {
        string Name { get; set; }

        string Value { get; set; }
    }
}