﻿using System.Windows.Input;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Interface of interface button component class
    /// </summary>
    public interface IInterfaceButton
    {
        string Content { get; set; }

        ICommand Command { get; set; }

        string CommandParameters { get; set; }
    }
}