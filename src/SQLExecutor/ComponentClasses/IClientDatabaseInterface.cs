﻿using BaseClientData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// CLient Database Interface interface
    /// </summary>
    public interface IClientDatabaseInterface
    {
        bool IsVisible { get; set; }     
        ObservableCollection<IInterfaceButton> ClientDatabaseControlButtonList { get; set; }
        ObservableCollection<IClientDatabaseCollection> ClientDatabaseCollectionList { get; set; }
    }
}
