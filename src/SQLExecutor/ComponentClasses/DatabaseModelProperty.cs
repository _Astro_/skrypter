﻿using BaseMVVM;
using InterfaceToDatabase;
using System.Collections.ObjectModel;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Database model porperty model class
    /// </summary>
    public class DatabaseModelProperty : ObservableObject, IDatabaseModelProperty
    {
        private string _propertyValue;

        private IDisplayedEnum _displayedItem;

        private int _currentSelectedIndex;

        private ObservableCollection<IDisplayedEnum> _enumList;

        public DatabaseModelPropertyEnum Property { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue
        {
            get
            {
                return _propertyValue;
            }
            set
            {
                Set(ref _propertyValue, value);
            }
        }

        public string WatermarkText { get; set; }

        public bool IsTextBox { get; set; }

        public bool IsPasswordTextBox { get; set; }

        public bool IsComboBox { get; set; }

        public ObservableCollection<IDisplayedEnum> EnumList
        {
            get
            {
                return _enumList;
            }
            set
            {
                Set(ref _enumList, value);
            }
        }

        public IDisplayedEnum DisplayedItem
        {
            get
            {
                return _displayedItem;
            }
            set
            {
                Set(ref _displayedItem, value);
            }
        }

        public int CurrentSelectedIndex
        {
            get
            {
                return _currentSelectedIndex;
            }
            set
            {
                Set(ref _currentSelectedIndex, value);
            }
        }
    }
}