﻿using BaseMVVM;
using System.Collections.ObjectModel;

namespace SQLExecutor.ComponentClasses
{
    /// <summary>
    /// Action expander model class
    /// </summary>
    public class ActionExpander : ObservableObject, IActionExpander
    {
        private ObservableCollection<IDatabaseModelProperty> _staticDatabaseModelPropertyList;

        private ObservableCollection<IDatabaseModelProperty> _dynamicDatabaseModelPropertyList;

        public string Header { get; set; }

        public bool IsExpanded { get; set; }

        public ObservableCollection<IDatabaseModelProperty> StaticDatabaseModelPropertyList
        {
            get
            {
                if (_staticDatabaseModelPropertyList == null)
                    _staticDatabaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();
                return _staticDatabaseModelPropertyList;
            }
            set
            {
                Set(ref _staticDatabaseModelPropertyList, value);
            }
        }

        public ObservableCollection<IDatabaseModelProperty> DynamicDatabaseModelPropertyList
        {
            get
            {
                if (_dynamicDatabaseModelPropertyList == null)
                    _dynamicDatabaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();
                return _dynamicDatabaseModelPropertyList;
            }
            set
            {
                Set(ref _dynamicDatabaseModelPropertyList, value);
            }
        }

        public ObservableCollection<IInterfaceButton> ActionButtonList { get; set; }
    }
}