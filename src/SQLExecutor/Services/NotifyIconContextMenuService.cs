﻿using LocalizationData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using static System.Windows.Forms.Menu;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Notify icon context menu service class
    /// </summary>
    public static class NotifyIconContextMenuService
    {
        private static Window _window;

        /// <summary>
        /// Return notify icon context menu
        /// </summary>
        /// <returns></returns>
        public static ContextMenu GetNotifyIconNontextMenu(Window window)
        {
            _window = window;

            var contextMenu = new ContextMenu();

            var menuItemCollection = new MenuItemCollection(contextMenu);

            menuItemCollection.Add(ResourceStrings.ButtonShow, ShowCommand);
            menuItemCollection.Add(ResourceStrings.ButtonExitApplication, ExitCommand);            

            return contextMenu;
        }

        /// <summary>
        /// Schow window command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ShowCommand(object sender, EventArgs e)
        {
            _window.Show();
            _window.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Exit application command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ExitCommand(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
