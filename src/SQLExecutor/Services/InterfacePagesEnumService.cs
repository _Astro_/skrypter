﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Interface pages enumeration service class
    /// </summary>
    public static class InterfacePagesEnumService
    {
        /// <summary>
        /// Return collection of interface pages enum descriptions
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<string> GetInterfacePagesCollection()
        {
            ObservableCollection<string> interfacePagesCollection = new ObservableCollection<string>();

            foreach (InterfacePagesEnum interfacePage in Enum.GetValues(typeof(InterfacePagesEnum)))
            {
                interfacePagesCollection.Add(interfacePage.ToName());
            }

            return interfacePagesCollection;
        }

        /// <summary>
        /// Return enum value from enum sring description
        /// </summary>
        /// <param name="interfacePageEnumString"></param>
        /// <returns></returns>
        public static InterfacePagesEnum GetEnumFromString(string interfacePageEnumString)
        {
            var enumValue = (from InterfacePagesEnum interfacePagesEnum in Enum.GetValues(typeof(InterfacePagesEnum))
                             where interfacePagesEnum.ToName() == interfacePageEnumString
                             select interfacePagesEnum).FirstOrDefault();

            return enumValue;
        }
    }
}
