﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;

namespace SQLExecutor.Services
{
    /// <summary>
    /// WPF navigaion service class
    /// </summary>
    public class NavigationService : INavigationService
    {
        private readonly Frame _frame;

        /// <summary>
        /// Class ctor
        /// </summary>
        /// <param name="frame"> Input frame </param>
        public NavigationService(Frame frame)
        {
            this._frame = frame;
        }

        public bool CanGoBack()
        {
            if (_frame.BackStack != null)
                return true;
            return false;
        }

        public bool CanGoForward()
        {
            if (_frame.ForwardStack != null)
                return true;
            return false;
        }

        public void GoForward()
        {
            _frame.GoForward();
        }

        public void GoBack()
        {
            _frame.GoBack();
        }

        public bool Navigate<T>(object parameter = null)
        {
            var type = typeof(T);
            return Navigate(type, parameter);
        }

        public bool Navigate(Type sourceType, object parameter = null)
        {
            var sourceInstance = Activator.CreateInstance(sourceType);
            return _frame.Navigate(sourceInstance, parameter);
        }

        public bool Navigate(string pageName)
        {
            var objectType = Assembly.GetExecutingAssembly().GetTypes().SingleOrDefault(
                a => a.Name.Equals(pageName));
            if (objectType == null) return false;

            var pageInstance = Activator.CreateInstance(objectType);
            return _frame.Navigate(pageInstance);
        }
    }
}