﻿using BaseClientData;
using InterfaceToDatabase;
using SQLExecutor.ComponentClasses;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Interface of database model service class
    /// </summary>
    public interface IDatabaseModelService
    {
        List<IDatabaseModel> GetAllDatabaseModel();

        List<IDatabaseModel> GetAllReportsModel();

        List<IDatabaseModel> GetAllDatabaseModelOfType(DatabaseType databaseType);

        IDatabaseModel GetDatabaseModelFromActionExpander(IActionExpander actionExpander);

        bool SaveDatabaseModel(IActionExpander actionExpander);

        bool CheckConnectionToDatabase(IActionExpander actionExpander);

        void DeleteSelectedDatabases(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList);

        void DeleteDatabasesForSelectedClient(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList, string selectedClientName);

        bool UpdateDatabaseModel(IActionExpander actionExpander, string originalDatabaseModelID);
    }
}