﻿using BaseClientData;
using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase;
using InterfaceToDatabase.InterfceToDatabaseFactory;
using InterfaceToDatabase.Repository;
using InterfaceToDatabase.Repository.SQLExecutorUnitOfWorkFactory;
using InterfaceToDatabase.SQLExecutor;
using LocalizationData;
using Reports;
using SQLExecutor.ComponentClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Tools;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Database model service
    /// </summary>
    public class DatabaseModelService : AResultService, IDatabaseModelService
    {
        private IActionExpander _actionExpander;
        private IUnitOfWork _unitOfWork;
        private IInterfaceToDatabaseFactory _interfaceToDatabaseFactory;
        private IInterfaceToDatabase _interfaceToDatabase;

        /// <summary>
        /// Default ctor
        /// </summary>
        public DatabaseModelService()
        {
            _interfaceToDatabaseFactory = GetInterfaceToDatabaseFactory();
            _unitOfWork = GetUnitOfWork();
        }

        #region IDatabaseModelService

        /// <summary>
        /// Return list of all database models from repository
        /// </summary>
        /// <returns></returns>
        public List<IDatabaseModel> GetAllDatabaseModel()
        {
            List<IDatabaseModel> databaseModelList = new List<IDatabaseModel>();

            databaseModelList.AddRange(_unitOfWork.MSSQLDatabaseModel.GetAll());
            databaseModelList.AddRange(_unitOfWork.SQLiteDatabaseModel.GetAll());

            return databaseModelList;
        }

        /// <summary>
        /// Return list of all reports models from repository
        /// </summary>
        /// <returns></returns>
        public List<IDatabaseModel> GetAllReportsModel()
        {
            List<IDatabaseModel> databaseModelList = new List<IDatabaseModel>();

            databaseModelList.AddRange(_unitOfWork.SSRSModelRepository.GetAll());

            return databaseModelList;
        }

        /// <summary>
        ///  Return list of all database of type from repository
        /// </summary>
        /// <param name="databaseType"></param>
        /// <returns></returns>
        public List<IDatabaseModel> GetAllDatabaseModelOfType(DatabaseType databaseType)
        {
            List<IDatabaseModel> databaseModelList = new List<IDatabaseModel>();

            switch (databaseType)
            {
                case DatabaseType.MSSQL:
                    databaseModelList.AddRange(_unitOfWork.MSSQLDatabaseModel.GetAll());
                    break;

                case DatabaseType.SQLite:
                    databaseModelList.AddRange(_unitOfWork.SQLiteDatabaseModel.GetAll());
                    break;
                default:
                    break;
            };
                                    
            return databaseModelList;
        }

        /// <summary>
        /// Return current database model from action expander
        /// </summary>
        /// <param name="actionExpander"> Input action expander </param>
        /// <returns></returns>
        public IDatabaseModel GetDatabaseModelFromActionExpander(IActionExpander actionExpander)
        {
            if (actionExpander == null)
                ReportFail(ResourceStrings.ActionExpanderNullObject);

            _actionExpander = actionExpander;

            var databaseType = GetDatabaseTypeFromStringValue(DatabaseTypeProperty);
            IDatabaseModel databaseModel = GetNewDatabaseModelByType(databaseType);

            SetDatabaseModelProperties(actionExpander, databaseModel);

            return databaseModel;
        }

        /// <summary>
        /// Save database model in repository
        /// </summary>
        /// <param name="actionExpander"></param>
        /// <returns></returns>
        public bool SaveDatabaseModel(IActionExpander actionExpander)
        {
            IDatabaseModel databaseModel = GetDatabaseModelFromActionExpander(actionExpander);

            if (!ValidateDatabaseModel(databaseModel))
                return false;

            if (!SaveDatabaseModelInRepository(databaseModel))
                return false;

            return true;
        }

        /// <summary>
        /// Check connection to database
        /// </summary>
        /// <param name="actionExpander"></param>
        /// <returns></returns>
        public bool CheckConnectionToDatabase(IActionExpander actionExpander)
        {
            IDatabaseModel databaseModel = GetDatabaseModelFromActionExpander(actionExpander);

            if (!ValidateDatabaseModel(databaseModel))
                return false;

            return CheckConnection(databaseModel);
        }

        /// <summary>
        /// Delete all selected databases models
        /// </summary>
        /// <param name="clientDatabaseCollectionList"></param>
        public void DeleteSelectedDatabases(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList)
        {
            foreach (IClientDatabaseCollection clientDatabaseCollection in clientDatabaseCollectionList)
            {
                DeleteSelectedDatabaseForClient(clientDatabaseCollection);
            }
        }

        /// <summary>
        /// Delete all databases models for selected client
        /// </summary>
        /// <param name="clientDatabaseCollectionList"></param>
        /// <param name="selectedClientName"></param>
        public void DeleteDatabasesForSelectedClient(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList, string selectedClientName)
        {
            if (selectedClientName != null && !string.IsNullOrEmpty(selectedClientName))
            {
                var clientDatabaseCollection = clientDatabaseCollectionList.Where(c => c.Header == selectedClientName).FirstOrDefault();

                DeleteDatabasesForClient(clientDatabaseCollection);
            }
        }

        /// <summary>
        /// Update edited database model
        /// </summary>
        /// <param name="actionExpander"></param>
        /// <param name="databaseModel"></param>
        /// <param name="originalDatabaseModelID"></param>
        /// <returns></returns>
        public bool UpdateDatabaseModel(IActionExpander actionExpander, string originalDatabaseModelID)
        {
            IDatabaseModel newDatabaseModel = GetDatabaseModelFromActionExpander(actionExpander);

            if (!ValidateDatabaseModel(newDatabaseModel))
                return false;

            newDatabaseModel.ID = originalDatabaseModelID;

            IDatabaseModel originalDatabaseModel = GetOriginalDatabaseModel(originalDatabaseModelID);

            if (!ObjectService.CompareDatabaseModelObjects<IDatabaseModel>(originalDatabaseModel, newDatabaseModel))
            {
                switch (newDatabaseModel.DatabaseType)
                {
                    case DatabaseType.MSSQL:
                        return UpdateMSSQLDatabaseModel(newDatabaseModel, originalDatabaseModel);

                    case DatabaseType.SSRS:
                        return UpdateSSRSModel(newDatabaseModel, originalDatabaseModel);

                    case DatabaseType.SQLite:
                        return UpdateSQLiteDatabaseModel(newDatabaseModel, originalDatabaseModel);

                    default:
                        return false;
                }
            }
            else
            {
                return ReportFail(ResourceStrings.SaveChangesNoChanges);
            }
        }

        #endregion IDatabaseModelService

        /// <summary>
        /// Update MSSQL database model
        /// </summary>
        /// <param name="newDatabaseModel"></param>
        /// <param name="originalDatabaseModel"></param>
        /// <returns></returns>
        private bool UpdateMSSQLDatabaseModel(IDatabaseModel newDatabaseModel, IDatabaseModel originalDatabaseModel)
        {
            if (_unitOfWork.MSSQLDatabaseModel.CheckIfExistsWithOptionalFields((MSSQLDatabaseModel)newDatabaseModel))
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);

            ObjectService.CopyAllProperties<IDatabaseModel>(newDatabaseModel, originalDatabaseModel);

            _unitOfWork.MSSQLDatabaseModel.Update((MSSQLDatabaseModel)originalDatabaseModel);
            _unitOfWork.Commit();

            return true;
        }

        /// <summary>
        /// Update SSRS model
        /// </summary>
        /// <param name="newDatabaseModel"></param>
        /// <param name="originalDatabaseModel"></param>
        /// <returns></returns>
        private bool UpdateSSRSModel(IDatabaseModel newDatabaseModel, IDatabaseModel originalDatabaseModel)
        {
            if (_unitOfWork.SSRSModelRepository.CheckIfExistsWithOptionalFields((SSRSModel)newDatabaseModel))
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);

            ObjectService.CopyAllProperties<IDatabaseModel>(newDatabaseModel, originalDatabaseModel);

            _unitOfWork.SSRSModelRepository.Update((SSRSModel)originalDatabaseModel);
            _unitOfWork.Commit();

            return true;
        }

        /// <summary>
        /// Update SQLite database model
        /// </summary>
        /// <param name="newDatabaseModel"></param>
        /// <param name="originalDatabaseModel"></param>
        /// <returns></returns>
        private bool UpdateSQLiteDatabaseModel(IDatabaseModel newDatabaseModel, IDatabaseModel originalDatabaseModel)
        {
            if (_unitOfWork.SQLiteDatabaseModel.CheckIfExistsWithOptionalFields((SQLiteDatabaseModel)newDatabaseModel))
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);

            ObjectService.CopyAllProperties<IDatabaseModel>(newDatabaseModel, originalDatabaseModel);

            _unitOfWork.SQLiteDatabaseModel.Update((SQLiteDatabaseModel)originalDatabaseModel);
            _unitOfWork.Commit();

            return true;
        }

        /// <summary>
        /// Return database model from repository based on ID
        /// </summary>
        /// <param name="databaseModelID"></param>
        /// <returns></returns>
        private IDatabaseModel GetOriginalDatabaseModel(string databaseModelID)
        {
            if (string.IsNullOrEmpty(databaseModelID))
                return null;

            var mssqlDatabaseModel = _unitOfWork.MSSQLDatabaseModel.GetById(databaseModelID);

            if (mssqlDatabaseModel != null)
                return mssqlDatabaseModel;

            var ssrsModel = _unitOfWork.SSRSModelRepository.GetById(databaseModelID);

            if (ssrsModel != null)
                return ssrsModel;

            var sqliteDatabaseModel = _unitOfWork.SQLiteDatabaseModel.GetById(databaseModelID);

            if (sqliteDatabaseModel != null)
                return sqliteDatabaseModel;

            return null;
        }

        /// <summary>
        /// Delete all databases in collection
        /// </summary>
        /// <param name="clientDatabaseCollection"></param>
        private void DeleteDatabasesForClient(IClientDatabaseCollection clientDatabaseCollection)
        {
            foreach (IDatabaseModel databaseModel in clientDatabaseCollection.ClientDatabaseList)
            {
                switch (databaseModel.DatabaseType)
                {
                    case DatabaseType.MSSQL:
                        DeleteMSSQLDatabaseModel(databaseModel);
                        break;

                    case DatabaseType.SSRS:
                        DeleteSSRSModel(databaseModel);
                        break;

                    case DatabaseType.SQLite:
                        DeleteSQLiteDatabaseModel(databaseModel);
                        break;

                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// Delete all selected databases models for client
        /// </summary>
        /// <param name="clientDatabaseCollection"></param>
        private void DeleteSelectedDatabaseForClient(IClientDatabaseCollection clientDatabaseCollection)
        {
            foreach (IDatabaseModel databaseModel in clientDatabaseCollection.ClientDatabaseList)
            {
                if (databaseModel.IsSelected.HasValue && databaseModel.IsSelected == true)
                {
                    switch (databaseModel.DatabaseType)
                    {
                        case DatabaseType.MSSQL:
                            DeleteMSSQLDatabaseModel(databaseModel);
                            break;

                        case DatabaseType.SSRS:
                            DeleteSSRSModel(databaseModel);
                            break;

                        case DatabaseType.SQLite:
                            DeleteSQLiteDatabaseModel(databaseModel);
                            break;

                        default:
                            return;
                    }
                }
            }
        }

        /// <summary>
        /// Delete SQLite database model from repository
        /// </summary>
        /// <param name="databaseModel"></param>
        private void DeleteSQLiteDatabaseModel(IDatabaseModel databaseModel)
        {
            _unitOfWork.SQLiteDatabaseModel.Delete(databaseModel.ID);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Delete MSSQL database model from repository
        /// </summary>
        /// <param name="databaseModel"></param>
        private void DeleteMSSQLDatabaseModel(IDatabaseModel databaseModel)
        {
            _unitOfWork.MSSQLDatabaseModel.Delete(databaseModel.ID);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// Delete SSRS model from repository
        /// </summary>
        /// <param name="databaseModel"></param>
        private void DeleteSSRSModel(IDatabaseModel databaseModel)
        {
            _unitOfWork.SSRSModelRepository.Delete(databaseModel.ID);
            _unitOfWork.Commit();
        }
        /// <summary>
        /// Return result of checking connection to database from database model
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool CheckConnection(IDatabaseModel databaseModel)
        {
            string connectionString = _interfaceToDatabase.GetConnectionString(databaseModel);
            if (string.IsNullOrEmpty(connectionString))
                return false;

            return _interfaceToDatabase.CheckConnection(connectionString);
        }

        /// <summary>
        /// Save database model by type in repository
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool SaveDatabaseModelInRepository(IDatabaseModel databaseModel)
        {
            switch (databaseModel.DatabaseType)
            {
                case DatabaseType.MSSQL:
                    return SaveMSSQLDatabaseModel(databaseModel);

                case DatabaseType.SSRS:
                    return SaveSSRSModel(databaseModel);

                case DatabaseType.SQLite:
                    return SaveSQLiteDatabaseModel(databaseModel);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Validate database model
        /// </summary>
        /// <param name="databaseModel"> Database model to validate </param>
        /// <returns></returns>
        private bool ValidateDatabaseModel(IDatabaseModel databaseModel)
        {
            _interfaceToDatabase = _interfaceToDatabaseFactory.GetInterfaceToDatabase(databaseModel);

            if (_interfaceToDatabase == null)
                return ReportFail(ResourceStrings.InvalidDatabaseModelUndefinedType);

            if (!_interfaceToDatabase.ValidateDatabaseModel(databaseModel))
                return false;

            return true;
        }

        /// <summary>
        /// Save MSSQL database model in repository
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool SaveMSSQLDatabaseModel(IDatabaseModel databaseModel)
        {
            MSSQLDatabaseModel mssqlDatabaseModel = (MSSQLDatabaseModel)databaseModel;

            if (!_unitOfWork.MSSQLDatabaseModel.CheckIfExists(mssqlDatabaseModel))
            {
                _unitOfWork.MSSQLDatabaseModel.Add(mssqlDatabaseModel);
                _unitOfWork.Commit();
                return true;
            }
            else
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);
        }

        /// <summary>
        /// Save SSRS model in repository
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool SaveSSRSModel(IDatabaseModel databaseModel)
        {
            SSRSModel ssrsModel = (SSRSModel)databaseModel;

            if (!_unitOfWork.SSRSModelRepository.CheckIfExists(ssrsModel))
            {
                _unitOfWork.SSRSModelRepository.Add(ssrsModel);
                _unitOfWork.Commit();
                return true;
            }
            else
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);
        }

        /// <summary>
        /// Save SQLite database in repository
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool SaveSQLiteDatabaseModel(IDatabaseModel databaseModel)
        {
            SQLiteDatabaseModel sqliteDatabaseModel = (SQLiteDatabaseModel)databaseModel;

            if (!_unitOfWork.SQLiteDatabaseModel.CheckIfExists(sqliteDatabaseModel))
            {
                _unitOfWork.SQLiteDatabaseModel.Add(sqliteDatabaseModel);
                _unitOfWork.Commit();
                return true;
            }
            else
                return ReportFail(ResourceStrings.DatabaseModelAlreadyExists);
        }

        /// <summary>
        /// Set database model properties to current database model
        /// </summary>
        private void SetDatabaseModelProperties(IActionExpander actionExpander, IDatabaseModel databaseModel)
        {
            if (databaseModel == null) return;

            switch (databaseModel.DatabaseType)
            {
                case DatabaseType.UNDEFINED:
                    return;

                case DatabaseType.MSSQL:
                    SetMSSQLDatabaseModelProperties(databaseModel);
                    return;

                case DatabaseType.SSRS:
                    SetSSRSModelProperties(databaseModel);
                    return;

                case DatabaseType.SQLite:
                    SetSQLiteDatabaseModelProperties(databaseModel);
                    return;

                default:
                    return;
            }
        }

        /// <summary>
        /// Database type string value in action expander
        /// </summary>
        private string DatabaseTypeProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.StaticDatabaseModelPropertyList.Where(s => s.Property ==
                DatabaseModelPropertyEnum.DatabaseType).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.DisplayedItem.Value;
                return null;
            }
        }

        /// <summary>
        /// Client name string value in action expander
        /// </summary>
        private string ClientNameProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.ClientName).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Server address string value in action expander
        /// </summary>
        private string ServerAddressProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.ServerAddress).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Database name string value in action expander
        /// </summary>
        private string DatabaseNameProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.DatabaseName).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Database alias name string value in action expander
        /// </summary>
        private string DatabaseAliasNameProperty
        {
            get
            {
                var databaseModelproperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.DatabaseAliasName).FirstOrDefault();

                if (databaseModelproperty != null)
                    return databaseModelproperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// User name string value in action expander
        /// </summary>
        private string UserNameProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.UserName).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Password string value in action expander
        /// </summary>
        private string PasswordProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.Password).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Database path string value in action expander
        /// </summary>
        private string DatabasePathProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.DatabasePath).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.PropertyValue;
                return null;
            }
        }

        /// <summary>
        /// Connection timeout string value in action expander
        /// </summary>
        private string ConnectionTimeoutProperty
        {
            get
            {
                var databaseModelProperty = _actionExpander.DynamicDatabaseModelPropertyList.Where(d => d.Property ==
                DatabaseModelPropertyEnum.ConnectionTimeout).FirstOrDefault();

                if (databaseModelProperty != null)
                    return databaseModelProperty.DisplayedItem.Value;
                return null;
            }
        }

        /// <summary>
        /// Set database model properties to SQLite database model
        /// </summary>
        private void SetSQLiteDatabaseModelProperties(IDatabaseModel databaseModel)
        {
            databaseModel.ClientName = ClientNameProperty;
            databaseModel.DatabaseName = DatabaseNameProperty;
            databaseModel.DatabaseAliasName = DatabaseAliasNameProperty;
            databaseModel.Password = PasswordProperty;
            databaseModel.DatabasePath = DatabasePathProperty;
        }

        /// <summary>
        /// Set database model properties to MSSQL database model
        /// </summary>
        private void SetMSSQLDatabaseModelProperties(IDatabaseModel databaseModel)
        {
            databaseModel.ClientName = ClientNameProperty;
            databaseModel.DatabaseName = DatabaseNameProperty;
            databaseModel.DatabaseAliasName = DatabaseAliasNameProperty;
            databaseModel.ServerAddress = ServerAddressProperty;
            databaseModel.UserName = UserNameProperty;
            databaseModel.Password = PasswordProperty;
            databaseModel.ConnectionTimeout = string.IsNullOrEmpty(ConnectionTimeoutProperty) ? 1 : int.Parse(ConnectionTimeoutProperty);
        }

        /// <summary>
        /// Set database model properties to SSRS model
        /// </summary>
        /// <param name="databaseModel"></param>
        private void SetSSRSModelProperties(IDatabaseModel databaseModel)
        {
            databaseModel.ClientName = ClientNameProperty;
            databaseModel.DatabaseName = DatabaseNameProperty;
            databaseModel.DatabaseAliasName = DatabaseAliasNameProperty;
            databaseModel.ServerAddress = ServerAddressProperty;
            databaseModel.UserName = UserNameProperty;
            databaseModel.Password = PasswordProperty;
        }

        /// <summary>
        /// Return database type from string value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private DatabaseType GetDatabaseTypeFromStringValue(string value)
        {
            var selectedDatabaseType = (from DatabaseType dbTypes in Enum.GetValues(typeof(DatabaseType))
                                        where dbTypes.ToString() == value
                                        select dbTypes).FirstOrDefault();
            return selectedDatabaseType;
        }

        /// <summary>
        /// Return new database model by type
        /// </summary>
        /// <param name="databaseType"></param>
        /// <returns></returns>
        private IDatabaseModel GetNewDatabaseModelByType(DatabaseType databaseType)
        {
            switch (databaseType)
            {
                case DatabaseType.UNDEFINED:
                    return null;

                case DatabaseType.MSSQL:
                    return new MSSQLDatabaseModel();

                case DatabaseType.SSRS:
                    return new SSRSModel();

                case DatabaseType.SQLite:
                    return new SQLiteDatabaseModel();

                default:
                    return null;
            }
        }

        /// <summary>
        /// Return new interface to database factory instance
        /// </summary>
        /// <returns></returns>
        private IInterfaceToDatabaseFactory GetInterfaceToDatabaseFactory()
        {
            return new InterfaceToDatabaseFactory();
        }

        /// <summary>
        /// Return new SQLExecutor unit of work instance
        /// </summary>
        /// <returns></returns>
        private IUnitOfWork GetUnitOfWork()
        {
            return UnitOfWorkFactory.GetUnitOfWork(SQLExecutorDatabase.GetConnectionString());
        }
    }
}