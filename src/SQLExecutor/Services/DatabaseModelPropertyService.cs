﻿using InterfaceToDatabase;
using LocalizationData;
using SQLExecutor.ComponentClasses;
using System.Collections.ObjectModel;
using System.Linq;

namespace SQLExecutor.Services
{
    public static class DatabaseModelPropertyService
    {
        public static ObservableCollection<IDatabaseModelProperty> GetMSSQLPropertyList()
        {
            ObservableCollection<IDatabaseModelProperty> databaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();

            databaseModelPropertyList.Add(ClientNameProperty());
            databaseModelPropertyList.Add(ServerAddressProperty());
            databaseModelPropertyList.Add(DatabaseNameProperty());
            databaseModelPropertyList.Add(DatabaseAliasNameProperty());
            databaseModelPropertyList.Add(UserNameProperty());
            databaseModelPropertyList.Add(PasswordProperty());
            databaseModelPropertyList.Add(ConnectionTimeoutProperty());

            return databaseModelPropertyList;
        }

        public static ObservableCollection<IDatabaseModelProperty> GetSQLitePropertyList()
        {
            ObservableCollection<IDatabaseModelProperty> databaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();

            databaseModelPropertyList.Add(ClientNameProperty());
            databaseModelPropertyList.Add(DatabaseNameProperty());
            databaseModelPropertyList.Add(DatabaseAliasNameProperty());
            databaseModelPropertyList.Add(PasswordProperty());
            databaseModelPropertyList.Add(DatabasePathProperty());

            return databaseModelPropertyList;
        }

        public static ObservableCollection<IDatabaseModelProperty> GetSSRSPropertyList()
        {
            ObservableCollection<IDatabaseModelProperty> databaseModelPropertyList = new ObservableCollection<IDatabaseModelProperty>();

            databaseModelPropertyList.Add(ClientNameProperty());
            databaseModelPropertyList.Add(DatabaseAliasNameProperty());
            databaseModelPropertyList.Add(ServerAddressURLProperty());
            databaseModelPropertyList.Add(DatabaseFolderNameProperty());
            databaseModelPropertyList.Add(UserNameProperty());
            databaseModelPropertyList.Add(PasswordProperty());           

            return databaseModelPropertyList;
        }

        /// <summary>
        /// Return database model client name property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty ClientNameProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.ClientName,
                PropertyName = ResourceStrings.PropertyClientName,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequired,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model server address property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty ServerAddressProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.ServerAddress,
                PropertyName = ResourceStrings.PropertyServerAddress,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequiredServerAddress,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        private static IDatabaseModelProperty ServerAddressURLProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.ServerAddress,
                PropertyName = ResourceStrings.PropertyServerAddressURL,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequiredField,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model database name property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty DatabaseNameProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.DatabaseName,
                PropertyName = ResourceStrings.PropertyDatabaseName,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequired,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model database name property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty DatabaseFolderNameProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.DatabaseName,
                PropertyName = ResourceStrings.PropertyDatabaseFolderName,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequiredField,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model database alias name property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty DatabaseAliasNameProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.DatabaseAliasName,
                PropertyName = ResourceStrings.PropertyDatabaseAliasName,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkOptionalProperty,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model user name property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty UserNameProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.UserName,
                PropertyName = ResourceStrings.PropertyUserName,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequiredField,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model password property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty PasswordProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.Password,
                PropertyName = ResourceStrings.PropertyPassword,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkRequired,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = true,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model database path property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty DatabasePathProperty()
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.DatabasePath,
                PropertyName = ResourceStrings.PropertyDatabasePath,
                PropertyValue = string.Empty,
                WatermarkText = ResourceStrings.WatermarkOnlySQLite,
                IsComboBox = false,
                IsTextBox = true,
                IsPasswordTextBox = false,
                DisplayedItem = new DisplayedEnum()
            };
        }

        /// <summary>
        /// Return database model connection timeout property component class
        /// </summary>
        /// <returns></returns>
        private static IDatabaseModelProperty ConnectionTimeoutProperty(int currentSelectedIndex = 0)
        {
            return new DatabaseModelProperty()
            {
                Property = DatabaseModelPropertyEnum.ConnectionTimeout,
                PropertyName = ResourceStrings.PropertyConnectionTimeout,
                PropertyValue = string.Empty,
                IsComboBox = true,
                IsTextBox = false,
                IsPasswordTextBox = false,
                DisplayedItem = ConnectionTimeoutEnumList.GetEnumList().ElementAt(currentSelectedIndex),
                CurrentSelectedIndex = currentSelectedIndex,
                EnumList = ConnectionTimeoutEnumList.GetEnumList()
            };
        }
    }
}