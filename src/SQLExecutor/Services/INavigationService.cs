﻿namespace SQLExecutor.Services
{
    /// <summary>
    /// Interface of navigation service
    /// </summary>
    public interface INavigationService
    {
        bool CanGoBack();

        bool CanGoForward();

        void GoForward();

        void GoBack();

        bool Navigate(string viewName);
    }
}