﻿using ErrorsAndExceptions;
using InterfaceToDatabase.SQLExecutor;
using LocalizationData;
using Scripts;
using SQLExecutor.ComponentClasses;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TreeViewItems;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Open dialog service fortreeViewItem collection
    /// </summary>
    public class OpenDialogService : AFileService, IOpenDialogService
    {
        private IMessageBoxService _messageBoxService;

        /// <summary>
        /// Default ctor
        /// </summary>
        public OpenDialogService()
        {
            _messageBoxService = GetMessageBoxService();
        }

        /// <summary>
        /// Return treeViewItem collection from open file dialog
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<ITreeViewItem> AddCollectionFromFilesDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Title = ResourceStrings.AddSelectedFiles;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
                return GetCollectionFromOpenFileDialog(openFileDialog);
            return null;
        }

        /// <summary>
        /// Return treeViewItem from selected directory path
        /// </summary>
        /// <returns></returns>
        public ITreeViewItem AddCollectionFromFolderDialog()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = ResourceStrings.AddSelectedFolder;
            folderBrowserDialog.ShowNewFolderButton = false;

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                return GetCollectionFromFolderBrowserDialog(folderBrowserDialog);
            return null;
        }

        /// <summary>
        /// Return treeViewItem from clipboard
        /// </summary>
        /// <returns></returns>
        public ITreeViewItem GetTreeViewItemFromClipboard()
        {
            if (System.Windows.Clipboard.ContainsText())
            {
                return new TreeViewItems.TreeViewItem()
                {
                    Name = string.Format("{0}_{1}_{2}",
                    ResourceStrings.ClipboardScript, DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()),
                    FullPath = null,
                    LastModified = DateTime.Now,
                    Type = FileAttributes.Normal,
                    Text = System.Windows.Clipboard.GetText(),
                    IsChecked = false,
                    Children = null,
                    Parent = null,
                    IsFromClipboard = true
                };
            }
            else
            {
                _messageBoxService.DisplayMessage(ResourceStrings.EmptyClipboard);
                return null;
            }
        }

        /// <summary>
        /// Return treeViewItem collection from drop event args
        /// </summary>
        /// <param name="dragEventArgs"></param>
        /// <returns></returns>
        public ObservableCollection<ITreeViewItem> GetCollectionFromDropEventArg(System.Windows.DragEventArgs dragEventArgs)
        {
            if (SetDragDropCopyEffectsIfAvailable(dragEventArgs))
            {
                ObservableCollection<ITreeViewItem> treeViewItemCollection = new ObservableCollection<ITreeViewItem>();
                IScriptService scriptService = GetScriptService();

                foreach (string patchToSource in (string[])(dragEventArgs.Data.GetData(System.Windows.DataFormats.FileDrop)))
                {
                    FileInfo fileInfo = new FileInfo(patchToSource);

                    if (IsDirectory(fileInfo))
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(fileInfo.FullName);

                        var scriptModelCollection = BuildTreeViewCollection(directoryInfo, scriptService);
                        if (scriptModelCollection != null)
                            treeViewItemCollection.Add(scriptModelCollection);
                    }
                    else
                    {
                        var treeViewItem = GetTreeViewItemFromFileInfo(fileInfo, scriptService);
                        if (treeViewItem != null)
                            treeViewItemCollection.Add(treeViewItem);
                    }
                }
                return treeViewItemCollection;
            }

            return null;
        }

        /// <summary>
        /// Return collection of treeViewItems from open file dialog
        /// </summary>
        /// <param name="openFileDialog"></param>
        /// <returns></returns>
        private ObservableCollection<ITreeViewItem> GetCollectionFromOpenFileDialog(OpenFileDialog openFileDialog)
        {
            ObservableCollection<ITreeViewItem> treeViewItemCollection = new ObservableCollection<ITreeViewItem>();
            IScriptService scriptService = GetScriptService();

            foreach (string filePath in openFileDialog.FileNames)
            {
                FileInfo fileInfo = new FileInfo(filePath);
                ITreeViewItem treeViewItem = GetTreeViewItemFromFileInfo(fileInfo, scriptService);
                if (treeViewItem != null)
                    treeViewItemCollection.Add(treeViewItem);
            }

            return treeViewItemCollection;
        }

        /// <summary>
        /// Return treeViewItem with sub collection from folder browser dialog
        /// </summary>
        /// <param name="folderBrowserDialog"></param>
        /// <returns></returns>
        private ITreeViewItem GetCollectionFromFolderBrowserDialog(FolderBrowserDialog folderBrowserDialog)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(folderBrowserDialog.SelectedPath);
            IScriptService scriptService = GetScriptService();

            ITreeViewItem treeViewItemCollection = BuildTreeViewCollection(directoryInfo, scriptService);

            return treeViewItemCollection;
        }

        /// <summary>
        /// Return treeViewItem from file info
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="scriptService"></param>
        /// <returns></returns>
        private ITreeViewItem GetTreeViewItemFromFileInfo(FileInfo fileInfo, IScriptService scriptService, ITreeViewItem parentItem = null)
        {
            IScriptModel scriptModel = scriptService.GetScriptModel(fileInfo);

            if (scriptModel == null)
            {
                GetAndDisplayError();
                return null;
            }

            ITreeViewItem treeViewItem = new TreeViewItems.TreeViewItem(scriptModel);
            treeViewItem.Children = new ObservableCollection<ITreeViewItem>();
            treeViewItem.Parent = parentItem;

            return treeViewItem;
        }

        /// <summary>
        /// Return recursive treeViewItem class with children from directory path
        /// </summary>
        /// <param name="directoryInfo"></param>
        /// <param name="scriptService"></param>
        /// <returns></returns>
        private ITreeViewItem BuildTreeViewCollection(DirectoryInfo directoryInfo, IScriptService scriptService, ITreeViewItem parentItem = null)
        {
            FileInfo parentFileInfo = new FileInfo(directoryInfo.FullName);
            ITreeViewItem treeViewItem = GetTreeViewItemFromFileInfo(parentFileInfo, scriptService, parentItem);

            if (treeViewItem == null) return treeViewItem;

            foreach (var directory in directoryInfo.GetDirectories())
            {
                var subtreeViewItem = BuildTreeViewCollection(directory, scriptService, treeViewItem);

                if (subtreeViewItem != null)
                    treeViewItem.Children.Add(subtreeViewItem);
            }

            foreach (var fileInfo in directoryInfo.GetFiles())
            {
                ITreeViewItem fileScriptModel = GetTreeViewItemFromFileInfo(fileInfo, scriptService, treeViewItem);
                if (fileScriptModel != null)
                    treeViewItem.Children.Add(fileScriptModel);
            }

            treeViewItem.Children = SortChildren(treeViewItem);

            return treeViewItem;
        }

        /// <summary>
        /// Return children collection sorted by name
        /// </summary>
        /// <param name="treeViewItem"></param>
        /// <returns></returns>
        private ObservableCollection<ITreeViewItem> SortChildren(ITreeViewItem treeViewItem)
        {
            if (treeViewItem.Children != null)
            {
                var sortedList = (from scriptModel in treeViewItem.Children
                                  select scriptModel).OrderBy(t => t.Name).ToList();

                ObservableCollection<ITreeViewItem> sortedCollection = new ObservableCollection<ITreeViewItem>();

                foreach (ITreeViewItem scriptModel in sortedList)
                    sortedCollection.Add(scriptModel);

                return sortedCollection;
            }
            else
                return null;
        }

        /// <summary>
        /// Return true for file drop and set drag drop effects
        /// </summary>
        /// <param name="dragEventArgs"></param>
        private bool SetDragDropCopyEffectsIfAvailable(System.Windows.DragEventArgs dragEventArgs)
        {
            if (dragEventArgs.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                dragEventArgs.Effects = System.Windows.DragDropEffects.Copy;
                return true;
            }
            else
            {
                dragEventArgs.Effects = System.Windows.DragDropEffects.None;
                return false;
            }
        }

        /// <summary>
        /// Return script service instance with user application settings
        /// </summary>
        /// <returns></returns>
        private ScriptService GetScriptService()
        {
            ISQLExecutorSettingsService sqlExecutorSettingsService = new SQLExecutorSettingsService();
            ISQLExecutorSettings sqlExecutorSettings = sqlExecutorSettingsService.GetSQLExecutorSettings();

            return new ScriptService(sqlExecutorSettings);
        }

        /// <summary>
        /// Get error from error service and display
        /// Error string is cleared in service after displayed
        /// </summary>
        private void GetAndDisplayError()
        {
            var errorString = ErrorsAndExceptionsService.Instance.GetException();

            _messageBoxService.DisplayError(errorString);

            ErrorsAndExceptionsService.Instance.ClearErrorsAndExceptions();
        }

        /// <summary>
        /// Return message box service instance
        /// </summary>
        /// <returns></returns>
        private IMessageBoxService GetMessageBoxService()
        {
            return new MessageBoxService();
        }
    }
}