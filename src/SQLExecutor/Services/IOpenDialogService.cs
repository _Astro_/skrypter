﻿using Scripts;
using SQLExecutor.ComponentClasses;
using System.Collections.ObjectModel;
using System.Windows;
using TreeViewItems;

namespace SQLExecutor.Services
{
    public interface IOpenDialogService
    {
        ObservableCollection<ITreeViewItem> AddCollectionFromFilesDialog();

        ITreeViewItem AddCollectionFromFolderDialog();

        ITreeViewItem GetTreeViewItemFromClipboard();

        ObservableCollection<ITreeViewItem> GetCollectionFromDropEventArg(DragEventArgs dragEventArgs);
    }
}