﻿using System.ComponentModel;

namespace SQLExecutor.Services
{
    /// <summary>
    /// Enumeration of interface pages
    /// </summary>
    public enum InterfacePagesEnum
    {
        [Description("")]
        SelectInterfacePage,

        //[Description("ICSI SyteLine Interface")]
        //ICSISyteLinePage,

        [Description("MSSQL Server Interface")]
        MSSQLServerPage
    }
}