﻿using System.Windows.Controls;

namespace SQLExecutor.PagesAndViews
{
    /// <summary>
    /// Interaction logic for ICSISyteLinePage.xaml
    /// </summary>
    public partial class ICSISyteLinePage : Page
    {
        public ICSISyteLinePage()
        {
            InitializeComponent();
        }
    }
}