﻿<Page x:Class="SQLExecutor.PagesAndViews.UserSettingsPage"
      xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
      xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
      xmlns:local="clr-namespace:SQLExecutor.PagesAndViews"
      mc:Ignorable="d"
      d:DesignHeight="480" d:DesignWidth="640"
      Title="{Binding Strings.UserSettingsPageTitle}"
      DataContext="{Binding UserSettingsViewModel, Source={StaticResource ViewModelLocator}}">

    <DockPanel x:Name="MainGrid"
               LastChildFill="False">
        <Menu
            x:Name="InterfaceMenuItems"
            DockPanel.Dock="Top"
            Height="Auto"
            Style="{StaticResource MenuCollection}"
            ItemsSource="{Binding MenuItemsCollection}">
            <Menu.ItemContainerStyle>
                <Style
                    TargetType="{x:Type MenuItem}">
                    <Setter Property="Command" Value="{Binding Command}" />
                    <Setter Property="CommandParameter" Value="{Binding CommandParameters}" />
                </Style>
            </Menu.ItemContainerStyle>
            <Menu.ItemTemplate>
                <HierarchicalDataTemplate
                    DataType="{x:Type local:UserSettingsPage}"
                    ItemsSource="{Binding SubMenuItems}">
                    <TextBlock
                        Text="{Binding Header}" />
                </HierarchicalDataTemplate>
            </Menu.ItemTemplate>
        </Menu>
        <ItemsControl
            DockPanel.Dock="Bottom"
            x:Name="ToolboxButtonList"
            ItemsSource="{Binding ToolboxButtonList}"
            BorderBrush="Black"
            BorderThickness="1.5"
            VerticalAlignment="Bottom"
            Height="Auto"
            Width="{Binding ElementName=MainGrid, Path=ActualWidth}">
            <ItemsControl.ItemsPanel>
                <ItemsPanelTemplate>
                    <UniformGrid Columns="3" />
                </ItemsPanelTemplate>
            </ItemsControl.ItemsPanel>
            <!-- ItemTemplate -->
            <ItemsControl.ItemTemplate>
                <DataTemplate>
                    <Button
                        Style="{StaticResource ActionButton}"
                        Command="{Binding Command}"
                        CommandParameter="{Binding CommandParameters}">
                        <TextBlock Text="{Binding Content}"
								   Margin="0.5"
								   TextWrapping="Wrap" />
                    </Button>
                </DataTemplate>
            </ItemsControl.ItemTemplate>
        </ItemsControl>
        <Grid
            DockPanel.Dock="Left"
            x:Name="LeftSideGrid"
            Width="{Binding ElementName=MainGrid, Path=ActualWidth,
			Converter={StaticResource MathConverter},
			ConverterParameter=@VALUE/2}"
            MaxWidth="{Binding Strings.ConstWidthOfDatabasesList}"
            MinWidth="{Binding ElementName=MainGrid, Path=ActualWidth,
			Converter={StaticResource MathConverter},
			ConverterParameter=@VALUE/3}">
            <DockPanel
                VerticalAlignment="Center">
                <Border
                    DockPanel.Dock="Top"
                    Style="{StaticResource RoundedBorder}">
                    <StackPanel
                        Margin="5">
                        <Border
                            Style="{StaticResource RoundedBorder}">
                            <Grid
                                Margin="2.5">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width="*" />
                                </Grid.ColumnDefinitions>
                                <TextBlock
                                    Grid.Column="0"
                                    Margin="2.5"
                                    TextWrapping="WrapWithOverflow"
                                    Text="{Binding Strings.SQLExecutorSettingsDefaultStartingInterface}" />
                                <ComboBox
                                    Grid.Column="1"
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    ItemsSource="{Binding InterfacePagesCollection, Mode=TwoWay}"
                                    SelectedItem="{Binding SQLExecutorSettings.DefaultStartingInterface, UpdateSourceTrigger=PropertyChanged}" />
                            </Grid>
                        </Border>
                        <Border
                            Style="{StaticResource RoundedBorder}">
                            <WrapPanel
                                Margin="2.5"
                                Orientation="Horizontal">
                                <TextBlock
                                    Margin="2.5"
                                    TextWrapping="WrapWithOverflow"
                                    Text="{Binding Strings.SQLExecutorSettingsUseSingleTransaction}" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    IsChecked="{Binding SQLExecutorSettings.UseSingleTransaction}"
                                    Content="Yes" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    Content="No"
                                    IsChecked="{Binding SQLExecutorSettings.UseSingleTransaction,
                                    Converter={StaticResource BoolInvertConverter}}" />
                            </WrapPanel>
                        </Border>
                        <Border
                            Style="{StaticResource RoundedBorder}">
                            <WrapPanel
                                 Margin="2.5"
                                Orientation="Horizontal">
                                <TextBlock
                                    Margin="2.5"
                                    TextWrapping="WrapWithOverflow"
                                    Text="{Binding Strings.SQLExecutorSettingsUseHiddenFilesAndFolders}" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    IsChecked="{Binding SQLExecutorSettings.IncludeHiddenFilesAndFolders}"
                                    Content="Yes" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    Content="No"
                                    IsChecked="{Binding SQLExecutorSettings.IncludeHiddenFilesAndFolders,
                                    Converter={StaticResource BoolInvertConverter}}" />
                            </WrapPanel>
                        </Border>
                        <Border
                            Style="{StaticResource RoundedBorder}">
                            <WrapPanel
                                 Margin="2.5"
                                Orientation="Horizontal">
                                <TextBlock
                                    Margin="2.5"
                                    TextWrapping="WrapWithOverflow"
                                    Text="{Binding Strings.SQLExecutorSettingsUseTemporaryFilesAndFolders}" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    IsChecked="{Binding SQLExecutorSettings.IncludeTemporaryFilesAndFolders}"
                                    Content="Yes" />
                                <RadioButton
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    Content="No"
                                    IsChecked="{Binding SQLExecutorSettings.IncludeTemporaryFilesAndFolders,
                                    Converter={StaticResource BoolInvertConverter}}" />
                            </WrapPanel>
                        </Border>
                        <Border
                            Style="{StaticResource RoundedBorder}">
                            <Grid
                                Margin="2.5">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width="*" />
                                </Grid.ColumnDefinitions>
                                <TextBlock
                                    Grid.Column="0"
                                    Margin="2.5"
                                    TextWrapping="WrapWithOverflow"
                                    Text="{Binding Strings.SQLExecutorSettingsDefaultEncoding}" />
                                <ComboBox
                                    Grid.Column="1"
                                    Margin="2.5"
                                    VerticalContentAlignment="Center"
                                    ItemsSource="{Binding EncodingCollection, Mode=TwoWay}"
                                    SelectedItem="{Binding SQLExecutorSettings.DefaultEncoding, UpdateSourceTrigger=PropertyChanged}" />
                            </Grid>
                        </Border>
                        <UniformGrid
                            Columns="2"
                            FirstColumn="1">
                            <Button
                                Style="{StaticResource ActionButton}"
                                Content="{Binding Strings.ButtonSaveChanges}"
                                Command="{Binding SaveSettingsChangesCommand}" />
                        </UniformGrid>
                    </StackPanel>
                </Border>
            </DockPanel>
        </Grid>
    </DockPanel>
</Page>