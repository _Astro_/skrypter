﻿using System.Windows.Controls;

namespace SQLExecutor.PagesAndViews
{
    /// <summary>
    /// Interaction logic for MSSQLServerPage.xaml
    /// </summary>
    public partial class MSSQLServerPage : Page
    {
        public MSSQLServerPage()
        {
            InitializeComponent();
        }
    }
}