﻿using LocalizationData;
using SQLExecutor.Services;
using System;
using System.Windows;
using System.Windows.Forms;

namespace SQLExecutor.PagesAndViews
{
    /// <summary>
    /// Interaction logic for StartingWindow.xaml
    /// </summary>
    public partial class StartingWindow : Window
    {
        private NotifyIcon _notifyIcon;

        public StartingWindow()
        {
            InitializeComponent();

            _notifyIcon = new NotifyIcon();
            _notifyIcon.Icon = ResourceIcons.MainIcon;
            _notifyIcon.Text = ResourceStrings.StartingWindowTitle;
            _notifyIcon.ContextMenu = NotifyIconContextMenuService.GetNotifyIconNontextMenu(this);
            _notifyIcon.Click += _notifyIcon_Click;
        }

        /// <summary>
        /// Only left-click to minimize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _notifyIcon_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;

            if (me != null && me.Button == MouseButtons.Left)
            {
                this.Show();
                this.WindowState = WindowState.Normal;
            }
        }

        /// <summary>
        /// Change window state method override
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Minimized)
            {
                this.Hide();
                _notifyIcon.Visible = true;
            }

            if (WindowState == System.Windows.WindowState.Normal)
            {
                if (!this.IsVisible)
                    this.Show();

                _notifyIcon.Visible = false;
            }

            base.OnStateChanged(e);
        }
    }
}