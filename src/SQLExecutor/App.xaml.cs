﻿using ErrorsAndExceptions;
using InterfaceToDatabase.SQLExecutor;
using SQLExecutor.PagesAndViews;
using SQLExecutor.Services;
using System.Windows;

namespace SQLExecutor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static NavigationService Navigation;
        public static IMessageBoxService MessageBoxService;
        private ISQLExecutorSettingsService _sqlExecutorSettingsService;
        private ISQLExecutorSettings _sqlExecutorSettings;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            StartingWindow startingWindow = new StartingWindow();
            startingWindow.Show();

            _sqlExecutorSettingsService = GetSQLExecutorSettingsService();
            _sqlExecutorSettings = _sqlExecutorSettingsService.GetSQLExecutorSettings();

            var defaultPageName = InterfacePagesEnumService.GetEnumFromString(_sqlExecutorSettings.DefaultStartingInterface);

            Navigation = new NavigationService(startingWindow.ApplicationFrame);
            MessageBoxService = new MessageBoxService();

            switch (defaultPageName)
            {
                //case InterfacePagesEnum.ICSISyteLinePage:
                //    Navigation.Navigate<ICSISyteLinePage>();
                //    return;

                case InterfacePagesEnum.MSSQLServerPage:
                    Navigation.Navigate<MSSQLServerPage>();
                    return;

                default:
                    Navigation.Navigate<SelectInterfacePage>();
                    return;
            }
        }

        /// <summary>
        /// Return new SQLExecutor settings service instance
        /// </summary>
        /// <returns></returns>
        private ISQLExecutorSettingsService GetSQLExecutorSettingsService()
        {
            return new SQLExecutorSettingsService();
        }
    }
}