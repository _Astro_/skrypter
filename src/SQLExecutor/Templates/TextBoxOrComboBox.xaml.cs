﻿using SQLExecutor.ComponentClasses;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace SQLExecutor.Templates
{
    /// <summary>
    /// Interaction logic for TextBoxOrComboBox.xaml
    /// </summary>
    public partial class TextBoxOrComboBox : UserControl
    {
        private const string INDEX_ZERO = "0";

        /// <summary>
        /// Dependency registration of label property
        /// </summary>
        public static readonly DependencyProperty LabelProperty = DependencyProperty
            .Register("Label",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata("Unnamed Label"));

        /// <summary>
        /// Dependency registration of text property
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty
            .Register("CurrentText",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Dependency registration of watermark text property
        /// </summary>
        public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty
            .Register("WatermarkText",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency registration of textbox visibility property
        /// </summary>
        public static readonly DependencyProperty IsTextBoxVisible = DependencyProperty
            .Register("IsTextBox",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency registration of password textbox visibility property
        /// </summary>
        public static readonly DependencyProperty IsPasswordTextBoxVisible = DependencyProperty
            .Register("IsPasswordTextBox",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency registration of combobox visibility property
        /// </summary>
        public static readonly DependencyProperty IsComboBoxVisible = DependencyProperty
            .Register("IsComboBox",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Dependency registration of item source list property
        /// </summary>
        public static readonly DependencyProperty ItemSourceListProperty = DependencyProperty
            .Register("ItemSourceList",
            typeof(ObservableCollection<IDisplayedEnum>),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(new ObservableCollection<IDisplayedEnum>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Dependency registration of current selected item property
        /// </summary>
        public static readonly DependencyProperty CurrentSelectedItemProperty = DependencyProperty
            .Register("CurrentSelectedItem",
            typeof(IDisplayedEnum),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(new DisplayedEnum(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Dependency registration of current selected index property
        /// </summary>
        public static readonly DependencyProperty CurrentSelectedIndexProperty = DependencyProperty
            .Register("CurrentSelectedIndex",
            typeof(string),
            typeof(TextBoxOrComboBox),
            new FrameworkPropertyMetadata(INDEX_ZERO, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Default ctor
        /// </summary>
        public TextBoxOrComboBox()
        {
            InitializeComponent();
            Root.DataContext = this;
        }

        public string Label
        {
            get
            {
                return (string)GetValue(LabelProperty);
            }
            set
            {
                SetValue(LabelProperty, value);
            }
        }

        public string CurrentText
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public string WatermarkText
        {
            get
            {
                return (string)GetValue(WatermarkTextProperty);
            }
            set
            {
                SetValue(WatermarkTextProperty, value);
            }
        }

        public string IsTextBox
        {
            get
            {
                return (string)GetValue(IsTextBoxVisible);
            }
            set
            {
                SetValue(IsTextBoxVisible, value);
            }
        }

        public string IsPasswordTextBox
        {
            get
            {
                return (string)GetValue(IsPasswordTextBoxVisible);
            }
            set
            {
                SetValue(IsPasswordTextBoxVisible, value);
            }
        }

        public string IsComboBox
        {
            get
            {
                return (string)GetValue(IsComboBoxVisible);
            }
            set
            {
                SetValue(IsComboBoxVisible, value);
            }
        }

        public ObservableCollection<IDisplayedEnum> ItemSourceList
        {
            get
            {
                return (ObservableCollection<IDisplayedEnum>)GetValue(ItemSourceListProperty);
            }
            set
            {
                SetValue(ItemSourceListProperty, value);
            }
        }

        public IDisplayedEnum CurrentSelectedItem
        {
            get
            {
                return (IDisplayedEnum)GetValue(CurrentSelectedItemProperty);
            }
            set
            {
                SetValue(CurrentSelectedItemProperty, value);
            }
        }

        public string CurrentSelectedIndex
        {
            get
            {
                return (string)GetValue(CurrentSelectedIndexProperty);
            }
            set
            {
                SetValue(CurrentSelectedIndexProperty, value);
            }
        }
    }
}