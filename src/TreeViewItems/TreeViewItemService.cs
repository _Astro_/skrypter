﻿using InterfaceToDatabase.SQLExecutor;
using Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TreeViewItems
{
    /// <summary>
    /// TreeViewItem service class
    /// </summary>
    public static class TreeViewItemService
    {
        private static int ZERO = 0;

        /// <summary>
        /// Expand all treeViewItem collection
        /// </summary>
        /// <param name="treeViewItemCollection"></param>
        public static void ExpandAll(ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            if (treeViewItemCollection != null && treeViewItemCollection.Count > ZERO)
                foreach (ITreeViewItem treeViwItem in treeViewItemCollection)
                    Expand(treeViwItem);
        }

        /// <summary>
        /// Collapse all treeViewItem collection
        /// </summary>
        /// <param name="treeViewItemCollection"></param>
        public static void CollapseAll(ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            if (treeViewItemCollection != null && treeViewItemCollection.Count > ZERO)
                foreach (ITreeViewItem treeViewItem in treeViewItemCollection)
                    Collapse(treeViewItem);
        }

        /// <summary>
        /// Change checked status of treeViewItem collection
        /// </summary>
        /// <param name="status"></param>
        /// <param name="treeViewItemCollection"></param>
        public static void ChangeCheckedStatus(bool status, ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            if (treeViewItemCollection != null && treeViewItemCollection.Count > ZERO)
                foreach (ITreeViewItem treeViewItem in treeViewItemCollection)
                    // Status of children is changed due to recursion - standard checked change behavior of TreeViewItem
                    treeViewItem.IsChecked = status;
        }

        /// <summary>
        /// Return list of selected items
        /// </summary>
        /// <param name="treeViewItemCollection"></param>
        /// <returns></returns>
        public static List<IScriptModel> GetListOfAllSelectedItems(ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            List<IScriptModel> scriptModelList = new List<IScriptModel>();
            IScriptService scriptService = GetScriptService();

            if (treeViewItemCollection != null && treeViewItemCollection.Count > ZERO)
                foreach (ITreeViewItem treeViewItem in treeViewItemCollection)
                    GetSelectedScripts(treeViewItem, scriptModelList, scriptService);

            return scriptModelList;
        }

        /// <summary>
        /// Reload all selected scripts with encoding from settings
        /// </summary>
        /// <param name="treeViewItemCollection"></param>
        public static void ReloadWithEncodingFromSettings(ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            if (treeViewItemCollection != null && treeViewItemCollection.Count > ZERO)
                foreach (ITreeViewItem treeViewItem in treeViewItemCollection)
                    ReloadScriptWithEncoding(treeViewItem);
        }

        /// <summary>
        /// Open preview/edit script window
        /// </summary>
        /// <param name="scriptModel"></param>
        public static void PreviewEditScript(IScriptModel scriptModel)
        {
            if (scriptModel == null) return;

            if (CheckIsScriptCurrentOpened(scriptModel) == true) return;

            IScriptEditViewModel scriptEditViewModel = new ScriptEditViewModel();
            ScriptEditViewModelFactory.AddScriptEditViewModel(scriptEditViewModel);
            ScriptEditView scriptEditView = new ScriptEditView();
            scriptEditView.Owner = Application.Current.MainWindow;
            scriptEditView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            scriptEditView.DataContext = scriptEditViewModel;
            scriptEditViewModel.EditedScriptModel = scriptModel;
            scriptEditView.Show();
        }

        /// <summary>
        /// Check is script open in window
        /// </summary>
        /// <param name="scriptModel"></param>
        /// <returns></returns>
        private static bool CheckIsScriptCurrentOpened(IScriptModel scriptModel)
        {
            var currentOpenedScript = (from scriptEditViewModel in ScriptEditViewModelFactory.GetScriptEditViewModelList()
                                       where scriptEditViewModel.EditedScriptModel.Id == scriptModel.Id
                                       select scriptEditViewModel.EditedScriptModel).FirstOrDefault();

            if (currentOpenedScript != null)
                return true;

            return false;
        }

        /// <summary>
        /// Reload script with children with settings encoding
        /// </summary>
        /// <param name="treeViewItem"></param>
        private static void ReloadScriptWithEncoding(ITreeViewItem treeViewItem)
        {
            if (!treeViewItem.IsFromClipboard && treeViewItem.Size > ZERO && treeViewItem.IsChecked == true)
            {
                ISQLExecutorSettings sqlExecutorSettings = GetSqlExecutorSettings();
                IScriptService scriptService = GetScriptService();
                var encoding = EncodingStreamReaderService.GetEncodingFromString(sqlExecutorSettings.DefaultEncoding);

                treeViewItem = (ITreeViewItem)scriptService.ReloadScriptWithEncoding(treeViewItem, encoding);
            }

            if (treeViewItem.Children != null)
                foreach (ITreeViewItem item in treeViewItem.Children)
                    ReloadScriptWithEncoding(item);
        }

        /// <summary>
        /// Return list of selected script with recursion
        /// </summary>
        /// <param name="treeViewItem"></param>
        /// <param name="scriptModelList"></param>
        /// <param name="scriptService"></param>
        private static void GetSelectedScripts(ITreeViewItem treeViewItem, List<IScriptModel> scriptModelList, IScriptService scriptService)
        {
            if (scriptService.IsFromClipboard(treeViewItem))
            {
                if (treeViewItem.IsChecked == true)
                    scriptModelList.Add(treeViewItem);
                return;
            }

            var fileInfo = new FileInfo(treeViewItem.FullPath);

            if (scriptService.IsScriptFile(fileInfo) && treeViewItem.IsChecked == true)
                scriptModelList.Add(treeViewItem);

            if (treeViewItem.Children != null)
                foreach (ITreeViewItem item in treeViewItem.Children)
                    GetSelectedScripts(item, scriptModelList, scriptService);
        }

        /// <summary>
        /// Recursive expand treeViewItem with children
        /// </summary>
        /// <param name="treeViewItem"></param>
        private static void Expand(ITreeViewItem treeViewItem)
        {
            treeViewItem.IsExpanded = true;

            if (treeViewItem.Children != null && treeViewItem.Children.Count > ZERO)
            {
                foreach (ITreeViewItem item in treeViewItem.Children)
                    Expand(item);
            }
        }

        /// <summary>
        /// Recursive collapse treeViewItem with children
        /// </summary>
        /// <param name="treeViewItem"></param>
        private static void Collapse(ITreeViewItem treeViewItem)
        {
            treeViewItem.IsExpanded = false;

            if (treeViewItem.Children != null && treeViewItem.Children.Count > ZERO)
            {
                foreach (ITreeViewItem item in treeViewItem.Children)
                    Collapse(item);
            }
        }

        /// <summary>
        /// Return script service instance with user application settings
        /// </summary>
        /// <returns></returns>
        private static ScriptService GetScriptService()
        {
            var sqlExecutorSettings = GetSqlExecutorSettings();

            return new ScriptService(sqlExecutorSettings);
        }

        /// <summary>
        /// Return application settings class
        /// </summary>
        /// <returns></returns>
        private static ISQLExecutorSettings GetSqlExecutorSettings()
        {
            ISQLExecutorSettingsService sqlExecutorSettingsService = new SQLExecutorSettingsService();
            ISQLExecutorSettings sqlExecutorSettings = sqlExecutorSettingsService.GetSQLExecutorSettings();

            return sqlExecutorSettings;
        }
    }
}
