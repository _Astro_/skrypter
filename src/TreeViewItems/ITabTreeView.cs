﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewItems
{
    /// <summary>
    /// TreeView model class for tabControl
    /// </summary>
    public interface ITabTreeView
    {
        TabTreeViewType TreeViewType { get; set; }

        string Header { get; set; }

        ObservableCollection<ITreeViewItem> TreeViewItemCollection { get; set; }

        ObservableCollection<IBindableMenuItem> ContextMenuItemsCollection { get; set; }
    }
}
