﻿using BaseMVVM;
using Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace TreeViewItems
{
    /// <summary>
    /// Treeview item model class extended script model 
    /// </summary>
    public class TreeViewItem : ObservableObject, ITreeViewItem
    {
        private string _id;
        private bool? _isChecked;
        private bool _isExpanded;
        private bool _updateChildren;
        private ObservableCollection<ITreeViewItem> _children;
        private ITreeViewItem _parent;

        #region IScriptModel

        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                Set(ref _id, value);
            }
        }

        public string Name { get; set; }

        public string Text { get; set; }

        public string FullPath { get; set; }

        public FileAttributes Type { get; set; }

        public long Size { get; set; }

        public DateTime LastModified { get; set; }

        public bool IsFromClipboard { get; set; }

        #endregion IScriptModel

        #region ITreeViewItem

        public bool? IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                Set(ref _isChecked, value);

                if (_updateChildren)
                    UpdateChildrenMethod(value);

                UpdateParentMethod(value);

                EnableUpdate();
            }
        }

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                Set(ref _isExpanded, value);
            }
        }

        public bool UpdateChildren
        {
            get
            {
                return _updateChildren;
            }
            set
            {
                Set(ref _updateChildren, value);
            }
        }

        public ObservableCollection<ITreeViewItem> Children
        {
            get
            {
                if (_children == null)
                    _children = new ObservableCollection<ITreeViewItem>();
                return _children;
            }
            set
            {
                Set(ref _children, value);
            }
        }

        public ITreeViewItem Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                Set(ref _parent, value);
            }
        }

        #endregion ITreeViewItem

        /// <summary>
        /// Default constructor
        /// </summary>
        public TreeViewItem()
        {
            _id = IdGenerator.GenerateStringId();
            _isChecked = false;
            EnableUpdate();
        }

        /// <summary>
        /// Copy from base script model class
        /// </summary>
        /// <param name="scriptModel"></param>
        public TreeViewItem(IScriptModel scriptModel)
        {
            Id = scriptModel.Id;
            Name = scriptModel.Name;
            Text = scriptModel.Text;
            FullPath = scriptModel.FullPath;
            Type = scriptModel.Type;
            Size = scriptModel.Size;
            LastModified = scriptModel.LastModified;
            Children = null;
            Parent = null;
            IsChecked = false;
            IsExpanded = false;
            EnableUpdate();
        }

        private void UpdateChildrenMethod(bool? value)
        {
            if (_children != null && _children.Count > 0 && value != null)
                foreach (ITreeViewItem item in _children)
                    item.IsChecked = value;
        }

        private void UpdateParentMethod(bool? value)
        {
            if (Parent != null && Parent.IsChecked != value)
            {
                Parent.UpdateChildren = false;

                foreach (ITreeViewItem item in Parent.Children)
                {
                    if (item.IsChecked != value)
                    {
                        Parent.IsChecked = null;
                        return;
                    }
                }

                Parent.IsChecked = value;
            }
        }

        private void EnableUpdate()
        {
            _updateChildren = true;
        }
    }
}
