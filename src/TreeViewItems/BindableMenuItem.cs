﻿using BaseMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TreeViewItems
{
    /// <summary>
    /// Bindable menu item model class
    /// </summary>
    public class BindableMenuItem : ObservableObject, IBindableMenuItem
    {
        public string Header { get; set; }

        public ICommand Command { get; set; }

        public string CommandParameters { get; set; }

        public ObservableCollection<IBindableMenuItem> SubMenuItems { get; set; }
    }
}
