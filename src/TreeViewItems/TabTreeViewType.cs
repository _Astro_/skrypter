﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewItems
{
    /// <summary>
    /// Tab treeView type enum
    /// </summary>
    public enum TabTreeViewType
    {
        SCRIPTS,
        REPORTS
    }
}
