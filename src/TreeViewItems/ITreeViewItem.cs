﻿using Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewItems
{
    /// <summary>
    /// Interface of treeview item model based on script model
    /// </summary>
    public interface ITreeViewItem : IScriptModel
    {
        new string Name { get; set; }

        bool? IsChecked { get; set; }

        bool IsExpanded { get; set; }

        bool UpdateChildren { get; set; }

        ObservableCollection<ITreeViewItem> Children { get; set; }

        ITreeViewItem Parent { get; set; }
    }
}
