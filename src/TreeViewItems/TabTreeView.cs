﻿using BaseMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewItems
{
    /// <summary>
    /// Tab treeView class
    /// </summary>
    public class TabTreeView : ObservableObject, ITabTreeView
    {
        private TabTreeViewType _tabTreeViewType;
        private string _header;
        private ObservableCollection<ITreeViewItem> _treeViewItemCollection;
        private ObservableCollection<IBindableMenuItem> _contextMenuItemsCollection;

        public TabTreeViewType TreeViewType
        {
            get
            {
                return _tabTreeViewType;
            }
            set
            {
                Set(ref _tabTreeViewType, value);
            }
        }

        public string Header
        {
            get
            {
                return _header;
            }
            set
            {
                Set(ref _header, value);
            }
        }

        public ObservableCollection<ITreeViewItem> TreeViewItemCollection
        {
            get
            {
                if (_treeViewItemCollection == null)
                    _treeViewItemCollection =  new ObservableCollection<ITreeViewItem>();

                return _treeViewItemCollection;
            }

            set
            {
                Set(ref _treeViewItemCollection, value);
            }
        }

        public ObservableCollection<IBindableMenuItem> ContextMenuItemsCollection
        {
            get
            {
                if (_contextMenuItemsCollection == null)
                    _contextMenuItemsCollection = new ObservableCollection<IBindableMenuItem>();
                return _contextMenuItemsCollection;
            }
            set
            {
                Set(ref _contextMenuItemsCollection, value);
            }
        }
    }
}
