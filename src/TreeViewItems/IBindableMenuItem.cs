﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TreeViewItems
{
    /// <summary>
    /// Interface of bindable menu item component class
    /// </summary>
    public interface IBindableMenuItem
    {
        string Header { get; set; }

        ICommand Command { get; set; }

        string CommandParameters { get; set; }

        ObservableCollection<IBindableMenuItem> SubMenuItems { get; set; }
    }
}
