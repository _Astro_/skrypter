﻿using InterfaceToDatabase.SQLExecutor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scripts;
using System.IO;
using SystemInterface.IO;
using SystemWrapper.IO;

namespace Scripts.Tests.UnitTests
{
    [TestClass]
    public class ScriptsServiceTests
    {
        private const bool FALSE = false;

        private IScriptService GetScriptService(ISQLExecutorSettings sqlExecutorSettings)
        {
            return new ScriptService(sqlExecutorSettings);
        }

        //[TestMethod]
        //public void ChangeStatus_SetToFalse_ReturnFalse()
        //{
        //    Mock<ISQLExecutorSettings> mockSQLExecutorSettings = new Mock<ISQLExecutorSettings>();
        //    IScriptService scriptService = GetScriptService(mockSQLExecutorSettings.Object);
        //    Mock<IScriptModel> mockScriptModel = new Mock<IScriptModel>();

        //    scriptService.ChangeStatus(mockScriptModel.Object, FALSE);

        //    Assert.IsTrue(mockScriptModel.Object.IsSelected == FALSE);
        //}

        [TestMethod]
        public void Test()
        {
            Mock<ISQLExecutorSettings> mockSQLExecutorSettings = new Mock<ISQLExecutorSettings>();
            IScriptService scriptService = GetScriptService(mockSQLExecutorSettings.Object);

            string path = Directory.GetCurrentDirectory() + "\\SQLExecutorDatabase.sqlite";
            FileInfo fileInfo = new FileInfo(path);
            var model = scriptService.GetScriptModel(fileInfo);
        }

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    string path = new DirectoryWrap().GetCurrentDirectory();
        //    IDirectoryInfo directoryInfoWrap = new DirectoryInfoWrap(path);
        //    IDirectoryInfo[] directoriesBefore = directoryInfoWrap.GetDirectories();

        //    directoryInfoWrap.CreateSubdirectory("Dir1");
        //    directoryInfoWrap.CreateSubdirectory("Dir2");
        //    IDirectoryInfo[] directoriesAfterCreate = directoryInfoWrap.GetDirectories();

        //    Assert.AreEqual("Dir1", directoriesAfterCreate[0].Name);
        //    Assert.AreEqual("Dir2", directoriesAfterCreate[1].Name);
        //    directoriesAfterCreate[0].Delete();
        //    directoriesAfterCreate[1].Delete();

        //    var directoriesAfterDelete = directoryInfoWrap.GetDirectories();
        //    Assert.AreEqual(directoriesBefore.Length, directoriesAfterDelete.Length);
        //}

        //[TestMethod]
        //public void GetFiles_must_have_files_in_Debug_folder()
        //{
        //    IDirectoryInfo directoryWrap = new DirectoryInfoWrap(new DirectoryWrap().GetCurrentDirectory());
        //    IFileInfo[] fileInfoWraps = directoryWrap.GetFiles();
        //    Assert.IsTrue(fileInfoWraps.Length > 0);
        //}
    }
}