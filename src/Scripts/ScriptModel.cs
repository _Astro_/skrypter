﻿using BaseMVVM;
using System;
using System.IO;
using Tools;

namespace Scripts
{
    /// <summary>
    /// Script model class
    /// </summary>
    public class ScriptModel : ObservableObject, IScriptModel
    {
        private string _id;

        #region IScriptModel

        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                Set(ref _id, value);
            }
        }

        public string Name { get; set; }

        public string Text { get; set; }

        public string FullPath { get; set; }

        public FileAttributes Type { get; set; }

        public long Size { get; set; }

        public DateTime LastModified { get; set; }

        public bool IsFromClipboard { get; set; }

        #endregion

        /// <summary>
        /// Default constructor
        /// </summary>
        public ScriptModel()
        {
            _id = IdGenerator.GenerateStringId();
        }
    }
}