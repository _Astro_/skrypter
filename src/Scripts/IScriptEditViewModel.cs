﻿using LocalizationData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Scripts
{
    /// <summary>
    /// Interface of script edit view model
    /// </summary>
    public interface IScriptEditViewModel
    {
        IScriptModel EditedScriptModel { get; set; }

        bool EnableEdit { get; set; }

        ICommand CloseViewCommand { get; set; }

        ResourceStrings Strings { get; }
    }
}
