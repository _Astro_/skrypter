﻿using BaseMVVM;
using LocalizationData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Scripts
{
    /// <summary>
    /// Script edit view model class
    /// </summary>
    public class ScriptEditViewModel : ObservableObject, IScriptEditViewModel
    {
        private ResourceStrings _resourceStrings = new PublicStrings();
        private IScriptModel _editedScriptModel;
        private bool _enableEdit;

        /// <summary>
        /// Ctor of script edit view model class
        /// </summary>
        public ScriptEditViewModel()
        {
            CloseViewCommand = new RelayCommand<IClosable>(CloseView);
        }

        public IScriptModel EditedScriptModel
        {
            get
            {
                return _editedScriptModel;
            }
            set
            {
                Set(ref _editedScriptModel, value);
            }
        }

        public bool EnableEdit
        {
            get
            {
                return _enableEdit;
            }
            set
            {
                Set(ref _enableEdit, value);
            }
        }

        public ICommand CloseViewCommand { get; set; }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        /// <summary>
        /// Close window method and remove view model from view model list
        /// </summary>
        /// <param name="window"></param>
        public void CloseView(IClosable window)
        {
            ScriptEditViewModelFactory.DeleteScriptEditViewModel(this);

            if (window != null)
                window.Close();
        }
    }
}
