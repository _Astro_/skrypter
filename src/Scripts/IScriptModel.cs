﻿using System;
using System.IO;

namespace Scripts
{
    /// <summary>
    /// Script model interface
    /// </summary>
    public interface IScriptModel
    {
        string Id { get; set; }

        string Name { get; set; }

        string Text { get; set; }

        string FullPath { get; set; }

        FileAttributes Type { get; set; }

        long Size { get; set; }

        DateTime LastModified { get; set; }

        bool IsFromClipboard { get; set; }
    }
}