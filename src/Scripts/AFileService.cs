﻿using ErrorsAndExceptions.AbstractServices;
using System;
using System.IO;
using System.Security.Permissions;

namespace Scripts
{
    /// <summary>
    /// Abstract file service class
    /// </summary>
    public abstract class AFileService : AResultService
    {
        /// <summary>
        /// Check file or folder exists
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool FileOrFolderExists(FileInfo fileInfo)
        {
            if (fileInfo.Exists || IsDirectory(fileInfo))
                return true;
            return false;
        }

        /// <summary>
        /// Check is file hidden
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool IsFileHidden(FileInfo fileInfo)
        {
            if (fileInfo.Attributes.HasFlag(FileAttributes.Hidden))
                return true;
            return false;
        }

        /// <summary>
        /// Check is file temporary
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool IsFileTemporary(FileInfo fileInfo)
        {
            if (fileInfo.Attributes.HasFlag(FileAttributes.Temporary))
                return true;
            return false;
        }

        /// <summary>
        /// Check is file available
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool IsFileAvailable(FileInfo fileInfo)
        {
            if (!IsDirectory(fileInfo))
            {
                FileStream fileStream = null;

                try
                {
                    fileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    return true;
                }
                catch (Exception exception)
                {
                    return ReportFail(exception);
                }
                finally
                {
                    if (fileStream != null)
                    {
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Check if is directory
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool IsDirectory(FileInfo fileInfo)
        {
            if (fileInfo.Exists == false && fileInfo.Attributes.HasFlag(FileAttributes.Directory))
                return true;
            return false;
        }
    }
}