﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts
{
    /// <summary>
    /// Static list of script edit view models
    /// </summary>
    public static class ScriptEditViewModelFactory
    {
        private static List<IScriptEditViewModel> _scriptEditViewModelList;

        /// <summary>
        /// Return list of script edit view models
        /// </summary>
        /// <returns></returns>
        public static List<IScriptEditViewModel> GetScriptEditViewModelList()
        {
            if (_scriptEditViewModelList != null)
                return _scriptEditViewModelList;

            return new List<IScriptEditViewModel>();
        }

        /// <summary>
        /// Add model to list
        /// </summary>
        /// <param name="scriptEditViewModel"></param>
        public static void AddScriptEditViewModel(IScriptEditViewModel scriptEditViewModel)
        {
            if (_scriptEditViewModelList == null)
                _scriptEditViewModelList = GetScriptEditViewModelList();

            _scriptEditViewModelList.Add(scriptEditViewModel);
        }

        /// <summary>
        /// Delete model from list if list contains it
        /// </summary>
        /// <param name="scriptEditViewModelToDelete"></param>
        public static void DeleteScriptEditViewModel(IScriptEditViewModel scriptEditViewModelToDelete)
        {
            if (_scriptEditViewModelList == null) return;

            var currentOpenedViewModel = (from scriptEditViewModel in _scriptEditViewModelList
                                       where scriptEditViewModel.EditedScriptModel.Id == scriptEditViewModelToDelete.EditedScriptModel.Id
                                       select scriptEditViewModel).FirstOrDefault();

            if (currentOpenedViewModel != null)
                _scriptEditViewModelList.Remove(currentOpenedViewModel);
        }
    }
}
