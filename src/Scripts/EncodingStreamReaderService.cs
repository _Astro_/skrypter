﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace Scripts
{
    /// <summary>
    /// Stream reader service including encoding
    /// </summary>
    public static class EncodingStreamReaderService
    {
        private const int WINDOWS_ENCODING = 1250;
        private const string DEFAULT_ENCODING = "DEFAULT: windows-1250";
        private static Encoding _defaultEncoding;

        /// <summary>
        /// Return stream reader based on encoding
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static StreamReader GetEncodingStreamReader(FileInfo fileInfo, Encoding encoding = null)
        {
            var defaultEncoding = GetDefaultEncoding();

            if (encoding == null)
                return new StreamReader(fileInfo.FullName, defaultEncoding);

            return new StreamReader(fileInfo.FullName, encoding);
        }

        /// <summary>
        /// Set service default encoding
        /// </summary>
        /// <param name="encodingType"></param>
        public static void SetDefaultEncoding(Encoding encodingType)
        {
            _defaultEncoding = encodingType;
        }

        /// <summary>
        /// Get service default encoding
        /// </summary>
        /// <returns></returns>
        public static Encoding GetDefaultEncoding()
        {
            if (_defaultEncoding == null)
                return GetDefaultWindowsEncoding();
            return _defaultEncoding;
        }

        /// <summary>
        /// Return encoding from string
        /// If string is null or empty or DEFAULT_ENCODING then return WINDOWS_ENCODING
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static Encoding GetEncodingFromString(string encoding)
        {
            if (encoding == DEFAULT_ENCODING || string.IsNullOrEmpty(encoding))
                return Encoding.GetEncoding(WINDOWS_ENCODING);
            return Encoding.GetEncoding(encoding);
        }

        /// <summary>
        /// Return list of encoding as list of string
        /// </summary>
        /// <returns></returns>
        public static List<string> GetEncodingStringList()
        {
            return Encoding.GetEncodings().Select(s => s.GetEncoding().HeaderName).ToList();
        }

        /// <summary>
        /// Return list of encoding
        /// </summary>
        /// <returns></returns>
        public static List<Encoding> GetEncodingList()
        {
            return Encoding.GetEncodings().Select(s => s.GetEncoding()).ToList();
        }

        /// <summary>
        /// Return list of encoding as list of string witn empty encoding and  DEFAULT_ENCODING
        /// </summary>
        /// <returns></returns>
        public static List<string> GetEncodingStringListForView()
        {
            List<string> encodingList = Encoding.GetEncodings().Select(s => s.GetEncoding().HeaderName).ToList();

            encodingList.Sort();
            encodingList.Insert(0, DEFAULT_ENCODING);
            encodingList.Insert(0, string.Empty);

            return encodingList;
        }

        /// <summary>
        /// Return collection of encoding as list of string witn empty encoding and  DEFAULT_ENCODING
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<string> GetEncodingStringCollectionForViewModel()
        {
            List<string> encodingList = GetEncodingStringListForView();
            ObservableCollection<string> encodingCollection = new ObservableCollection<string>();

            foreach (string encoding in encodingList)
                encodingCollection.Add(encoding);

            return encodingCollection;
        }

        /// <summary>
        /// Return default WINDOWS_ENCODING
        /// </summary>
        /// <returns></returns>
        private static Encoding GetDefaultWindowsEncoding()
        {
            return Encoding.GetEncoding(WINDOWS_ENCODING);
        }
    }
}