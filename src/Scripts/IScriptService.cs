﻿using System.IO;
using System.Text;

namespace Scripts
{
    /// <summary>
    /// Interface of script service
    /// </summary>
    public interface IScriptService
    {
        IScriptModel GetScriptModel(FileInfo fileInfo, Encoding encoding = null);

        IScriptModel ReloadScriptWithEncoding(IScriptModel scriptModel, Encoding encoding);

        bool ValidateScriptFile(FileInfo fileInfo);

        string GetScriptText(FileInfo fileInfo, Encoding encoding = null);

        bool IsScriptFile(FileInfo fileInfo);

        bool IsFromClipboard(IScriptModel scriptModel);
    }
}