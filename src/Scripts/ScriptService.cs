﻿using InterfaceToDatabase.SQLExecutor;
using LocalizationData;
using System;
using System.IO;
using System.Text;

namespace Scripts
{
    /// <summary>
    /// Script service class
    /// </summary>
    public class ScriptService : AFileService, IScriptService
    {
        private ISQLExecutorSettings _sqlExecutorSettings;

        /// <summary>
        /// Default ctor
        /// </summary>
        /// <param name="sqlExecutorSettings"></param>
        public ScriptService(ISQLExecutorSettings sqlExecutorSettings)
        {
            _sqlExecutorSettings = sqlExecutorSettings;
        }

        /// <summary>
        /// Return script model from file or folder
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public IScriptModel GetScriptModel(FileInfo fileInfo, Encoding encoding = null)
        {
            if (!ValidateScriptFile(fileInfo))
                return null;

            if (encoding == null)
                encoding = EncodingStreamReaderService.GetEncodingFromString(_sqlExecutorSettings.DefaultEncoding);

            return new ScriptModel()
            {
                Name = fileInfo.Name,
                FullPath = fileInfo.FullName,
                LastModified = fileInfo.LastWriteTime,
                Type = fileInfo.Attributes,
                Size = fileInfo.Exists ? fileInfo.Length : 0,
                Text = GetScriptText(fileInfo, encoding)
            };
        }

        /// <summary>
        /// Reload script model text with current encoding
        /// </summary>
        /// <param name="scriptModel"> Script model with text to reload </param>
        /// <param name="encoding"> Current encoding </param>
        /// <returns></returns>
        public IScriptModel ReloadScriptWithEncoding(IScriptModel scriptModel, Encoding encoding)
        {
            FileInfo fileInfo = new FileInfo(scriptModel.FullPath);

            scriptModel.Text = GetScriptText(fileInfo, encoding);

            return scriptModel;
        }

        /// <summary>
        /// Validate script file with using user settings
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool ValidateScriptFile(FileInfo fileInfo)
        {
            if (!FileOrFolderExists(fileInfo))
                return ReportFail(ResourceStrings.FileNotFound);

            if (IsFileHidden(fileInfo) && _sqlExecutorSettings.IncludeHiddenFilesAndFolders == false)
                return ReportFail(string.Format("{0}\n{1}", ResourceStrings.HiddenFile, fileInfo.FullName));

            if (IsFileTemporary(fileInfo) && _sqlExecutorSettings.IncludeTemporaryFilesAndFolders == false)
                return ReportFail(string.Format("{0}\n{1}", ResourceStrings.TemporaryFile, fileInfo.FullName));

            if (!IsFileAvailable(fileInfo))
                return false;

            return true;
        }

        /// <summary>
        /// Return text of script with encoding
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string GetScriptText(FileInfo fileInfo, Encoding encoding = null)
        {
            if (IsDirectory(fileInfo)) return string.Empty;

            TextReader textReader = null; 

            try
            {
                textReader = EncodingStreamReaderService.GetEncodingStreamReader(fileInfo, encoding);
                var scriptText = textReader.ReadToEnd();
                return scriptText;
            }
            catch (Exception exception)
            {
                ReportFail(exception);
                return string.Empty;
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                    textReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Check is script file not directory
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool IsScriptFile(FileInfo fileInfo)
        {
            return !IsDirectory(fileInfo);
        }

        /// <summary>
        /// Check is script pasted from clipdoard
        /// </summary>
        /// <param name="scriptModel"></param>
        /// <returns></returns>
        public bool IsFromClipboard(IScriptModel scriptModel)
        {
            return scriptModel.IsFromClipboard;
        }
    }
}