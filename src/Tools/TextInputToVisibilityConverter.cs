﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Tools
{
    public class TextInputToVisibilityConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values is bool)
            {
                bool isVisible = (bool)values;
                if (isVisible)
                    return Visibility.Visible;
            }
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}