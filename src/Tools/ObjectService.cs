﻿using System;
using System.Reflection;

namespace Tools
{
    public static class ObjectService
    {
        /// <summary>
        /// Set value to object of type from property info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="instance"></param>
        /// <param name="value"></param>
        public static void SetValue(PropertyInfo info, object instance, object value)
        {
            info.SetValue(instance, Convert.ChangeType(value, info.PropertyType));
        }

        /// <summary>
        /// Compare two objects of generic T type
        /// </summary>
        /// <typeparam name="T"> Type of objects </typeparam>
        /// <param name="objectA"> First object </param>
        /// <param name="objectB"> Second object </param>
        /// <returns></returns>
        public static bool CompareObjects<T>(T objectA, T objectB)
        {
            Type objectType = typeof(T);

            if (objectA == null || objectB == null)
                return false;

            foreach (PropertyInfo propertyInfo in objectType.GetProperties())
            {
                string valueOfObjectAProperty = string.Empty;
                if (objectType.GetProperty(propertyInfo.Name).GetValue(objectA, null) != null)
                    valueOfObjectAProperty = objectType.GetProperty(propertyInfo.Name).GetValue(objectA, null).ToString();

                string valueOfObjectBProperty = string.Empty;
                if (objectType.GetProperty(propertyInfo.Name).GetValue(objectB, null) != null)
                    valueOfObjectBProperty = objectType.GetProperty(propertyInfo.Name).GetValue(objectB, null).ToString();

                if (string.IsNullOrEmpty(valueOfObjectAProperty) && !string.IsNullOrEmpty(valueOfObjectBProperty))
                    return false;

                if (!string.IsNullOrEmpty(valueOfObjectAProperty) && string.IsNullOrEmpty(valueOfObjectBProperty))
                    return false;

                if (!string.IsNullOrEmpty(valueOfObjectAProperty) && !string.IsNullOrEmpty(valueOfObjectBProperty))
                {
                    if (valueOfObjectAProperty != valueOfObjectBProperty)
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compare two database model objects of generic T type
        /// </summary>
        /// <typeparam name="T"> Type of objects </typeparam>
        /// <param name="objectA"> First object </param>
        /// <param name="objectB"> Second object </param>
        /// <returns></returns>
        public static bool CompareDatabaseModelObjects<T>(T objectA, T objectB)
        {
            Type objectType = typeof(T);

            if (objectA == null || objectB == null)
                return false;

            foreach (PropertyInfo propertyInfo in objectType.GetProperties())
            {
                if (propertyInfo.Name != "IsSelected")
                {
                    string valueOfObjectAProperty = string.Empty;
                    if (objectType.GetProperty(propertyInfo.Name).GetValue(objectA, null) != null)
                        valueOfObjectAProperty = objectType.GetProperty(propertyInfo.Name).GetValue(objectA, null).ToString();

                    string valueOfObjectBProperty = string.Empty;
                    if (objectType.GetProperty(propertyInfo.Name).GetValue(objectB, null) != null)
                        valueOfObjectBProperty = objectType.GetProperty(propertyInfo.Name).GetValue(objectB, null).ToString();

                    if (string.IsNullOrEmpty(valueOfObjectAProperty) && !string.IsNullOrEmpty(valueOfObjectBProperty))
                        return false;

                    if (!string.IsNullOrEmpty(valueOfObjectAProperty) && string.IsNullOrEmpty(valueOfObjectBProperty))
                        return false;

                    if (!string.IsNullOrEmpty(valueOfObjectAProperty) && !string.IsNullOrEmpty(valueOfObjectBProperty))
                    {
                        if (valueOfObjectAProperty != valueOfObjectBProperty)
                            return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Copy all properties from object to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToCopyFrom"></param>
        /// <param name="objectForCopyTo"></param>
        public static void CopyAllProperties<T>(T objectToCopyFrom, T objectForCopyTo)
        {
            Type objectType = typeof(T);

            if (objectToCopyFrom == null || objectForCopyTo == null)
                return;

            foreach (PropertyInfo propertyInfo in objectType.GetProperties())
            {
                if (objectType.GetProperty(propertyInfo.Name).GetValue(objectToCopyFrom, null) != null)
                {
                    objectType.GetProperty(propertyInfo.Name).SetValue(objectForCopyTo,
                        objectType.GetProperty(propertyInfo.Name).GetValue(objectToCopyFrom, null));
                }
                else
                {
                    objectType.GetProperty(propertyInfo.Name).SetValue(objectForCopyTo, null);
                }
            }
        }
    }
}