﻿using System;
using System.Windows.Data;

namespace Tools
{
    public class IntToStringConverter : IValueConverter
    {
        /// <summary>
        /// Convert int to string
        /// </summary>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                return value.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Convert string to int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            int outputValue;

            if (value is string)
            {
                if (int.TryParse(value.ToString(), out outputValue))
                    return outputValue;
                return default(int);
            }
            return default(int);
        }
    }
}