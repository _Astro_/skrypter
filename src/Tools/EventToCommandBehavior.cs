﻿using LocalizationData;
using Microsoft.Xaml.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace Tools
{
    /// <summary>
    /// Behavior that will connect an UI event to a viewmodel Command,
    /// allowing the event arguments to be passed as the CommandParameter.
    /// </summary>
    public class EventToCommandBehavior : Behavior<FrameworkElement>
    {
        private Delegate _handler;
        private EventInfo _oldEvent;

        // Event
        public string Event
        {
            get
            {
                return (string)GetValue(EventProperty);
            }
            set
            {
                SetValue(EventProperty, value);
            }
        }

        public static readonly DependencyProperty EventProperty =
            DependencyProperty.Register(
                ResourceStrings.Event,
                typeof(string),
                typeof(EventToCommandBehavior),
                new PropertyMetadata(null, OnEventChanged)
                );

        // Command
        public ICommand Command
        {
            get
            {
                return (ICommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register(
                ResourceStrings.Command,
                typeof(ICommand),
                typeof(EventToCommandBehavior),
                new PropertyMetadata(null)
                );

        // PassEventArgsToCommand (default: false)
        public bool PassEventArgsToCommand
        {
            get
            {
                return (bool)GetValue(PassArgumentsProperty);
            }
            set
            {
                SetValue(PassArgumentsProperty, value);
            }
        }

        public static readonly DependencyProperty PassArgumentsProperty =
            DependencyProperty.Register(
                ResourceStrings.PassEventArgsToCommand,
                typeof(bool),
                typeof(EventToCommandBehavior),
                new PropertyMetadata(false)
                );

        private static void OnEventChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var behavior = (EventToCommandBehavior)dependencyObject;

            if (behavior.AssociatedObject != null) // is not yet attached at initial load
                behavior.AttachHandler((string)eventArgs.NewValue);
        }

        protected override void OnAttached()
        {
            AttachHandler(this.Event); // initial set
        }

        /// <summary>
        /// Attaches the handler to the event
        /// </summary>
        private void AttachHandler(string eventName)
        {
            // detach old event
            if (_oldEvent != null)
                _oldEvent.RemoveEventHandler(this.AssociatedObject, _handler);

            // attach new event
            if (!string.IsNullOrEmpty(eventName))
            {
                EventInfo eventInfo = this.AssociatedObject.GetType().GetEvent(eventName);
                if (eventInfo != null)
                {
                    MethodInfo methodInfo = this.GetType().GetMethod(
                        ResourceStrings.ExecuteCommand, BindingFlags.Instance | BindingFlags.NonPublic);
                    _handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, methodInfo);
                    eventInfo.AddEventHandler(this.AssociatedObject, _handler);
                    _oldEvent = eventInfo; // store to detach in case the Event property changes
                }
            }
        }

        /// <summary>
        /// Executes the Command
        /// </summary>
        private void ExecuteCommand(object sender, EventArgs eventArgs)
        {
            object parameter = this.PassEventArgsToCommand ? eventArgs : null;
            if (this.Command != null)
            {
                if (this.Command.CanExecute(parameter))
                    this.Command.Execute(parameter);
            }
        }
    }
}
