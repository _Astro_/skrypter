﻿using System;

namespace Tools
{
    /// <summary>
    /// Id generator static class
    /// </summary>
    public static class IdGenerator
    {
        /// <summary>
        /// Generate GUID and return it as string
        /// </summary>
        /// <returns></returns>
        public static string GenerateStringId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}