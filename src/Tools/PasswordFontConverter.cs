﻿using System;
using System.Windows.Data;

namespace Tools
{
    /// <summary>
    /// Password font converter service
    /// </summary>
    public class PasswordFontConverter : IValueConverter
    {
        private const string PASSWORD_FONT = "Segoe MDL2 Assets";
        private const string NORMAL_FONT = "Segoe UI";

        /// <summary>
        /// Convert font to normal or password family
        /// </summary>
        /// <returns></returns>
        public object Convert(object values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values is bool)
            {
                bool isPasswordFont = (bool)values;
                if (isPasswordFont)
                    return PASSWORD_FONT;
            }
            return NORMAL_FONT;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}