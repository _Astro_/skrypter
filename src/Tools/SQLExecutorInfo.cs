﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    public static class SQLExecutorInfo
    {
        /// <summary>
        /// Return SQL Executor about string
        /// </summary>
        /// <returns></returns>
        public static string GetSQLExecutorAboutInfo()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("SQL Executor C# Application\n"));
            builder.Append(string.Format("Version {0}\n", GetCurrentVersion().Replace(",", ".")));
            builder.Append(string.Format("Release for public open source\n"));

            return builder.ToString();
        }

        /// <summary>
        /// Return current version as application build julian date
        /// </summary>
        /// <returns></returns>
        private static string GetCurrentVersion()
        {
            var assemblyBuildDateTime = Assembly.GetExecutingAssembly().GetLinkerTime();
            var julianDate = assemblyBuildDateTime.ToOADate() + 2415018.5;
            return julianDate.ToString();
        }

        /// <summary>
        /// Return assembly build datetime
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private static DateTime GetLinkerTime(this Assembly assembly, TimeZoneInfo target = null)
        {
            var filePath = assembly.Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;

            var buffer = new byte[2048];

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                stream.Read(buffer, 0, 2048);

            var offset = BitConverter.ToInt32(buffer, c_PeHeaderOffset);
            var secondsSince1970 = BitConverter.ToInt32(buffer, offset + c_LinkerTimestampOffset);
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var linkTimeUtc = epoch.AddSeconds(secondsSince1970);

            var tz = target ?? TimeZoneInfo.Local;
            var localTime = TimeZoneInfo.ConvertTimeFromUtc(linkTimeUtc, tz);

            return localTime;
        }
    }
}
