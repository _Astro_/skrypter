﻿using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase;
using InterfaceToDatabase.InterfceToDatabaseFactory;
using LocalizationData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SQLExecutorUnitTest
{
    [TestClass]
    public class InterfaceToMSSQLTest : AResultService
    {
        //private IInterfaceToDatabaseFactory _interfaceToDatabaseFactory;
        //private IInterfaceToDatabase _interfaceToMSSQL;
        //private MSSQLDatabaseModel _mssqlDatabaseModel;

        //[TestInitialize]
        //public void TestInitialize()
        //{
        //    _mssqlDatabaseModel = new MSSQLDatabaseModel()
        //    {
        //        DatabaseType = DatabaseType.MSSQL,
        //        ServerAddress = "127.0.0.1",
        //        DatabaseName = "master",
        //        DatabaseAliasName = "master",
        //        UserName = "sa",
        //        Password = "sqlix#1",
        //        ConnectionTimeout = 10
        //    };
        //    _interfaceToDatabaseFactory = new InterfaceToDatabaseFactory();
        //}

        //[TestCleanup]
        //public void TestCleanUp()
        //{
        //    _mssqlDatabaseModel = null;
        //    _interfaceToDatabaseFactory = null;
        //    _interfaceToMSSQL = null;
        //    ClearErrorsAndExceptions();
        //}

        //[TestMethod]
        //public void ValidateConnectionStringReturnTrue()
        //{
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.GetConnectionString();

        //    Assert.IsTrue(!string.IsNullOrEmpty(result));
        //    Assert.IsTrue(string.IsNullOrEmpty(GetException()));
        //}

        //[TestMethod]
        //public void InvalidDatabaseModelNullDatabaseModel()
        //{
        //    _mssqlDatabaseModel = null;
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    Assert.IsNull(_interfaceToMSSQL);
        //}

        //[TestMethod]
        //public void InvalidDatabaseModelNullServerAddress()
        //{
        //    _mssqlDatabaseModel.ServerAddress = null;
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.GetConnectionString();

        //    Assert.IsTrue(string.IsNullOrEmpty(result));
        //    Assert.IsTrue(GetException() == ResourceStrings.InvalidDatabaseModelServerAddress);
        //}

        //[TestMethod]
        //public void InvalidDatabaseModelNullDatabaseName()
        //{
        //    _mssqlDatabaseModel.DatabaseName = null;
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.GetConnectionString();

        //    Assert.IsTrue(string.IsNullOrEmpty(result));
        //    Assert.IsTrue(GetException() == ResourceStrings.InvalidDatabaseModelDatabaseName);
        //}

        //[TestMethod]
        //public void InvalidDatabaseModelNullUserName()
        //{
        //    _mssqlDatabaseModel.UserName = null;
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.GetConnectionString();

        //    Assert.IsTrue(string.IsNullOrEmpty(result));
        //    Assert.IsTrue(GetException() == ResourceStrings.InvalidDatabaseModelUserName);
        //}

        //[TestMethod]
        //public void InvalidDatabaseModelNullPassword()
        //{
        //    _mssqlDatabaseModel.Password = null;
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.GetConnectionString();

        //    Assert.IsTrue(string.IsNullOrEmpty(result));
        //    Assert.IsTrue(GetException() == ResourceStrings.InvalidDatabaseModelPassword);
        //}

        //[TestMethod]
        //public void ValidateOpenConnectionReturnTrue()
        //{
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var result = _interfaceToMSSQL.OpenConnection();

        //    Assert.IsTrue(result);
        //    Assert.IsTrue(string.IsNullOrEmpty(GetException()));
        //}

        //[TestMethod]
        //public void ValidateCloseConnectionReturnTrue()
        //{
        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var openningResult = _interfaceToMSSQL.OpenConnection();
        //    var closingResult = _interfaceToMSSQL.CloseConnection();

        //    Assert.IsTrue(openningResult);
        //    Assert.IsTrue(closingResult);
        //    Assert.IsTrue(string.IsNullOrEmpty(GetException()));
        //}

        //[TestMethod]
        //public void ExecuteScriptReturnTrue()
        //{
        //    string script = "SELECT 1 GO";

        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var openningResult = _interfaceToMSSQL.OpenConnection();
        //    var executingResult = _interfaceToMSSQL.ExecuteScriptOnDatabase(script);
        //    var closingResult = _interfaceToMSSQL.CloseConnection();

        //    Assert.IsTrue(openningResult);
        //    Assert.IsTrue(executingResult);
        //    Assert.IsTrue(closingResult);
        //    Assert.IsTrue(string.IsNullOrEmpty(GetException()));
        //}

        //[TestMethod]
        //public void ExecuteScriptReturnFalse()
        //{
        //    string script = "SELECT FAIL";

        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var openningResult = _interfaceToMSSQL.OpenConnection();
        //    var executingResult = _interfaceToMSSQL.ExecuteScriptOnDatabase(script);
        //    var closingResult = _interfaceToMSSQL.CloseConnection();

        //    Assert.IsTrue(openningResult);
        //    Assert.IsFalse(executingResult);
        //    Assert.IsTrue(closingResult);
        //    Assert.IsTrue(!string.IsNullOrEmpty(GetException()));
        //}

        //[TestMethod]
        //public void ExecuteScriptsReturnTrue()
        //{
        //    string script_1 = "SELECT 1 GO";
        //    string script_2 = "SELECT 2 GO";
        //    List<string> scriptsList = new List<string>();
        //    scriptsList.Add(script_1);
        //    scriptsList.Add(script_2);

        //    _interfaceToMSSQL = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_mssqlDatabaseModel);

        //    var openningResult = _interfaceToMSSQL.OpenConnection();
        //    var executingResult = _interfaceToMSSQL.ExecuteScriptsOnDatabase(scriptsList);
        //    var closingResult = _interfaceToMSSQL.CloseConnection();

        //    Assert.IsTrue(openningResult);
        //    Assert.IsTrue(executingResult);
        //    Assert.IsTrue(closingResult);
        //    Assert.IsTrue(string.IsNullOrEmpty(GetException()));
        //}
    }
}