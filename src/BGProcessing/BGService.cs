﻿using BaseClientData;
using ErrorsAndExceptions;
using InterfaceToDatabase;
using InterfaceToDatabase.InterfceToDatabaseFactory;
using InterfaceToDatabase.SQLExecutor;
using LocalizationData;
using Reports;
using Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TreeViewItems;
using WPFViewService;

namespace BGProcessing
{
    /// <summary>
    /// Singleton design pattern class: global background service
    /// Sealed keyword prevents inheritance of this class
    /// </summary>
    public sealed class BGService
    {
        private static BGService _instance;
        private static readonly object _instanceLock = new object();
        private ResourceStrings _resourceStrings = new PublicStrings();
        private Task _currentTask;
        private ParallelOptions _loopOptions;
        private BGProgressBar _bgProgressBar;
        private BGProgressBarViewModel _bgProgressBarViewModel;
        private List<IDatabaseModel> _clientDatabaseList;
        private List<IScriptModel> _itemList;
        private IMessageBoxService _messageBoxService;
        private ISQLExecutorSettings _sqlExecutorSettings;
        private bool _useSingleTransaction;
        private static int ONE_CORE_SEQUENTIAL_EXECUTION = 1;
        private static int ONE_TRANSACTION = 1;
        private static int ONE_STEP = 1;
        private static int _numberOfScripts;
        private static int _numberOfDatabases;
        private static int _numberOfSteps;

        /// <summary>
        /// Class instance
        /// </summary>
        public static BGService Instance
        {
            get
            {
                if (_instance == null)
                {
                    // Lock thread on _instanceLock - other threads need to wait
                    // until the object get released locked area
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                            _instance = new BGService();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Open BGProgressbar view
        /// </summary>
        public void OpenProgressBar()
        {
            if (!IsProgressBarDisplayed())
            {
                _bgProgressBarViewModel = new BGProgressBarViewModel();
                _bgProgressBar = GetGBProgressBar();
                _bgProgressBar.Owner = Application.Current.MainWindow;
                _bgProgressBar.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                _bgProgressBar.DataContext = _bgProgressBarViewModel;
                _bgProgressBar.Show();
            }
            else
                _bgProgressBar.Focus();            
        }        
        
        /// <summary>
        /// Close BGProgressbar view
        /// </summary>
        public void CloseProgressBar()
        {
            if (_bgProgressBar != null)
            {
                _bgProgressBar.Close();
                _bgProgressBar = null;
            }
        }

        /// <summary>
        /// Execute all selected items on all selected databases
        /// </summary>
        /// <param name="clientDatabaseCollection"></param>
        public void ExecuteItems(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollection, ObservableCollection<ITreeViewItem> treeViewItemCollection)
        {
            if (_currentTask != null && _currentTask.Status == TaskStatus.Running)
                return;

            _clientDatabaseList = ClientDatabaseCollectionService.GetListOfSelectedDatabase(clientDatabaseCollection);
            _itemList = TreeViewItemService.GetListOfAllSelectedItems(treeViewItemCollection);
            _messageBoxService = GetMessageBoxService();
            _sqlExecutorSettings = GetSQLExecutorSettings();
            _useSingleTransaction = _sqlExecutorSettings.UseSingleTransaction;

            _currentTask = Task.Factory.StartNew(() => DoExecution());
        }

        /// <summary>
        /// Work method - sequential execution every selected items on every selected database
        /// </summary>
        private void DoExecution()
        {
            SetWorkInitialValues();

            _loopOptions = new ParallelOptions();            

            _loopOptions.CancellationToken = _bgProgressBarViewModel.TokenSource.Token;
            _loopOptions.MaxDegreeOfParallelism = ONE_CORE_SEQUENTIAL_EXECUTION;

            DoDatabaseLoop();
        }

        /// <summary>
        /// Select loop due to database model type
        /// </summary>
        private bool DoDatabaseLoop()
        {
            bool loopResult = true;

            foreach (IDatabaseModel databaseModel in _clientDatabaseList)
            {
                switch (databaseModel.DatabaseType)
                {
                    case DatabaseType.MSSQL:
                        loopResult = DoScriptsLoop(databaseModel);
                        break;
                    case DatabaseType.SQLite:
                        loopResult = DoScriptsLoop(databaseModel);
                        break;
                    case DatabaseType.SSRS:
                        loopResult = DoReportsLoop(databaseModel);
                        break;
                    default:
                        loopResult = false;
                        break;
                };

                if (loopResult != true)
                {
                    // Due to focus lost
                    ViewService.FocusOnViewOwner<BGProgressBar>();
                    return false;
                }
            }

            return DisplayCompletedMessage();
        }

        /// <summary>
        /// Connect and do script loop for database model
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool DoScriptsLoop(IDatabaseModel databaseModel)
        {
            bool loopResult = true;

            DisplayWorkInitialValue(databaseModel);

            var interfaceToDatabase = GetInterfaceDoDatabase(databaseModel);
            var connectionString = interfaceToDatabase.GetConnectionString(databaseModel);
            var connectionResult = interfaceToDatabase.OpenConnection(connectionString);

            if (connectionResult != true)
            {
                interfaceToDatabase.CloseConnection();
                interfaceToDatabase = null;
                return GetAndDisplayError();
            }

            loopResult = ExecuteScripts(interfaceToDatabase);

            interfaceToDatabase.CloseConnection();
            interfaceToDatabase = null;

            return loopResult;
        }

        /// <summary>
        /// Select mode of script execution and do
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <returns></returns>
        private bool ExecuteScripts(IInterfaceToDatabase interfaceToDatabase)
        {
            switch (_useSingleTransaction)
            {
                case true:
                    return ExecuteLoopSingleTransaction(interfaceToDatabase);
                case false:
                    return ExecuteLoop(interfaceToDatabase);
                default:
                    return true;
            }
        }

        /// <summary>
        /// Do loop for all scripts on database
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <returns></returns>
        private bool ExecuteLoop(IInterfaceToDatabase interfaceToDatabase)
        {
            var loopResult = new ParallelLoopResult();

            try
            {
                loopResult = Parallel.ForEach(_itemList, _loopOptions, (scriptModel, loopState, index) => ProcessWorkItem(interfaceToDatabase, loopState, index));

                if (loopResult.IsCompleted != true)
                {
                    DisplayCancellationMessage();

                    return GetAndDisplayError();
                }                    
            }
            catch
            {
                if (_loopOptions.CancellationToken.IsCancellationRequested == true)
                    return DisplayCancellationMessage();
            }

            return true;
        }

        /// <summary>
        /// Do loop for all scripts on database with one transaction
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <returns></returns>
        private bool ExecuteLoopSingleTransaction(IInterfaceToDatabase interfaceToDatabase)
        {
            var loopResult = new ParallelLoopResult();

            try
            {
                loopResult = Parallel.For(0, ONE_TRANSACTION, _loopOptions, (scriptModel, loopState) => ProcessWorkItem(interfaceToDatabase, loopState));

                if (loopResult.IsCompleted != true)
                    return GetAndDisplayError();
            }
            catch
            {
                if (_loopOptions.CancellationToken.IsCancellationRequested == true)                    
                    return DisplayCancellationMessage();
            }

            return true;
        }

        /// <summary>
        /// Execute all selected scripts on input database
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <param name="loopState"></param>
        /// <param name="currentScriptIndex"></param>
        private void ProcessWorkItem(IInterfaceToDatabase interfaceToDatabase, ParallelLoopState loopState, long currentScriptIndex)
        {
            var currentScript = _itemList[(int)currentScriptIndex];

            var UpdateScriptModelMethod = new Action<IScriptModel>(_bgProgressBarViewModel.UpdateScriptModel);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, UpdateScriptModelMethod, currentScript);

            var executionResult = interfaceToDatabase.ExecuteScriptOnDatabase(currentScript.Text); 
            
            if (executionResult != true)
                loopState.Break();
            else
            {
                var IncrementProgressMethod = new Action<int>(_bgProgressBarViewModel.IncrementProgress);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, IncrementProgressMethod, ONE_STEP);
            }
        }

        /// <summary>
        /// Execute all selected scripts on input database in one transaction
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <param name="loopState"></param>
        /// <param name="currentScriptIndex"></param>
        private void ProcessWorkItem(IInterfaceToDatabase interfaceToDatabase, ParallelLoopState loopState)
        {
            var IncrementProgressMethod = new Action<int>(_bgProgressBarViewModel.IncrementProgress);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, IncrementProgressMethod, _numberOfScripts);

            var scriptsToExecute = (from script in _itemList
                                    select script.Text).ToList();

            var executionResult = interfaceToDatabase.ExecuteScriptsOnDatabase(scriptsToExecute);

            if (executionResult != true)
                loopState.Break();
        }

        /// <summary>
        /// Loop execution for reports
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool DoReportsLoop(IDatabaseModel databaseModel)
        {
            bool loopResult = true;

            DisplayWorkInitialValue(databaseModel);

            _loopOptions.MaxDegreeOfParallelism = -1; // Maximum available

            loopResult = ExecuteReports(databaseModel);

            return loopResult;
        }

        /// <summary>
        /// Execute reports loop
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private bool ExecuteReports(IDatabaseModel databaseModel)
        {
            var loopResult = new ParallelLoopResult();            

            try
            {
                loopResult = Parallel.ForEach(_itemList, _loopOptions, (scriptModel, loopState, index) => ProcessWorkItem(databaseModel, loopState, index));

                if (loopResult.IsCompleted != true)
                {
                    DisplayCancellationMessage();

                    return GetAndDisplayError();
                }
            }
            catch
            {
                if (_loopOptions.CancellationToken.IsCancellationRequested == true)
                    return DisplayCancellationMessage();
            }

            return true;
        }

        /// <summary>
        /// Execute all selected reports on input database
        /// </summary>
        /// <param name="interfaceToDatabase"></param>
        /// <param name="loopState"></param>
        /// <param name="currentReportIndex"></param>
        private void ProcessWorkItem(IDatabaseModel databaseModel, ParallelLoopState loopState, long currentReportIndex)
        {
            var currentScript = _itemList[(int)currentReportIndex];

            var UpdateScriptModelMethod = new Action<IScriptModel>(_bgProgressBarViewModel.UpdateScriptModel);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, UpdateScriptModelMethod, currentScript);

            var executionResult = ReportModelService.ExecuteReport(databaseModel, currentScript);

            if (executionResult != true)
                loopState.Break();
            else
            {
                var IncrementProgressMethod = new Action<int>(_bgProgressBarViewModel.IncrementProgress);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, IncrementProgressMethod, ONE_STEP);
            }
        }

        /// <summary>
        /// Set initial values of work
        /// </summary>
        private void SetWorkInitialValues()
        {
            _numberOfScripts = _itemList.Count;
            _numberOfDatabases = _clientDatabaseList.Count;
            _numberOfSteps = _numberOfScripts * _numberOfDatabases;

            _bgProgressBarViewModel.Progress = 0;
            _bgProgressBarViewModel.ProgressMax = _numberOfSteps;
            _bgProgressBarViewModel.TokenSource = GetCancellationTokenSource();
        }

        /// <summary>
        /// Display initial information for work in UI
        /// </summary>
        /// <param name="databaseModel"></param>
        private void DisplayWorkInitialValue(IDatabaseModel databaseModel)
        {
            var UpdateDatabaseModelMethod = new Action<IDatabaseModel>(_bgProgressBarViewModel.UpdateDatabaseModel);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, UpdateDatabaseModelMethod, databaseModel);

            if (_useSingleTransaction)
                return;
            else
            {
                var scriptModel = _itemList.First();

                var UpdateScriptModelMethod = new Action<IScriptModel>(_bgProgressBarViewModel.UpdateScriptModel);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, UpdateScriptModelMethod, scriptModel);
            }
        }

        /// <summary>
        /// Display cancellation message on progressbar text
        /// </summary>
        private bool DisplayCancellationMessage()
        {
            var ShowCancellationMessageMethod = new Action(_bgProgressBarViewModel.ShowCancellationMessage);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, ShowCancellationMessageMethod);

            return false;
        }

        /// <summary>
        /// Display execution completed message on progressbar text
        /// </summary>
        /// <returns></returns>
        private bool DisplayCompletedMessage()
        {
            var ShowCompletedMessageMethod = new Action(_bgProgressBarViewModel.ShowCompletedMessage);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, ShowCompletedMessageMethod);

            return true;
        }

        /// <summary>
        /// Check is progressbar displayed
        /// </summary>
        /// <returns></returns>
        private bool IsProgressBarDisplayed()
        {
            if (_bgProgressBar != null && _bgProgressBar.IsVisible == true)
                return true;

            return false;
        }

        /// <summary>
        /// Return BGProgressbar class instance
        /// </summary>
        /// <returns></returns>
        private BGProgressBar GetGBProgressBar()
        {
            return new BGProgressBar();
        }

        /// <summary>
        /// Return new cancellation token source class instance
        /// </summary>
        /// <returns></returns>
        private CancellationTokenSource GetCancellationTokenSource()
        {
            return new CancellationTokenSource();
        }

        /// <summary>
        /// Return new instance of message box service 
        /// </summary>
        /// <returns></returns>
        private IMessageBoxService GetMessageBoxService()
        {
            return new MessageBoxService();
        }

        /// <summary>
        /// Return application settings
        /// </summary>
        /// <returns></returns>
        private ISQLExecutorSettings GetSQLExecutorSettings()
        {
            ISQLExecutorSettingsService sqlExecutorSettingsService = new SQLExecutorSettingsService();
            return sqlExecutorSettingsService.GetSQLExecutorSettings();
        }

        /// <summary>
        /// Return interfase to database model
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <returns></returns>
        private IInterfaceToDatabase GetInterfaceDoDatabase(IDatabaseModel databaseModel)
        {
            var interfaceToDatabaseFactory = new InterfaceToDatabaseFactory();

            return interfaceToDatabaseFactory.GetInterfaceToDatabase(databaseModel);
        }

        /// <summary>
        /// Get error from error service and display
        /// Error string is cleared in service after displayed
        /// </summary>
        private bool GetAndDisplayError()
        {
            var errorString = ErrorsAndExceptionsService.Instance.GetException();

            _messageBoxService.DisplayError(errorString);

            ErrorsAndExceptionsService.Instance.ClearErrorsAndExceptions();            

            return false;
        }
    }
}
