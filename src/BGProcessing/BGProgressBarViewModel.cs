﻿using BaseMVVM;
using ErrorsAndExceptions;
using InterfaceToDatabase;
using LocalizationData;
using Scripts;
using System.Threading;
using System.Windows.Input;

namespace BGProcessing
{
    /// <summary>
    /// BackGround ProgressBar ViewModel class
    /// </summary>
    public class BGProgressBarViewModel : ObservableObject
    {
        private ResourceStrings _resourceStrings = new PublicStrings();
        private IMessageBoxService _messageBoxService;
        private int _progress;
        private int _progressMax = 100;
        private string _progressMessage;
        private IDatabaseModel _databaseModel;
        private IScriptModel _scriptModel;

        /// <summary>
        /// Default constructor
        /// </summary>
        public BGProgressBarViewModel()
        {
            _messageBoxService = GetMessageBoxService();
            CancelCommand = new RelayCommand<IClosable>(CancelAction);
        }

        /// <summary>
        /// A cancellation token source for the background operations.
        /// </summary>
        internal CancellationTokenSource TokenSource { get; set; }

        public IDatabaseModel DatabaseModel
        {
            get
            {
                if (_databaseModel == null)
                    return new SQLiteDatabaseModel();
                return _databaseModel;
            }

            set
            {
                Set(ref _databaseModel, value);
            }
        }

        public IScriptModel ScriptModel
        {
            get
            {
                if (_scriptModel == null)                    
                    return new ScriptModel();
                return _scriptModel;
            }

            set
            {
                Set(ref _scriptModel, value);
            }
        }

        public int Progress
        {
            get
            {
                return _progress;
            }

            set
            {
                Set(ref _progress, value);
            }
        }

        public int ProgressMax
        {
            get
            {
                return _progressMax;
            }

            set
            {
                Set(ref _progressMax, value);
            }
        }

        public string ProgressMessage
        {
            get
            {
                if (string.IsNullOrEmpty(_progressMessage))
                    _progressMessage = ResourceStrings.ZeroPercent;
                return _progressMessage;
            }

            set
            {
                Set(ref _progressMessage, value);
            }
        }

        public ICommand CancelCommand { get; set; }

        /// <summary>
        /// Cancel and close bgprogressbar view
        /// </summary>
        /// <param name="window"></param>
        public void CancelAction(IClosable window)
        {
            // If proccess is canceled or completed - close window
            if (TokenSource.IsCancellationRequested == true || ProgressMessage == ResourceStrings.ExecutionCompleted)
            {
                if (window != null)
                    window.Close();
            }

            if (TokenSource != null)
                TokenSource.Cancel();
        }

        #region ResourceStrings

        /// <summary>
        /// Resource strings collection available in xaml file
        /// </summary>
        public ResourceStrings Strings
        {
            get
            {
                return _resourceStrings;
            }
        }

        #endregion ResourceStrings

        /// <summary>
        /// Add step of progress and update progress message
        /// </summary>
        /// <param name="step"></param>
        internal void IncrementProgress(int step)
        {
            Progress += step;
            ProgressMessage = GetProgressString();
        }

        /// <summary>
        /// Set current proccessing database model
        /// </summary>
        /// <param name="databaseModel"></param>
        internal void UpdateDatabaseModel(IDatabaseModel databaseModel)
        {
            DatabaseModel = databaseModel;
        }

        /// <summary>
        /// Set current proccessing script model
        /// </summary>
        /// <param name="scriptModel"></param>
        internal void UpdateScriptModel(IScriptModel scriptModel)
        {
            ScriptModel = scriptModel;
        }

        /// <summary>
        /// Display message after progress canceled
        /// </summary>
        internal void ShowCancellationMessage()
        {
            ProgressMessage = ResourceStrings.ExecutionCanceled;
        }

        /// <summary>
        /// Display message after proccess completed
        /// </summary>
        internal void ShowCompletedMessage()
        {
            ProgressMessage = ResourceStrings.ExecutionCompleted;
        }

        /// <summary>
        /// Return step progress as percent string
        /// </summary>
        /// <returns></returns>
        private string GetProgressString()
        {
            var floatProgress = (float)Progress / ProgressMax;
            var floatPercentProgress = floatProgress * 100;

            var stringProgress = string.Format("{0}%", ((int)floatPercentProgress).ToString());

            return stringProgress;
        }

        /// <summary>
        /// Return new instance of message box service 
        /// </summary>
        /// <returns></returns>
        private IMessageBoxService GetMessageBoxService()
        {
            return new MessageBoxService();
        }
    }
}