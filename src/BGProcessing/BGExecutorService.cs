﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BGProcessing
{
    public static class BGExecutorService
    {
        private static BGProgressBar _bgProgressBar;

        /// <summary>
        /// Open BGProgress bar
        /// </summary>
        public static void DisplayBGProgressBar()
        {
            if (_bgProgressBar == null)
            {
                _bgProgressBar = new BGProgressBar();
                _bgProgressBar.Owner = Application.Current.MainWindow;
                _bgProgressBar.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                _bgProgressBar.Show();
            }            
        }

        /// <summary>
        /// Close BGProgress bar
        /// </summary>
        private static void CloseBGProgressBar()
        {
            if (_bgProgressBar != null)
            {
                _bgProgressBar.Close();
                _bgProgressBar = null;
            }
        }

        /// <summary>
        /// Return new BGProgress bar instance
        /// </summary>
        /// <returns></returns>
        private static BGProgressBar GetBGProgressBar()
        {
            return new BGProgressBar();
        }
    }
}
