﻿using System;

namespace ErrorsAndExceptions.AbstractServices
{
    /// <summary>
    /// Result service class based on errors and exceptions global singleton service
    /// </summary>
    public abstract class AResultService
    {
        /// <summary>
        /// Method return last exception description
        /// </summary>
        /// <returns></returns>
        public string GetException()
        {
            return ErrorsAndExceptionsService.Instance.GetException();
        }

        /// <summary>
        /// Report fail as 'false' and set exception
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public bool ReportFail(Exception exception)
        {
            return ErrorsAndExceptionsService.Instance.ReportFail(exception);
        }

        /// <summary>
        /// Report fail as 'false' and set maual exception message
        /// </summary>
        /// <param name="manualExceptionMessage"></param>
        /// <returns></returns>
        public bool ReportFail(string manualExceptionMessage)
        {
            return ErrorsAndExceptionsService.Instance.ReportFail(manualExceptionMessage);
        }

        /// <summary>
        ///  Clear any of existing errors or exceptions messages
        /// </summary>
        public static void ClearErrorsAndExceptions()
        {
            ErrorsAndExceptionsService.Instance.ClearErrorsAndExceptions();
        }
    }
}