﻿using LocalizationData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ErrorsAndExceptions
{
    /// <summary>
    /// Message Box Service class
    /// </summary>
    public class MessageBoxService : IMessageBoxService
    {
        /// <summary>
        /// Display information message box
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool DisplayMessage(string message)
        {
            MessageBox.Show(message, ResourceStrings.DisplayMessageString, MessageBoxButton.OK, MessageBoxImage.Information);
            return true;
        }

        /// <summary>
        /// Display question message box
        /// </summary>
        /// <param name="questionMessage"> Input question </param>
        /// <returns></returns>
        public bool DisplayQuestion(string questionMessage)
        {
            if (MessageBox.Show(questionMessage,
                ResourceStrings.DisplayQuestionString, MessageBoxButton.YesNo, MessageBoxImage.Question)
                != MessageBoxResult.Yes)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Display warning message box
        /// </summary>
        /// <param name="warning"></param>
        public void DisplayWarning(string warning)
        {
            MessageBox.Show(warning, ResourceStrings.DisplayWarningString, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// Display error message box
        /// </summary>
        /// <param name="errorMessage"></param>
        public bool DisplayError(string errorMessage)
        {
            MessageBox.Show(errorMessage, ResourceStrings.DisplayErrorString, MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }
    }
}
