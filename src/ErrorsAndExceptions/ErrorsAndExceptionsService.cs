﻿using System;

namespace ErrorsAndExceptions
{
    /// <summary>
    /// Singleton design pattern class: global errors and exceptions service
    /// Sealed keyword prevents inheritance of this class
    /// </summary>
    public sealed class ErrorsAndExceptionsService
    {
        private static ErrorsAndExceptionsService _instance;
        private static readonly object _instanceLock = new object();
        private Exception _exception;
        private string _manualExceptionMessage;

        /// <summary>
        /// Class instance
        /// </summary>
        public static ErrorsAndExceptionsService Instance
        {
            get
            {
                if (_instance == null)
                {
                    // Lock thread on _instanceLock - other threads need to wait
                    // until the object get released locked area
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                            _instance = new ErrorsAndExceptionsService();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Method return last exception description
        /// </summary>
        /// <returns></returns>
        public string GetException()
        {
            if (_exception == null) return _manualExceptionMessage;

            return _exception.Message + "\n" +
                (_exception.InnerException != null ?
                _exception.InnerException.ToString() : string.Empty);
        }

        /// <summary>
        /// Report fail as 'false' and set exception
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public bool ReportFail(Exception exception)
        {
            _exception = exception;
            return false;
        }

        /// <summary>
        /// Report fail as 'false' and set manual exception message
        /// </summary>
        /// <param name="manualExceptionMessage"></param>
        /// <returns></returns>
        public bool ReportFail(string manualExceptionMessage)
        {
            _manualExceptionMessage = manualExceptionMessage;
            return false;
        }

        /// <summary>
        /// Clear any existings errors or exceptions message
        /// </summary>
        public void ClearErrorsAndExceptions()
        {
            _exception = null;
            _manualExceptionMessage = string.Empty;
        }
    }
}