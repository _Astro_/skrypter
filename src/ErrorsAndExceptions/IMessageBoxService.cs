﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorsAndExceptions
{
    /// <summary>
    /// Message Box Service interface
    /// </summary>
    public interface IMessageBoxService
    {
        bool DisplayMessage(string message);

        bool DisplayQuestion(string questionMessage);

        void DisplayWarning(string warning);

        bool DisplayError(string errorMessage);
    }
}
