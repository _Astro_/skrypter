﻿namespace InterfaceToDatabase
{
    /// <summary>
    /// Interface of database model class
    /// </summary>
    public interface IDatabaseModel
    {
        DatabaseType DatabaseType { get; set; }

        string ID { get; set; }

        string ClientName { get; set; }

        string ServerAddress { get; set; }

        string DatabaseName { get; set; }

        string DatabaseAliasName { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        string DatabasePath { get; set; }

        int ConnectionTimeout { get; set; }

        bool? IsSelected { get; set; }
    }
}