﻿namespace InterfaceToDatabase
{
    /// <summary>
    /// Enumeration of connection timeouts
    /// </summary>
    public enum ConnectionTimeoutEnum
    {
        Minimum = 1,
        Five = 5,
        Ten = 10,
        Fifteen = 15,
        Thirty = 30,
        Sixty = 60,
        Ninety = 90,
        Maximum = 180
    }
}