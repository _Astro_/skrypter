﻿using System.Data.Entity.ModelConfiguration;

namespace InterfaceToDatabase.DAL
{
    /// <summary>
    /// Entity Framework Model Map class for MSSQL Server database model
    /// </summary>
    public class MSSQLDatabaseModelMap : EntityTypeConfiguration<MSSQLDatabaseModel>
    {
        public MSSQLDatabaseModelMap()
        {
            HasKey(t => t.ID);

            Property(t => t.DatabaseType);
            Property(t => t.ID);
            Property(t => t.ClientName);
            Property(t => t.DatabaseName);
            Property(t => t.DatabaseAliasName);
            Property(t => t.ServerAddress);
            Property(t => t.UserName);
            Property(t => t.Password);
            Property(t => t.ConnectionTimeout);

            Ignore(t => t.IsSelected);

            ToTable(RepositoryDatabaseTable.MSSQLDatabaseModelTable.ToString());
        }
    }
}