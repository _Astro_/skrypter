﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceToDatabase.DAL
{
    /// <summary>
    /// Entity Framework Model Map class for SSRS model
    /// </summary>
    public class SSRSModelMap : EntityTypeConfiguration<SSRSModel>
    {
        public SSRSModelMap()
        {
            HasKey(t => t.ID);

            Property(t => t.DatabaseType);
            Property(t => t.ID);
            Property(t => t.ClientName);
            Property(t => t.DatabaseName);
            Property(t => t.DatabaseAliasName);
            Property(t => t.ServerAddress);
            Property(t => t.UserName);
            Property(t => t.Password);

            Ignore(t => t.IsSelected);

            ToTable(RepositoryDatabaseTable.SSRSModelTable.ToString());
        }
    }
}
