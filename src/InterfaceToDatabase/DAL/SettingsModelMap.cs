﻿using InterfaceToDatabase.SQLExecutor;
using System.Data.Entity.ModelConfiguration;

namespace InterfaceToDatabase.DAL
{
    /// <summary>
    /// Entity Framework Model Map class for Settings model
    /// </summary>
    public class SettingsModelMap : EntityTypeConfiguration<SettingsModel>
    {
        public SettingsModelMap()
        {
            HasKey(t => t.Name);

            Property(t => t.Name);
            Property(t => t.Value);

            ToTable(RepositoryDatabaseTable.SettingsModelTable.ToString());
        }
    }
}