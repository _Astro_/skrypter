﻿using System.Data.Entity.ModelConfiguration;

namespace InterfaceToDatabase.DAL
{
    /// <summary>
    /// Entity Framework Model Map class for SQLite database model
    /// </summary>
    public class SQLiteDatabaseModelMap : EntityTypeConfiguration<SQLiteDatabaseModel>
    {
        public SQLiteDatabaseModelMap()
        {
            HasKey(t => t.ID);

            Property(t => t.DatabaseType);
            Property(t => t.ID);
            Property(t => t.ClientName);
            Property(t => t.DatabaseName);
            Property(t => t.DatabaseAliasName);
            Property(t => t.Password);
            Property(t => t.DatabasePath);

            Ignore(t => t.IsSelected);

            ToTable(RepositoryDatabaseTable.SQLiteDatabaseModelTable.ToString());
        }
    }
}