﻿using InterfaceToDatabase.SQLExecutor;
using SQLite.CodeFirst;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;

namespace InterfaceToDatabase.DAL
{
    /// <summary>
    /// Entity Framework Database context class
    /// </summary>
    public partial class EFDatabaseContext : DbContext
    {
        /// <summary>
        /// Default ctor or database context class
        /// </summary>
        /// <param name="connectionString"></param>
        public EFDatabaseContext(string connectionString)
            : base(new SQLiteConnection() { ConnectionString = connectionString }, true)
        {
        }

        /// <summary>
        /// Configuration of database model builder
        /// </summary>
        /// <param name="dbModelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder dbModelBuilder)
        {
            dbModelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            dbModelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            dbModelBuilder.Configurations.Add(new MSSQLDatabaseModelMap());
            dbModelBuilder.Configurations.Add(new SSRSModelMap());
            dbModelBuilder.Configurations.Add(new SQLiteDatabaseModelMap());
            dbModelBuilder.Configurations.Add(new SettingsModelMap());

            Database.SetInitializer(new SqliteCreateDatabaseIfNotExists<EFDatabaseContext>(dbModelBuilder));
        }

        public virtual DbSet<MSSQLDatabaseModel> MSSQLDatabaseModel { get; set; }

        public virtual DbSet<SSRSModel> SSRSModel { get; set; }

        public virtual DbSet<SQLiteDatabaseModel> SQLiteDatabaseModel { get; set; }

        public virtual DbSet<SettingsModel> SettingsModel { get; set; }
    }
}