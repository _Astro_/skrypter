﻿using InterfaceToDatabase.InterfceToDatabaseFactory;
using LocalizationData;
using System;

namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// SQLExecutor database class
    /// </summary>
    public static class SQLExecutorDatabase
    {
        private static IInterfaceToDatabaseFactory _interfaceToDatabaseFactory;
        private static IInterfaceToDatabase _interfaceToSQLite;
        private static SQLiteDatabaseModel _sqliteDatabaseModel;
        private static string _databaseFilePath = AppDomain.CurrentDomain.BaseDirectory;
        private static string _clientName = ResourceStrings.SQLExecutorClientName;
        private static string _databaseName = ResourceStrings.SQLExecutorDatabaseName;
        private static string _databasePassword = ResourceStrings.SQLExecutorDatabasePassword;

        /// <summary>
        /// Return connection string to SQLExecutor database
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            _sqliteDatabaseModel = new SQLiteDatabaseModel()
            {
                ClientName = _clientName,
                DatabaseName = _databaseName,
                DatabasePath = _databaseFilePath,
                Password = _databasePassword
            };
            _interfaceToDatabaseFactory = new InterfaceToDatabaseFactory();
            _interfaceToSQLite = _interfaceToDatabaseFactory.GetInterfaceToDatabase(_sqliteDatabaseModel);

            return _interfaceToSQLite.GetConnectionString(_sqliteDatabaseModel);
        }
    }
}