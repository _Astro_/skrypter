﻿using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase.Repository;
using InterfaceToDatabase.Repository.SQLExecutorUnitOfWorkFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tools;

namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// SQLExecutor settings service class
    /// </summary>
    public class SQLExecutorSettingsService : AResultService, ISQLExecutorSettingsService
    {
        private IUnitOfWork _unitOfWork;

        /// <summary>
        /// Default ctor
        /// </summary>
        public SQLExecutorSettingsService()
        {
            _unitOfWork = GetUnitOfWork();
        }

        /// <summary>
        /// Return SQLExecutor settings class from repository
        /// </summary>
        /// <returns></returns>
        public ISQLExecutorSettings GetSQLExecutorSettings()
        {
            var settingsList = _unitOfWork.SettingsModel.GetAll().ToList();
            return GetSettingsModelFromListOfSettings(settingsList);
        }

        /// <summary>
        /// Update SQLExecutor settings
        /// </summary>
        /// <param name="sqlExecutorSettings"> Actual settings to update </param>
        public bool UpdateSQLExecutorSettings(ISQLExecutorSettings sqlExecutorSettings)
        {
            var settingsList = _unitOfWork.SettingsModel.GetAll().ToList();

            Type objectType = typeof(ISQLExecutorSettings);
            List<PropertyInfo> propertyInfoList = objectType.GetProperties().ToList();

            try
            {
                foreach (PropertyInfo propertyInfo in propertyInfoList)
                {
                    var settingsModel = _unitOfWork.SettingsModel.GetByExpressions(s => s.Name == propertyInfo.Name).FirstOrDefault();

                    if (settingsModel != null)
                        UpdateSettingsModel(propertyInfo, settingsModel, sqlExecutorSettings);
                    else
                        AddSettingsModel(propertyInfo, sqlExecutorSettings);
                }

                _unitOfWork.Commit();
                return true;
            }
            catch (Exception exception)
            {
                return ReportFail(exception);
            }
        }

        /// <summary>
        /// Add new settings model to repository from SQLExecutor settings model class
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="sqlExecutorSettings"></param>
        private void AddSettingsModel(PropertyInfo propertyInfo, ISQLExecutorSettings sqlExecutorSettings)
        {
            SettingsModel settingsModel = new SettingsModel()
            {
                Name = propertyInfo.Name,
                Value = propertyInfo.GetValue(sqlExecutorSettings) != null ?
                    propertyInfo.GetValue(sqlExecutorSettings).ToString() : string.Empty
            };

            _unitOfWork.SettingsModel.Add(settingsModel);
        }

        /// <summary>
        /// Update settings model in repository from SQLExecutor settings model class
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="settingsModel"></param>
        /// <param name="sqlExecutorSettings"></param>
        private void UpdateSettingsModel(PropertyInfo propertyInfo, SettingsModel settingsModel, ISQLExecutorSettings sqlExecutorSettings)
        {
            settingsModel.Value = propertyInfo.GetValue(sqlExecutorSettings) != null ?
                propertyInfo.GetValue(sqlExecutorSettings).ToString() : string.Empty;

            _unitOfWork.SettingsModel.Update(settingsModel);
        }

        /// <summary>
        /// Return SQLExecutor settings model class from list of basic settings models
        /// </summary>
        /// <param name="settingsModelList"> List of basic settings models </param>
        /// <returns></returns>
        private ISQLExecutorSettings GetSettingsModelFromListOfSettings(List<SettingsModel> settingsModelList)
        {
            ISQLExecutorSettings sqlExecutorSettings = new SQLExecutorSettings();

            Type objectType = typeof(ISQLExecutorSettings);

            foreach (ISettingsModel settingsModel in settingsModelList)
            {
                PropertyInfo propertyInfo = objectType.GetProperty(settingsModel.Name);

                if (propertyInfo != null)
                    ObjectService.SetValue(propertyInfo, sqlExecutorSettings, settingsModel.Value);
            }

            return sqlExecutorSettings;
        }

        /// <summary>
        /// Return new SQLExecutor unit of work instance
        /// </summary>
        /// <returns></returns>
        private IUnitOfWork GetUnitOfWork()
        {
            return UnitOfWorkFactory.GetUnitOfWork(SQLExecutorDatabase.GetConnectionString());
        }
    }
}