﻿using System.Text;

namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// Interface of SQLExecutor settings model class
    /// </summary>
    public interface ISQLExecutorSettings
    {
        string DefaultStartingInterface { get; set; }

        bool UseSingleTransaction { get; set; }

        bool IncludeHiddenFilesAndFolders { get; set; }

        bool IncludeTemporaryFilesAndFolders { get; set; }

        string DefaultEncoding { get; set; }
    }
}