﻿namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// Interface of SQLExecutor settings service class
    /// </summary>
    public interface ISQLExecutorSettingsService
    {
        ISQLExecutorSettings GetSQLExecutorSettings();

        bool UpdateSQLExecutorSettings(ISQLExecutorSettings sqlExecutorSettings);
    }
}