﻿using BaseMVVM;

namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// Settings model class
    /// </summary>
    public class SettingsModel : ObservableObject, ISettingsModel
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}