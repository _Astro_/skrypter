﻿namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// Interface of settings model class
    /// </summary>
    public interface ISettingsModel
    {
        string Name { get; set; }

        string Value { get; set; }
    }
}