﻿using BaseMVVM;
using System.Text;

namespace InterfaceToDatabase.SQLExecutor
{
    /// <summary>
    /// SQLExecutor settings model class
    /// </summary>
    public class SQLExecutorSettings : ObservableObject, ISQLExecutorSettings
    {
        private string _defaultStartingInterface;
        private bool _useSingleTransaction;
        private bool _includeHiddenFilesAndFolders;
        private bool _includeTemporaryFilesAndFolders;
        private string _defaultEncoding;

        public string DefaultStartingInterface
        {
            get
            {
                return _defaultStartingInterface;
            }
            set
            {
                Set(ref _defaultStartingInterface, value);
            }
        }

        public bool UseSingleTransaction
        {
            get
            {
                return _useSingleTransaction;
            }
            set
            {
                Set(ref _useSingleTransaction, value);
            }
        }

        public bool IncludeHiddenFilesAndFolders
        {
            get
            {
                return _includeHiddenFilesAndFolders;
            }
            set
            {
                Set(ref _includeHiddenFilesAndFolders, value);
            }
        }

        public bool IncludeTemporaryFilesAndFolders
        {
            get
            {
                return _includeTemporaryFilesAndFolders;
            }
            set
            {
                Set(ref _includeTemporaryFilesAndFolders, value);
            }
        }

        public string DefaultEncoding
        {
            get
            {
                return _defaultEncoding;
            }
            set
            {
                Set(ref _defaultEncoding, value);
            }
        }
    }
}