﻿namespace InterfaceToDatabase
{
    /// <summary>
    /// Database type enumeration
    /// </summary>
    public enum DatabaseType
    {
        UNDEFINED,
        MSSQL,
        SSRS,
        SQLite,
        MySQL,
        PostgresSQL
    }
}