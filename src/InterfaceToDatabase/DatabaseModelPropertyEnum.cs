﻿namespace InterfaceToDatabase
{
    /// <summary>
    /// Enumeration of database model properties
    /// </summary>
    public enum DatabaseModelPropertyEnum
    {
        DatabaseType,
        ID,
        ClientName,
        ServerAddress,
        DatabaseName,
        DatabaseAliasName,
        UserName,
        Password,
        DatabasePath,
        ConnectionTimeout
    }
}