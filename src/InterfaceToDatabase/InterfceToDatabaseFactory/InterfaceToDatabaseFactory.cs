﻿namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Factory of interface to database class
    /// </summary>
    public class InterfaceToDatabaseFactory : IInterfaceToDatabaseFactory
    {
        public IInterfaceToDatabase GetInterfaceToDatabase(IDatabaseModel databaseModel)
        {
            DatabaseType databaseType =
                databaseModel != null ? databaseModel.DatabaseType : DatabaseType.UNDEFINED;

            switch (databaseType)
            {
                case DatabaseType.MSSQL:
                    return new InterfaceToMSSQL();

                case DatabaseType.SQLite:
                    return new InterfaceToSQLite();

                case DatabaseType.SSRS:
                    return new InterfaceToSSRS();

                default:
                    return null;
            }
        }
    }
}