﻿using System.Collections.Generic;
using System.Data;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Interface of interface to database class
    /// </summary>
    public interface IInterfaceToDatabase
    {
        IDbConnection DbConnection { get; }

        string GetConnectionString(IDatabaseModel databaseModel);

        bool OpenConnection(string connectionString);

        bool CloseConnection();

        bool CheckConnection(string connectionString);

        bool ExecuteScriptOnDatabase(string script);

        bool ExecuteScriptsOnDatabase(List<string> listOfScripts);

        bool ValidateDatabaseModel(IDatabaseModel databaseModel);
    }
}