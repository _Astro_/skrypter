﻿using System.Data;
using Microsoft.Data.SqlClient;
using System.Data.SQLite;

namespace InterfaceToDatabase.InterfceToDatabaseFactory.DbConnectFactory
{
    /// <summary>
    /// DbConnection factory class: factory method design pattern
    /// </summary>
    public static class DbConnectionFactory
    {
        private static IDbConnection _customDbConnection = null;

        public static IDbConnection GetDbConnection(DatabaseType databaseType, string connectionString)
        {
            if (_customDbConnection != null)
                return _customDbConnection;

            switch (databaseType)
            {
                case DatabaseType.MSSQL:
                    return new SqlConnection(connectionString);

                case DatabaseType.SQLite:
                    return new SQLiteConnection(connectionString);

                default:
                    return null;
            }
        }

        public static void SetDbConnection(IDbConnection dbConnection)
        {
            _customDbConnection = dbConnection;
        }
    }
}