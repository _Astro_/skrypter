﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Data;
using Microsoft.Data.SqlClient;

namespace InterfaceToDatabase.InterfceToDatabaseFactory.MSSQLSmoServerFactory
{
    public static class SmoServerFactory
    {
        private static ISmoServer _customSmoServer;

        public static ISmoServer GetSmoServer(IDbConnection dbConnection)
        {
            if (_customSmoServer != null)
                return _customSmoServer;

            return new SmoServer()
            {
                Server = new Server(new ServerConnection((SqlConnection)dbConnection))
            };
        }

        public static void SetSmoServer(ISmoServer smoServer)
        {
            _customSmoServer = smoServer;
        }
    }
}