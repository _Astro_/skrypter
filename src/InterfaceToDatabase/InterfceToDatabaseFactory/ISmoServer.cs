﻿using Microsoft.SqlServer.Management.Smo;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Smo server interface
    /// </summary>
    public interface ISmoServer
    {
        Server Server { get; set; }
    }
}