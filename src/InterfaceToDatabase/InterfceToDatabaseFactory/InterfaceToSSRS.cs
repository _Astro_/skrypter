﻿using ErrorsAndExceptions.AbstractServices;
using LocalizationData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Interface to SSRS class
    /// </summary>
    public class InterfaceToSSRS : AResultService, IInterfaceToDatabase
    {
        /// <summary>
        /// Never used
        /// </summary>
        public IDbConnection DbConnection
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Get connection string from database model for connection check
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        public string GetConnectionString(IDatabaseModel databaseModel)
        {
            if (ValidateDatabaseModel(databaseModel))
                return BuildConnectionString(databaseModel);

            return string.Empty;
        }

        /// <summary>
        /// Never used - constant return true
        /// </summary>
        /// <param name="connectionString"> Database connection string </param>
        /// <returns></returns>
        public bool OpenConnection(string connectionString)
        {
            return true;
        }
        /// <summary>
        /// Never used - constant return true
        /// </summary>
        public bool CloseConnection()
        {
            return true;
        }

        /// <summary>
        /// Check connection to SSRS from connection string
        /// </summary>
        /// <param name="connectionString"> Input connection string </param>
        /// <returns></returns>
        public bool CheckConnection(string connectionString)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                var processResult = string.Empty;

                ProcessStartInfo processStartInfo = new ProcessStartInfo();
                SetProcessStartInfoParms(processStartInfo, connectionString);

                Process process = new Process();
                process.StartInfo = processStartInfo;
                bool processStarted = process.Start();

                if (processStarted)
                {
                    process.StandardInput.Flush();
                    process.StandardInput.Close();
                    processResult += process.StandardError.ReadToEnd();
                    process.WaitForExit();

                    if (processResult != string.Empty)
                        return ReportFail(processResult);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Not supported - return false
        /// </summary>
        /// <param name="script"> Script text </param>
        /// <returns></returns>
        public bool ExecuteScriptOnDatabase(string script)
        {
            return false;
        }

        /// <summary>
        /// Not supported - return false
        /// </summary>
        /// <param name="listOfScripts"> List of script text </param>
        /// <returns></returns>
        public bool ExecuteScriptsOnDatabase(List<string> listOfScripts)
        {
            return false;
        }

        /// <summary>
        /// SSRS model validation method
        /// </summary>
        /// <param name="databaseModel"> Input model </param>
        /// <returns></returns>
        public bool ValidateDatabaseModel(IDatabaseModel databaseModel)
        {
            if (!ValidateDatabaseModelIsObject(databaseModel)) return false;
            if (!ValidateDatabaseModelClientName(databaseModel)) return false;
            if (!ValidateDatabaseModelServerAddress(databaseModel)) return false;
            if (!ValidateDatabaseModelDatabaseName(databaseModel)) return false;
            if (!ValidateDatabaseModelUserName(databaseModel)) return false;
            if (!ValidateDatabaseModelPassword(databaseModel)) return false;

            return true;
        }

        private bool ValidateDatabaseModelIsObject(IDatabaseModel databaseModel)
        {
            if (databaseModel == null)
                return ReportFail(ResourceStrings.InvalidDatabaseModelObjectNull);
            return true;
        }

        private bool ValidateDatabaseModelClientName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.ClientName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelClientName);
            return true;
        }

        private bool ValidateDatabaseModelServerAddress(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.ServerAddress))
                return ReportFail(ResourceStrings.InvalidDatabaseModelServerAddress);
            return true;
        }

        private bool ValidateDatabaseModelDatabaseName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.DatabaseName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelDatabaseName);
            return true;
        }

        private bool ValidateDatabaseModelUserName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.UserName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelUserName);
            return true;
        }

        private bool ValidateDatabaseModelPassword(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.Password))
                return ReportFail(ResourceStrings.InvalidDatabaseModelPassword);
            return true;
        }

        /// <summary>
        /// Set parameters for process
        /// </summary>
        /// <param name="processStartInfo"></param>
        /// <param name="connectionCommand"></param>
        private static void SetProcessStartInfoParms(ProcessStartInfo processStartInfo, string connectionCommand)
        {
            processStartInfo.Arguments = connectionCommand;
            processStartInfo.CreateNoWindow = true;
            processStartInfo.FileName = ResourceStrings.ReportServerExecName;
            processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.RedirectStandardInput = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;
        }

        /// <summary>
        /// Return connection string from SSRS model for connection check only
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        private string BuildConnectionString(IDatabaseModel databaseModel)
        {
            if (databaseModel == null || databaseModel.DatabaseType != DatabaseType.SSRS) return string.Empty;

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format(@"-i ""{0}{1}"" ", AppDomain.CurrentDomain.BaseDirectory, ResourceStrings.SSRSTestConnectionFileName));
            builder.Append(string.Format(@"-s ""{0}"" ", databaseModel.ServerAddress.Replace(@"/", @"//").Replace(@"////", @"//")));
            builder.Append(string.Format(@"-u ""{0}"" ", databaseModel.UserName));
            builder.Append(string.Format(@"-p ""{0}"" ", databaseModel.Password));

            return builder.ToString();
        }
    }
}
