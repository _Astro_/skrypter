﻿namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Interface of interface to database factory
    /// </summary>
    public interface IInterfaceToDatabaseFactory
    {
        IInterfaceToDatabase GetInterfaceToDatabase(IDatabaseModel databaseModel);
    }
}