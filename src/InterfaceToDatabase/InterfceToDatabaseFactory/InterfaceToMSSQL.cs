﻿using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase.InterfceToDatabaseFactory.DbConnectFactory;
using InterfaceToDatabase.InterfceToDatabaseFactory.MSSQLSmoServerFactory;
using LocalizationData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Interfase to MSSQL database class
    /// </summary>
    public class InterfaceToMSSQL : AResultService, IInterfaceToDatabase
    {
        private const DatabaseType INTERFACE_DATABASE_TYPE = DatabaseType.MSSQL;
        private string _connectionString;
        private IDbConnection _dbConnection;
        private ISmoServer _smoServer;

        public IDbConnection DbConnection
        {
            get
            {
                return _dbConnection;
            }
        }

        /// <summary>
        /// Get connection string from database model
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        public string GetConnectionString(IDatabaseModel databaseModel)
        {
            if (ValidateDatabaseModel(databaseModel))
                return BuildConnectionString(databaseModel);

            return string.Empty;
        }

        /// <summary>
        /// Open connection to database from model
        /// </summary>
        /// <param name="connectionString"> Database connection string </param>
        /// <returns></returns>
        public bool OpenConnection(string connectionString)
        {
            _connectionString = connectionString;
            if (string.IsNullOrEmpty(_connectionString))
                return false;

            try
            {
                _dbConnection = DbConnectionFactory.GetDbConnection(INTERFACE_DATABASE_TYPE, _connectionString);
                _smoServer = SmoServerFactory.GetSmoServer(_dbConnection);
                _smoServer.Server.ConnectionContext.Connect();

                return true;
            }
            catch (Exception exception)
            {
                return ReportFail(exception);
            }
        }

        /// <summary>
        /// Close connection if open
        /// </summary>
        public bool CloseConnection()
        {
            try
            {
                if (_smoServer.Server != null && _smoServer.Server.ConnectionContext.IsOpen)
                    _smoServer.Server.ConnectionContext.Disconnect();

                if (_dbConnection != null && _dbConnection.State != ConnectionState.Closed)
                {
                    _dbConnection.Close();
                    _dbConnection.Dispose();
                }
                return true;
            }
            catch (Exception exception)
            {
                return ReportFail(exception);
            }
        }

        /// <summary>
        /// Check connection to database from connection string
        /// </summary>
        /// <param name="connectionString"> Input connection string </param>
        /// <returns></returns>
        public bool CheckConnection(string connectionString)
        {
            if (!OpenConnection(connectionString))
                return false;

            CloseConnection();

            return true;
        }

        /// <summary>
        /// Execute single script on database
        /// </summary>
        /// <param name="script"> Script text </param>
        /// <returns></returns>
        public bool ExecuteScriptOnDatabase(string script)
        {
            if (_smoServer.Server == null || !_smoServer.Server.ConnectionContext.IsOpen)
                return ReportFail(ResourceStrings.ConnectionIsNotOpen);

            _smoServer.Server.ConnectionContext.BeginTransaction();
            try
            {
                _smoServer.Server.ConnectionContext.ExecuteNonQuery(script);
                _smoServer.Server.ConnectionContext.CommitTransaction();
                return true;
            }
            catch (Exception exception)
            {
                _smoServer.Server.ConnectionContext.RollBackTransaction();
                return ReportFail(exception);
            }
        }

        /// <summary>
        /// Execute list of scripts on database with one transaction
        /// </summary>
        /// <param name="listOfScripts"> List of script text </param>
        /// <returns></returns>
        public bool ExecuteScriptsOnDatabase(List<string> listOfScripts)
        {
            if (_smoServer.Server == null || !_smoServer.Server.ConnectionContext.IsOpen)
                return ReportFail(ResourceStrings.ConnectionIsNotOpen);

            _smoServer.Server.ConnectionContext.BeginTransaction();

            foreach (string script in listOfScripts)
            {
                try
                {
                    _smoServer.Server.ConnectionContext.ExecuteNonQuery(script);
                }
                catch (Exception exception)
                {
                    _smoServer.Server.ConnectionContext.RollBackTransaction();
                    return ReportFail(exception);
                }
            }
            _smoServer.Server.ConnectionContext.CommitTransaction();
            return true;
        }

        /// <summary>
        /// MSSQL Database Model validation method
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        public bool ValidateDatabaseModel(IDatabaseModel databaseModel)
        {
            if (!ValidateDatabaseModelIsObject(databaseModel)) return false;
            if (!ValidateDatabaseModelClientName(databaseModel)) return false;
            if (!ValidateDatabaseModelServerAddress(databaseModel)) return false;
            if (!ValidateDatabaseModelDatabaseName(databaseModel)) return false;
            if (!ValidateDatabaseModelUserName(databaseModel)) return false;
            if (!ValidateDatabaseModelPassword(databaseModel)) return false;

            return true;
        }

        private bool ValidateDatabaseModelIsObject(IDatabaseModel databaseModel)
        {
            if (databaseModel == null)
                return ReportFail(ResourceStrings.InvalidDatabaseModelObjectNull);
            return true;
        }

        private bool ValidateDatabaseModelClientName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.ClientName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelClientName);
            return true;
        }

        private bool ValidateDatabaseModelServerAddress(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.ServerAddress))
                return ReportFail(ResourceStrings.InvalidDatabaseModelServerAddress);
            return true;
        }

        private bool ValidateDatabaseModelDatabaseName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.DatabaseName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelDatabaseName);
            return true;
        }

        private bool ValidateDatabaseModelUserName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.UserName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelUserName);
            return true;
        }

        private bool ValidateDatabaseModelPassword(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.Password))
                return ReportFail(ResourceStrings.InvalidDatabaseModelPassword);
            return true;
        }

        /// <summary>
        /// Return connection string from MSSQL Database Model
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        private string BuildConnectionString(IDatabaseModel databaseModel)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("Server={0};", databaseModel.ServerAddress));
            builder.Append(string.Format("User ID={0};", databaseModel.UserName));
            builder.Append(string.Format("Password={0};", databaseModel.Password));
            builder.Append(string.Format("Database={0};", databaseModel.DatabaseName));
            builder.Append(string.Format("Connection Timeout={0}",
                databaseModel.ConnectionTimeout == 0 ? 1 : databaseModel.ConnectionTimeout));

            return builder.ToString();
        }
    }
}