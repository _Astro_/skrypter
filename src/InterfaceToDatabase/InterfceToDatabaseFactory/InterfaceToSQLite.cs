﻿using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase.InterfceToDatabaseFactory.DbConnectFactory;
using LocalizationData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Interface to SQLite database class
    /// </summary>
    public class InterfaceToSQLite : AResultService, IInterfaceToDatabase
    {
        private const DatabaseType INTERFACE_DATABASE_TYPE = DatabaseType.SQLite;
        private const int DEFAULT_VERSION = 3;
        private string _connectionString;
        private IDbConnection _dbConnection;
        private SQLiteTransaction _transaction;
        private SQLiteCommand _command;
        private const string TEST_CONNECTION_COMMAND = "SELECT 1";

        public IDbConnection DbConnection
        {
            get
            {
                return _dbConnection;
            }
        }

        /// <summary>
        /// Get connection string from database model
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        public string GetConnectionString(IDatabaseModel databaseModel)
        {
            if (ValidateDatabaseModel(databaseModel))
                return BuildConnectionString(databaseModel);

            return string.Empty;
        }

        /// <summary>
        /// Open connection to database from model
        /// </summary>
        /// <param name="connectionString"> Database connection string </param>
        /// <returns></returns>
        public bool OpenConnection(string connectionString)
        {
            _connectionString = connectionString;
            if (string.IsNullOrEmpty(_connectionString))
                return false;
            try
            {
                _dbConnection = DbConnectionFactory.GetDbConnection(INTERFACE_DATABASE_TYPE, _connectionString);
                _dbConnection.Open();
            }
            catch (Exception exception)
            {
                return ReportFail(exception);
            }

            return true;
        }

        /// <summary>
        /// Close connection if open
        /// </summary>
        public bool CloseConnection()
        {
            try
            {
                if (_dbConnection != null && _dbConnection.State != ConnectionState.Closed)
                {
                    _dbConnection.Close();
                    _dbConnection.Dispose();
                    _dbConnection = null;
                }
                return true;
            }
            catch (Exception exception)
            {
                return ReportFail(exception);
            }
        }

        /// <summary>
        /// Check connection to database from connection string
        /// </summary>
        /// <param name="connectionString"> Input connection string </param>
        /// <returns></returns>
        public bool CheckConnection(string connectionString)
        {
            if (!DatabaseExists(connectionString))
                return ReportFail(ResourceStrings.SQLiteDatabaseNotExists);

            if (!OpenConnection(connectionString))
                return false;
            // Because connection can be openned without password,
            // but script can't be executed without password
            if (!ExecuteScriptOnDatabase(TEST_CONNECTION_COMMAND))
                return false;

            CloseConnection();

            return true;
        }

        /// <summary>
        /// Execute script on database from database model
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public bool ExecuteScriptOnDatabase(string script)
        {
            if (_dbConnection == null && _dbConnection.State == ConnectionState.Closed)
                return ReportFail(ResourceStrings.ConnectionIsNotOpen);

            _transaction = ((SQLiteConnection)_dbConnection).BeginTransaction();
            _command = ((SQLiteConnection)_dbConnection).CreateCommand();
            _command.Transaction = _transaction;
            _command.CommandText = script;

            try
            {
                _command.ExecuteNonQuery();
                _transaction.Commit();

                return true;
            }
            catch (Exception exception)
            {
                _transaction.Rollback();
                return ReportFail(exception);
            }
            finally
            {
                _transaction = null;
                _command = null;
            }
        }

        /// <summary>
        /// Execute list of scripts on database with one transaction
        /// </summary>
        /// <param name="listOfScripts"> List of script text </param>
        /// <returns></returns>
        public bool ExecuteScriptsOnDatabase(List<string> listOfScripts)
        {
            if (_dbConnection == null && _dbConnection.State == ConnectionState.Closed)
                return ReportFail(ResourceStrings.ConnectionIsNotOpen);

            _transaction = ((SQLiteConnection)_dbConnection).BeginTransaction();
            _command = ((SQLiteConnection)_dbConnection).CreateCommand();
            _command.Transaction = _transaction;

            foreach (string script in listOfScripts)
            {
                try
                {
                    _command.CommandText = script;
                    _command.ExecuteNonQuery();

                    return true;
                }
                catch (Exception exception)
                {
                    _transaction.Rollback();
                    return ReportFail(exception);
                }
            }
            _transaction.Commit();
            _transaction = null;
            _command = null;
            return true;
        }

        /// <summary>
        /// SQLite Database Model validation method
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        public bool ValidateDatabaseModel(IDatabaseModel databaseModel)
        {
            if (!ValidateDatabaseModelIsObject(databaseModel)) return false;
            if (!ValidateDatabaseModelClientName(databaseModel)) return false;
            if (!ValidateDatabaseModelDatabaseName(databaseModel)) return false;
            if (!ValidateDatabaseModelPassword(databaseModel)) return false;
            if (!ValidateDatabaseModelDatabasePatch(databaseModel)) return false;

            return true;
        }

        private bool ValidateDatabaseModelIsObject(IDatabaseModel databaseModel)
        {
            if (databaseModel == null)
                return ReportFail(ResourceStrings.InvalidDatabaseModelObjectNull);
            return true;
        }

        private bool ValidateDatabaseModelClientName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.ClientName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelClientName);
            return true;
        }

        private bool ValidateDatabaseModelDatabaseName(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.DatabaseName))
                return ReportFail(ResourceStrings.InvalidDatabaseModelDatabaseName);
            return true;
        }

        private bool ValidateDatabaseModelPassword(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.Password))
                return ReportFail(ResourceStrings.InvalidDatabaseModelPassword);
            return true;
        }

        private bool ValidateDatabaseModelDatabasePatch(IDatabaseModel databaseModel)
        {
            if (string.IsNullOrEmpty(databaseModel.DatabasePath))
                return ReportFail(ResourceStrings.InvalidDatabaseModelDatabasePath);
            return true;
        }

        /// <summary>
        /// Return connection string from SQLite Database Model
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        private string BuildConnectionString(IDatabaseModel databaseModel)
        {
            SQLiteConnectionStringBuilder connectionStringBuilder = new SQLiteConnectionStringBuilder();
            connectionStringBuilder.DataSource =
                string.Format("{0}\\{1}",
                databaseModel.DatabasePath,
                databaseModel.DatabaseName);
            connectionStringBuilder.Password = databaseModel.Password;
            connectionStringBuilder.Version = DEFAULT_VERSION;

            return connectionStringBuilder.ToString();
        }

        /// <summary>
        /// Check if database file exists
        /// </summary>
        /// <param name="connectionString"> Connection string to database </param>
        /// <returns></returns>
        private bool DatabaseExists(string connectionString)
        {
            SQLiteConnectionStringBuilder connectionStringBuilder = new SQLiteConnectionStringBuilder(connectionString);

            return File.Exists(connectionStringBuilder.DataSource);
        }
    }
}