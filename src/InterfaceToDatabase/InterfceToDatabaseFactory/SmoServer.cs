﻿using Microsoft.SqlServer.Management.Smo;

namespace InterfaceToDatabase.InterfceToDatabaseFactory
{
    /// <summary>
    /// Smo server class
    /// </summary>
    public class SmoServer : ISmoServer
    {
        private Server _server;

        public Server Server
        {
            get
            {
                return _server;
            }
            set
            {
                _server = value;
            }
        }
    }
}