﻿namespace InterfaceToDatabase
{
    /// <summary>
    /// Service class for database model to edit proccess
    /// </summary>
    public static class EditDatabaseModelService
    {
        private static IDatabaseModel _databaseModelToEdit;

        /// <summary>
        /// Return database model to edit
        /// </summary>
        /// <returns></returns>
        public static IDatabaseModel GetDatabaseModelToEdit()
        {
            return _databaseModelToEdit;
        }

        /// <summary>
        /// Set database model to edit
        /// </summary>
        /// <param name="databaseModel"></param>
        public static void SetDatabaseModelToEdit(IDatabaseModel databaseModel)
        {
            _databaseModelToEdit = databaseModel;
        }

        /// <summary>
        /// Check if is set database model to edit
        /// </summary>
        /// <returns></returns>
        public static bool IsModelToEdit()
        {
            if (_databaseModelToEdit == null)
                return false;
            return true;
        }
    }
}