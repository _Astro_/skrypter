﻿using System.Data.Entity;
using System.Linq;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository class of SQLite database model
    /// </summary>
    public class SQLiteDatabaseModelRepository :
        Repository<SQLiteDatabaseModel>, ISQLiteDatabaseModelRepository
    {
        public SQLiteDatabaseModelRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Check if SQLite database model already exists in repository
        /// </summary>
        /// <param name="sqliteDatabaseModel"></param>
        /// <returns></returns>
        public bool CheckIfExists(SQLiteDatabaseModel sqliteDatabaseModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == sqliteDatabaseModel.ClientName &&
                d.DatabaseName == sqliteDatabaseModel.DatabaseName &&
                d.Password == sqliteDatabaseModel.Password &&
                d.DatabasePath == sqliteDatabaseModel.DatabasePath).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Check if SQLite database model already exists in repository
        /// using all optional fields
        /// </summary>
        /// <param name="sqliteDatabaseModel"></param>
        /// <returns></returns>
        public bool CheckIfExistsWithOptionalFields(SQLiteDatabaseModel sqliteDatabaseModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == sqliteDatabaseModel.ClientName &&
                d.DatabaseName == sqliteDatabaseModel.DatabaseName &&
                d.DatabaseAliasName == sqliteDatabaseModel.DatabaseAliasName &&
                d.Password == sqliteDatabaseModel.Password &&
                d.DatabasePath == sqliteDatabaseModel.DatabasePath).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }
    }
}