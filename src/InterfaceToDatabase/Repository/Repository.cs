﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository pattern class
    /// http://www.codeproject.com/Articles/615499/Models-POCO-Entity-Framework-and-Data-Patterns
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : class
    {
        private DbContext _dbContext { get; set; }
        private DbSet<T> _dbSet { get; set; }

        public Repository(DbContext dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = this._dbContext.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {
            return this._dbSet;
        }

        public virtual T GetById(string id)
        {
            return this._dbSet.Find(id);
        }

        public virtual IQueryable<T> GetByExpressions(Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
            {
                return this._dbSet.AsQueryable().Where(predicate);
            }
            return this._dbSet.AsQueryable();
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = this._dbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                this._dbSet.Add(entity);
            }
        }

        public virtual void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = this._dbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                this._dbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = this._dbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                this._dbSet.Attach(entity);
                this._dbSet.Remove(entity);
            }
        }

        public virtual void Delete(string id)
        {
            var entity = this.GetById(id);
            if (entity != null)
                this.Delete(entity);
        }
    }
}