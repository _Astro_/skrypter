﻿namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// SQL Executor unit of work interface
    /// </summary>
    public interface IUnitOfWork
    {
        void Commit();

        void Dispose();

        IMSSQLDatabaseModelRepository MSSQLDatabaseModel { get; }

        ISSRSModelRepository SSRSModelRepository { get; }

        ISQLiteDatabaseModelRepository SQLiteDatabaseModel { get; }

        ISettingsModelRepository SettingsModel { get; }
    }
}