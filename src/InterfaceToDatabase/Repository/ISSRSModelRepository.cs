﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Interface of SSRS model repository
    /// </summary>
    public interface ISSRSModelRepository : IRepository<SSRSModel>
    {
        bool CheckIfExists(SSRSModel ssrsModel);
        bool CheckIfExistsWithOptionalFields(SSRSModel ssrsModel);
    }
}
