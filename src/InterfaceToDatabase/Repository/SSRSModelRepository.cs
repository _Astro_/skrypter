﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository class of SSRS Model
    /// </summary>
    public class SSRSModelRepository : Repository<SSRSModel>, ISSRSModelRepository
    {
        public SSRSModelRepository(DbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Check if SSRS model already exists in repository
        /// </summary>
        /// <param name="ssrsModel"></param>
        /// <returns></returns>
        public bool CheckIfExists(SSRSModel ssrsModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == ssrsModel.ClientName &&
                d.ServerAddress == ssrsModel.ServerAddress &&
                d.DatabaseName == ssrsModel.DatabaseName &&
                d.UserName == ssrsModel.UserName &&
                d.Password == ssrsModel.Password
                ).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Check if SSRS model already exists in repository
        /// using all optional fields
        /// </summary>
        /// <param name="ssrsModel"></param>
        /// <returns></returns>
        public bool CheckIfExistsWithOptionalFields(SSRSModel ssrsModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == ssrsModel.ClientName &&
                d.ServerAddress == ssrsModel.ServerAddress &&
                d.DatabaseName == ssrsModel.DatabaseName &&
                d.DatabaseAliasName == ssrsModel.DatabaseAliasName &&
                d.UserName == ssrsModel.UserName &&
                d.Password == ssrsModel.Password
                ).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }
    }
}
