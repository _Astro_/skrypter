﻿namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Interface of SQLite Database model repository
    /// </summary>
    public interface ISQLiteDatabaseModelRepository : IRepository<SQLiteDatabaseModel>
    {
        bool CheckIfExists(SQLiteDatabaseModel sqliteDatabaseModel);

        bool CheckIfExistsWithOptionalFields(SQLiteDatabaseModel sqliteDatabaseModel);
    }
}