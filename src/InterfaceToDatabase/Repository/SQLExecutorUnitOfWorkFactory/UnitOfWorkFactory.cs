﻿namespace InterfaceToDatabase.Repository.SQLExecutorUnitOfWorkFactory
{
    public static class UnitOfWorkFactory
    {
        private static IUnitOfWork _customUnitOfWork;

        public static IUnitOfWork GetUnitOfWork(string connectionString)
        {
            if (_customUnitOfWork != null)
                return _customUnitOfWork;

            return new UnitOfWork(connectionString);
        }

        public static void SetUnitOfWork(IUnitOfWork unitOfWork)
        {
            _customUnitOfWork = unitOfWork;
        }
    }
}