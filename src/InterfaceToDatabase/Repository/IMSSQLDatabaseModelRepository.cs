﻿namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Interface of MS SQL Server Database model repository
    /// </summary>
    public interface IMSSQLDatabaseModelRepository : IRepository<MSSQLDatabaseModel>
    {
        bool CheckIfExists(MSSQLDatabaseModel mssqlDatabaseModel);

        bool CheckIfExistsWithOptionalFields(MSSQLDatabaseModel mssqlDatabaseModel);
    }
}