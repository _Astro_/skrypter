﻿using System.Data.Entity;
using System.Linq;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository class of MSSQL Server database model
    /// </summary>
    public class MSSQLDatabaseModelRepository
        : Repository<MSSQLDatabaseModel>, IMSSQLDatabaseModelRepository
    {
        public MSSQLDatabaseModelRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Check if MSSQL database model already exists in repository
        /// </summary>
        /// <param name="mssqlDatabaseModel"></param>
        /// <returns></returns>
        public bool CheckIfExists(MSSQLDatabaseModel mssqlDatabaseModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == mssqlDatabaseModel.ClientName &&
                d.ServerAddress == mssqlDatabaseModel.ServerAddress &&
                d.DatabaseName == mssqlDatabaseModel.DatabaseName &&
                d.UserName == mssqlDatabaseModel.UserName &&
                d.Password == mssqlDatabaseModel.Password
                ).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Check if MSSQL database model already exists in repository
        /// using all optional fields
        /// </summary>
        /// <param name="mssqlDatabaseModel"></param>
        /// <returns></returns>
        public bool CheckIfExistsWithOptionalFields(MSSQLDatabaseModel mssqlDatabaseModel)
        {
            var databaseModel = this.GetByExpressions(
                d => d.ClientName == mssqlDatabaseModel.ClientName &&
                d.ServerAddress == mssqlDatabaseModel.ServerAddress &&
                d.DatabaseName == mssqlDatabaseModel.DatabaseName &&
                d.DatabaseAliasName == mssqlDatabaseModel.DatabaseAliasName &&
                d.UserName == mssqlDatabaseModel.UserName &&
                d.Password == mssqlDatabaseModel.Password &&
                d.ConnectionTimeout == mssqlDatabaseModel.ConnectionTimeout
                ).ToList();

            if (databaseModel.Count == 0)
                return false;
            else
                return true;
        }
    }
}