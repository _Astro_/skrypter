﻿using InterfaceToDatabase.SQLExecutor;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Interface of settings model repository
    /// </summary>
    public interface ISettingsModelRepository : IRepository<SettingsModel>
    {
    }
}