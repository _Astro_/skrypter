﻿using System;
using System.Linq;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository pattern interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        T GetById(string id);

        IQueryable<T> GetByExpressions(System.Linq.Expressions.Expression<Func<T, bool>> predicate);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(string id);
    }
}