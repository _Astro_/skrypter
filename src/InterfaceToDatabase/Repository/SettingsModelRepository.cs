﻿using InterfaceToDatabase.SQLExecutor;
using System.Data.Entity;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// Repository of settings model
    /// </summary>
    public class SettingsModelRepository : Repository<SettingsModel>, ISettingsModelRepository
    {
        public SettingsModelRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}