﻿using InterfaceToDatabase.DAL;

namespace InterfaceToDatabase.Repository
{
    /// <summary>
    /// SQL Executor unit of work class
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private EFDatabaseContext _efDatabaseContext;
        private IMSSQLDatabaseModelRepository _mssqlDatabaseModel;
        private ISSRSModelRepository _ssrsModelRepository;
        private ISQLiteDatabaseModelRepository _sqliteDatabaseModel;
        private ISettingsModelRepository _settingsModel;

        /// <summary>
        /// Default ctor
        /// </summary>
        /// <param name="connectionString"> Database connection string </param>
        public UnitOfWork(string connectionString)
        {
            CreateDatabaseContext(connectionString);
        }

        #region IMSSQLDatabaseModelUnitOfWork

        public IMSSQLDatabaseModelRepository MSSQLDatabaseModel
        {
            get
            {
                if (_mssqlDatabaseModel == null)
                    _mssqlDatabaseModel = new MSSQLDatabaseModelRepository(_efDatabaseContext);
                return _mssqlDatabaseModel;
            }
        }

        public ISSRSModelRepository SSRSModelRepository
        {
            get
            {
                if (_ssrsModelRepository == null)
                    _ssrsModelRepository = new SSRSModelRepository(_efDatabaseContext);
                return _ssrsModelRepository;
            }
        }

        public ISQLiteDatabaseModelRepository SQLiteDatabaseModel
        {
            get
            {
                if (_sqliteDatabaseModel == null)
                    _sqliteDatabaseModel = new SQLiteDatabaseModelRepository(_efDatabaseContext);
                return _sqliteDatabaseModel;
            }
        }

        public ISettingsModelRepository SettingsModel
        {
            get
            {
                if (_settingsModel == null)
                    _settingsModel = new SettingsModelRepository(_efDatabaseContext);
                return _settingsModel;
            }
        }

        public void Commit()
        {
            _efDatabaseContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_efDatabaseContext != null)
                    _efDatabaseContext.Dispose();
            }
        }

        #endregion IMSSQLDatabaseModelUnitOfWork

        /// <summary>
        /// Create database context using connection string
        /// </summary>
        /// <param name="connectionString"> Input connection string </param>
        private void CreateDatabaseContext(string connectionString)
        {
            _efDatabaseContext = new EFDatabaseContext(connectionString);
        }
    }
}