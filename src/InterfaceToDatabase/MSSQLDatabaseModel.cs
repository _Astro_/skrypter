﻿using BaseMVVM;
using System.ComponentModel.DataAnnotations.Schema;
using Tools;

namespace InterfaceToDatabase
{
    /// <summary>
    /// Microsoft SQL Database Model class
    /// </summary>
    public class MSSQLDatabaseModel : ObservableObject, IDatabaseModel
    {
        private DatabaseType _databaseType;
        private string _id;
        private string _databaseName;
        private string _databaseAliasName;
        private int _connectionTimeout;
        private const int DEFAULT_CONNECTION_TIMEOUT_VALUE = 1;
        private bool? _isSelected;

        #region IDatabaseModel

        public DatabaseType DatabaseType
        {
            get
            {
                return _databaseType;
            }
            set
            {
                _databaseType = value;
            }
        }

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string ClientName { get; set; }

        public string ServerAddress { get; set; }

        public string DatabaseName
        {
            get
            {
                return _databaseName;
            }
            set
            {
                _databaseName = value;
            }
        }

        public string DatabaseAliasName
        {
            get
            {
                if (string.IsNullOrEmpty(_databaseAliasName))
                    return _databaseName;
                return _databaseAliasName;
            }
            set
            {
                _databaseAliasName = value;
            }
        }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string DatabasePath { get; set; }

        public int ConnectionTimeout
        {
            get
            {
                return _connectionTimeout;
            }
            set
            {
                _connectionTimeout = value;
            }
        }

        [NotMapped]
        public bool? IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                Set(ref _isSelected, value);
            }
        }

        #endregion IDatabaseModel

        public MSSQLDatabaseModel()
        {
            _databaseType = DatabaseType.MSSQL;
            _id = IdGenerator.GenerateStringId();
            _connectionTimeout = DEFAULT_CONNECTION_TIMEOUT_VALUE;
            _isSelected = false;
        }
    }
}