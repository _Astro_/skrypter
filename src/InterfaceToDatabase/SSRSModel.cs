﻿using BaseMVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace InterfaceToDatabase
{
    /// <summary>
    /// MS SQL Server Reporting Service model
    /// </summary>
    public class SSRSModel : ObservableObject, IDatabaseModel
    {
        private DatabaseType _databaseType;
        private string _id;
        private string _databaseName;
        private string _databaseAliasName;
        private bool? _isSelected;

        #region IDatabaseModel

        public DatabaseType DatabaseType
        {
            get
            {
                return _databaseType;
            }
            set
            {
                _databaseType = value;
            }
        }

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string ClientName { get; set; }

        public string ServerAddress { get; set; }

        public string DatabaseName
        {
            get
            {
                return _databaseName;
            }
            set
            {
                _databaseName = value;
            }
        }

        public string DatabaseAliasName
        {
            get
            {
                if (string.IsNullOrEmpty(_databaseAliasName))
                    return _databaseName;
                return _databaseAliasName;
            }
            set
            {
                _databaseAliasName = value;
            }
        }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string DatabasePath { get; set; }

        public int ConnectionTimeout { get; set; }

        [NotMapped]
        public bool? IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                Set(ref _isSelected, value);
            }
        }

        #endregion IDatabaseModel

        public SSRSModel()
        {
            _databaseType = DatabaseType.SSRS;
            _id = IdGenerator.GenerateStringId();
            _isSelected = false;
        }
    }
}
