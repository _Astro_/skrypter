﻿using System.ComponentModel;

namespace InterfaceToDatabase
{
    /// <summary>
    /// Enumeration of repository database model tables
    /// </summary>
    public enum RepositoryDatabaseTable
    {
        [Description("MSSQLDatabaseModelTable")]
        MSSQLDatabaseModelTable,

        [Description("SSRSModelTable")]
        SSRSModelTable,

        [Description("SQLiteDatabaseModelTable")]
        SQLiteDatabaseModelTable,

        [Description("SettingsModelTable")]
        SettingsModelTable
    }
}