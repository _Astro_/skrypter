﻿namespace SQLExecutor.Tests.UnitTests
{
    //[TestClass]
    //public class DatabaseCollectionViewModelTests
    //{
    //    private const string MENU_ITEMS_COLLECTION = "MenuItemsCollection";
    //    private const string TOOLBOX_BUTTON_LIST = "ToolboxButtonList";
    //    private const string ACTION_EXPANDER_LIST = "ActionExpanderList";
    //    private const string CLIENT_DATABASE_CONTROL_BUTTON_LIST = "ClientDatabaseControlButtonList";
    //    private const string CLIENT_DATABASE_COLLECTION_LIST = "ClientDatabaseCollectionList";
    //    private bool _isPropertyChanged;
    //    private const string FAKE_HEADER = "FakeHeader";

    //    [TestInitialize]
    //    public void TestInitialize()
    //    {
    //        IsPropertyChanged = false;
    //    }

    //    [TestCleanup]
    //    public void TestCleanup()
    //    {
    //        IsPropertyChanged = false;
    //    }

    //    [TestMethod]
    //    public void MenuItemsCollection_SetToNull_ReturnEmptyCollection()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == MENU_ITEMS_COLLECTION)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.MenuItemsCollection = GetNullValue<ObservableCollection<IBindableMenuItem>>();

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNull(databaseCollectionViewModel.MenuItemsCollection.FirstOrDefault());
    //    }

    //    [TestMethod]
    //    public void MenuItemsCollection_GetMenuItemsCollection_ReturnFakeCollection()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == MENU_ITEMS_COLLECTION)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.MenuItemsCollection = FakeMenuItemsCollection;

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNotNull(databaseCollectionViewModel.MenuItemsCollection.FirstOrDefault());
    //        Assert.IsTrue(ObjectService.CompareObjects<IBindableMenuItem>(databaseCollectionViewModel.MenuItemsCollection.FirstOrDefault(),
    //            FakeMenuItemsCollection.FirstOrDefault()));
    //    }

    //    [TestMethod]
    //    public void ToolboxButtonList_SetToNull_ReturnEmptyList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == TOOLBOX_BUTTON_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ToolboxButtonList = GetNullValue<ObservableCollection<IInterfaceButton>>();

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNull(databaseCollectionViewModel.ToolboxButtonList.FirstOrDefault());
    //    }

    //    [TestMethod]
    //    public void ToolboxButtonList_GetToolboxButtonList_ReturnFakeList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == TOOLBOX_BUTTON_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ToolboxButtonList = FakeToolboxButtonList;

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNotNull(databaseCollectionViewModel.ToolboxButtonList.FirstOrDefault());
    //        Assert.IsTrue(ObjectService.CompareObjects<IInterfaceButton>(databaseCollectionViewModel.ToolboxButtonList.FirstOrDefault(),
    //            FakeToolboxButtonList.FirstOrDefault()));
    //    }

    //    [TestMethod]
    //    public void ActionExpanderList_SetToNull_ReturnEmptyList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == ACTION_EXPANDER_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ActionExpanderList = GetNullValue<ObservableCollection<IActionExpander>>();

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNull(databaseCollectionViewModel.ActionExpanderList.FirstOrDefault());
    //    }

    //    [TestMethod]
    //    public void ActionExpanderList_GetActionExpanderList_ReturnFakeList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == ACTION_EXPANDER_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ActionExpanderList = FakeActionExpanderList;

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNotNull(databaseCollectionViewModel.ActionExpanderList.FirstOrDefault());
    //        Assert.IsTrue(ObjectService.CompareObjects<IActionExpander>(databaseCollectionViewModel.ActionExpanderList.FirstOrDefault(),
    //            FakeActionExpanderList.FirstOrDefault()));
    //    }

    //    [TestMethod]
    //    public void ClientDatabaseControlButtonList_SetToNull_ReturnEmptyList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == CLIENT_DATABASE_CONTROL_BUTTON_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ClientDatabaseControlButtonList = GetNullValue<ObservableCollection<IInterfaceButton>>();

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNull(databaseCollectionViewModel.ClientDatabaseControlButtonList.FirstOrDefault());
    //    }

    //    [TestMethod]
    //    public void ClientDatabaseControlButtonList_GetClientDatabaseControlButtonList_ReturnFakeList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == CLIENT_DATABASE_CONTROL_BUTTON_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ClientDatabaseControlButtonList = FakeClientDatabaseControlButtonList;

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNotNull(databaseCollectionViewModel.ClientDatabaseControlButtonList.FirstOrDefault());
    //        Assert.IsTrue(ObjectService.CompareObjects<IInterfaceButton>(databaseCollectionViewModel.ClientDatabaseControlButtonList.FirstOrDefault(),
    //            FakeClientDatabaseControlButtonList.FirstOrDefault()));
    //    }

    //    [TestMethod]
    //    public void ClientDatabaseCollectionList_SetToNull_ReturnEmptyList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == CLIENT_DATABASE_COLLECTION_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ClientDatabaseCollectionList = GetNullValue<ObservableCollection<IClientDatabaseCollection>>();

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNull(databaseCollectionViewModel.ClientDatabaseCollectionList.FirstOrDefault());
    //    }

    //    [TestMethod]
    //    public void ClientDatabaseCollectionList_GetClientDatabaseCollectionList_ReturnFakeList()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.PropertyChanged +=
    //            (sender, e) =>
    //            {
    //                if (e.PropertyName == CLIENT_DATABASE_COLLECTION_LIST)
    //                    IsPropertyChanged = true;
    //            };
    //        databaseCollectionViewModel.ClientDatabaseCollectionList = FakeClientDatabaseCollectionList;

    //        Assert.IsTrue(IsPropertyChanged);
    //        Assert.IsNotNull(databaseCollectionViewModel.ClientDatabaseCollectionList.FirstOrDefault());
    //        Assert.IsTrue(ObjectService.CompareObjects<IClientDatabaseCollection>(databaseCollectionViewModel.ClientDatabaseCollectionList.FirstOrDefault(),
    //            FakeClientDatabaseCollectionList.FirstOrDefault()));
    //    }

    //    [TestMethod]
    //    public void GoBack_NormalCall_ReturnTrue()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.GoBack(null);

    //        mockNavigationService.Verify(m => m.GoBack(), Times.Once);
    //    }

    //    [TestMethod]
    //    public void GoToUserSettingsPage_NormalCall_ReturnTrue()
    //    {
    //        Mock<INavigationService> mockNavigationService = new Mock<INavigationService>();
    //        Mock<IMessageBoxService> messageBoxService = new Mock<IMessageBoxService>();
    //        FakeUnitOfWork();
    //        DatabaseCollectionViewModel databaseCollectionViewModel = GetDatabaseCollectionViewModel(mockNavigationService.Object, messageBoxService.Object);

    //        databaseCollectionViewModel.GoToUserSettingsPage(null);

    //        mockNavigationService.Verify(m => m.Navigate(ResourceStrings.UserSettingsPageName), Times.Once);
    //    }

    //    private ObservableCollection<IClientDatabaseCollection> FakeClientDatabaseCollectionList
    //    {
    //        get
    //        {
    //            ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList = new ObservableCollection<IClientDatabaseCollection>();

    //            Mock<IClientDatabaseCollection> mockClientDatabaseCollection = new Mock<IClientDatabaseCollection>();
    //            mockClientDatabaseCollection.SetupAllProperties();
    //            mockClientDatabaseCollection.Setup(m => m.Header).Returns(FAKE_HEADER);

    //            clientDatabaseCollectionList.Add(mockClientDatabaseCollection.Object);

    //            return clientDatabaseCollectionList;
    //        }
    //    }

    //    private ObservableCollection<IInterfaceButton> FakeClientDatabaseControlButtonList
    //    {
    //        get
    //        {
    //            ObservableCollection<IInterfaceButton> clientDatabaseControlButtonList = new ObservableCollection<IInterfaceButton>();

    //            Mock<IInterfaceButton> mockInterfaceButton = new Mock<IInterfaceButton>();
    //            mockInterfaceButton.SetupAllProperties();
    //            mockInterfaceButton.Setup(m => m.Content).Returns(FAKE_HEADER);

    //            clientDatabaseControlButtonList.Add(mockInterfaceButton.Object);

    //            return clientDatabaseControlButtonList;
    //        }
    //    }

    //    private ObservableCollection<IActionExpander> FakeActionExpanderList
    //    {
    //        get
    //        {
    //            ObservableCollection<IActionExpander> actionExpanderList = new ObservableCollection<IActionExpander>();

    //            Mock<IActionExpander> mockActionExpander = new Mock<IActionExpander>();
    //            mockActionExpander.SetupAllProperties();
    //            mockActionExpander.Setup(m => m.Header).Returns(FAKE_HEADER);

    //            actionExpanderList.Add(mockActionExpander.Object);

    //            return actionExpanderList;
    //        }
    //    }

    //    private ObservableCollection<IInterfaceButton> FakeToolboxButtonList
    //    {
    //        get
    //        {
    //            ObservableCollection<IInterfaceButton> toolboxButtonList = new ObservableCollection<IInterfaceButton>();

    //            Mock<IInterfaceButton> mockInterfaceButton = new Mock<IInterfaceButton>();
    //            mockInterfaceButton.SetupAllProperties();
    //            mockInterfaceButton.Setup(m => m.Content).Returns(FAKE_HEADER);

    //            toolboxButtonList.Add(mockInterfaceButton.Object);

    //            return toolboxButtonList;
    //        }
    //    }

    //    private ObservableCollection<IBindableMenuItem> FakeMenuItemsCollection
    //    {
    //        get
    //        {
    //            ObservableCollection<IBindableMenuItem> menuItemsCollection = new ObservableCollection<IBindableMenuItem>();

    //            Mock<IBindableMenuItem> mockBindableMenuItem = new Mock<IBindableMenuItem>();
    //            mockBindableMenuItem.SetupAllProperties();
    //            mockBindableMenuItem.Setup(m => m.Header).Returns(FAKE_HEADER);

    //            menuItemsCollection.Add(mockBindableMenuItem.Object);

    //            return menuItemsCollection;
    //        }
    //    }

    //    private bool IsPropertyChanged
    //    {
    //        get
    //        {
    //            return _isPropertyChanged;
    //        }
    //        set
    //        {
    //            _isPropertyChanged = value;
    //        }
    //    }

    //    private T GetNullValue<T>() where T : class
    //    {
    //        return null;
    //    }

    //    private DatabaseCollectionViewModel GetDatabaseCollectionViewModel(INavigationService navigationService, IMessageBoxService messageBoxService)
    //    {
    //        return new DatabaseCollectionViewModel(navigationService, messageBoxService);
    //    }

    //    private void FakeUnitOfWork()
    //    {
    //        Mock<IUnitOfWork> mockUnitOfWork = new Mock<IUnitOfWork>();
    //        mockUnitOfWork.SetupAllProperties();
    //        mockUnitOfWork.SetupGet(m => m.MSSQLDatabaseModel).Returns(new Mock<IMSSQLDatabaseModelRepository>().Object);
    //        mockUnitOfWork.SetupGet(m => m.SQLiteDatabaseModel).Returns(new Mock<ISQLiteDatabaseModelRepository>().Object);
    //        mockUnitOfWork.SetupGet(m => m.SettingsModel).Returns(new Mock<ISettingsModelRepository>().Object);
    //        UnitOfWorkFactory.SetUnitOfWork(mockUnitOfWork.Object);
    //    }
    //}
}