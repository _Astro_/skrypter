﻿using InterfaceToDatabase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClientData
{
    /// <summary>
    /// Interface of client database collectiom component class
    /// </summary>
    public interface IClientDatabaseCollection
    {
        string Header { get; set; }

        bool IsExpanded { get; set; }

        ObservableCollection<IDatabaseModel> ClientDatabaseList { get; set; }
    }
}
