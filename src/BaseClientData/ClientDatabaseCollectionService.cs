﻿using InterfaceToDatabase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClientData
{

    public static class ClientDatabaseCollectionService
    {
        private const int ZERO_DATABASES = 0;

        /// <summary>
        /// Return client database collection list from database model list
        /// </summary>
        /// <param name="databaseModelList"></param>
        /// <returns></returns>
        public static ObservableCollection<IClientDatabaseCollection> GetCollectionOfAllClients(List<IDatabaseModel> databaseModelList)
        {
            ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList = new ObservableCollection<IClientDatabaseCollection>();
            List<string> clientNameList = GetClientNameList(databaseModelList);

            foreach (string client in clientNameList)
            {
                clientDatabaseCollectionList.Add(GetClientDatabaseCollection(databaseModelList, client));
            }

            return clientDatabaseCollectionList;
        }

        /// <summary>
        /// Return number of current selected databases
        /// </summary>
        /// <returns></returns>
        public static int GetNumberOfSelectedDatabase(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList)
        {
            int numberOfSelectedDatabase = ZERO_DATABASES;

            foreach (IClientDatabaseCollection clientDatabaseCollection in clientDatabaseCollectionList)
            {
                var selectedNumber = (from database in clientDatabaseCollection.ClientDatabaseList
                                      where database.IsSelected == true
                                      select database).ToList().Count;
                numberOfSelectedDatabase += selectedNumber;
            }

            return numberOfSelectedDatabase;
        }

        /// <summary>
        /// Return first selected database model from view
        /// </summary>
        /// <returns></returns>
        public static IDatabaseModel GetFirstSelectedDatabaseModel(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList)
        {
            foreach (IClientDatabaseCollection clientDatabaseCollection in clientDatabaseCollectionList)
            {
                var databaseModel = (from database in clientDatabaseCollection.ClientDatabaseList
                                     where database.IsSelected == true
                                     select database).FirstOrDefault();

                if (databaseModel != null)
                    return databaseModel;
            }
            return null;
        }

        /// <summary>
        /// Returl list of selected databases
        /// </summary>
        /// <param name="clientDatabaseCollectionList"></param>
        /// <returns></returns>
        public static List<IDatabaseModel> GetListOfSelectedDatabase(ObservableCollection<IClientDatabaseCollection> clientDatabaseCollectionList)
        {
            List<IDatabaseModel> databaseModelList = new List<IDatabaseModel>();

            foreach (IClientDatabaseCollection clientDatabaseCollection in clientDatabaseCollectionList)
            {
                var listOfSelected = (from database in clientDatabaseCollection.ClientDatabaseList
                                      where database.IsSelected == true
                                      select database).ToList();

                if (listOfSelected != null)
                    databaseModelList.AddRange(listOfSelected);
            }

            return databaseModelList;
        }

        /// <summary>
        /// Return client database collection from list based on client name
        /// </summary>
        /// <param name="databaseModelList"></param>
        /// <param name="clientName"></param>
        /// <returns></returns>
        private static IClientDatabaseCollection GetClientDatabaseCollection(List<IDatabaseModel> databaseModelList, string clientName)
        {
            var clientDatabaseList = (from databaseModel in databaseModelList
                                      orderby databaseModel.DatabaseType, // All database
                                      databaseModel.DatabaseAliasName, // All database
                                      databaseModel.ServerAddress, // MSSQL database
                                      databaseModel.DatabasePath // SQLite database
                                      where databaseModel.ClientName == clientName
                                      select databaseModel).ToList();

            IClientDatabaseCollection clientDatabaseCollection = new ClientDatabaseCollection();
            clientDatabaseCollection.ClientDatabaseList = new ObservableCollection<IDatabaseModel>(clientDatabaseList);
            clientDatabaseCollection.Header = clientName;
            clientDatabaseCollection.IsExpanded = false;

            return clientDatabaseCollection;
        }

        /// <summary>
        /// Return all client names from database model list
        /// </summary>
        /// <param name="databaseModelList"></param>
        /// <returns></returns>
        private static List<string> GetClientNameList(List<IDatabaseModel> databaseModelList)
        {
            var clientNameList = (from model in databaseModelList
                                  orderby model.ClientName
                                  select model.ClientName).Distinct().ToList();

            return clientNameList;
        }
    }
}
