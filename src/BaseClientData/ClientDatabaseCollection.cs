﻿using BaseMVVM;
using InterfaceToDatabase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClientData
{
    /// <summary>
    /// Client database collection model class
    /// </summary>
    public class ClientDatabaseCollection : ObservableObject, IClientDatabaseCollection
    {
        private bool _isExpanded;

        private ObservableCollection<IDatabaseModel> _clientDatabaseList;

        public string Header { get; set; }

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                Set(ref _isExpanded, value);
            }
        }

        public ObservableCollection<IDatabaseModel> ClientDatabaseList
        {
            get
            {
                if (_clientDatabaseList == null)
                    _clientDatabaseList = new ObservableCollection<IDatabaseModel>();
                return _clientDatabaseList;
            }
            set
            {
                Set(ref _clientDatabaseList, value);
            }
        }
    }
}
