﻿namespace BaseMVVM
{
    /// <summary>
    /// Additional interface for view class
    /// </summary>
    public interface IClosable
    {
        void Close();
    }
}