﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BaseMVVM
{
    /// <summary>
    /// Implementation of INotifyPropertyChanged interface
    /// </summary>
    public class ObservableObject : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        // Event occurs when property value is changes
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise property change event
        /// </summary>
        /// <param name="propertyName"> Property name </param>
        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged

        /// <summary>
        /// Update property if value is changed
        /// </summary>
        /// <typeparam name="T"> Property type </typeparam>
        /// <param name="propertyName"> Property name </param>
        /// <param name="oldValue"> Old value of property </param>
        /// <param name="newValue"> New value of property </param>
        /// <returns> Return true if value changed </returns>
        public virtual bool Set<T>(string propertyName, ref T oldValue, T newValue)
        {
            if (Equals(oldValue, newValue)) return false;

            oldValue = newValue;
            RaisePropertyChanged(propertyName);

            return true;
        }

        /// <summary>
        /// Update property if value is changed
        /// </summary>
        /// <typeparam name="T"> Property type </typeparam>
        /// <param name="oldValue"> Old value of property </param>
        /// <param name="newValue"> New value of property </param>
        /// <param name="propertyName"> Property name </param>
        /// <returns> Return true if value changed </returns>
        public bool Set<T>(ref T oldValue, T newValue, [CallerMemberName] string propertyName = null)
        {
            return Set(propertyName, ref oldValue, newValue);
        }
    }
}