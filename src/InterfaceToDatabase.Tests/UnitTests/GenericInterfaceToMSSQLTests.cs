﻿using InterfaceToDatabase.InterfceToDatabaseFactory;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data;

namespace InterfaceToDatabase.Tests.UnitTests
{
    [TestClass]
    public class GenericInterfaceToMSSQLTests : GenericInterfaceToDatabaseTests<InterfaceToMSSQL>
    {
        protected override IDatabaseModel ValidDatabaseModel
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutClientName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, string.Empty)
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutServerAddress
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, string.Empty)
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabaseName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, string.Empty)
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabaseAliasName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutUserName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, string.Empty)
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutPassword
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, string.Empty);

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabasePath
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutConnectionTimeout
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.ServerAddress, "ServerAddress")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.UserName, "UserName")
                    .SetupProperty(m => m.Password, "Password");

                return mockDatabaseModel.Object;
            }
        }

        protected override bool IsClientNameImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsServerAddressImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsDatabaseNameImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsDatabaseAliasNameImportant
        {
            get
            {
                return false;
            }
        }

        protected override bool IsUserNameImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsPasswordImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsDatabasePathImportant
        {
            get
            {
                return false;
            }
        }

        protected override bool IsConnectionTimeoutImportant
        {
            get
            {
                return false;
            }
        }

        protected override string VALID_CONNECTION_STRING
        {
            get
            {
                return "Server=ServerAddress;User ID=UserName;Password=Password;Database=DatabaseName;Connection Timeout=1";
            }
        }

        protected override IDbConnection DbConnection
        {
            get
            {
                Mock<IDbConnection> mockDbConnection = new Mock<IDbConnection>();
                mockDbConnection.SetupAllProperties();
                //mockDbConnection.SetupGet(mm => mm.State).Returns(() => ConnectionState.Open);
                return mockDbConnection.Object;
            }
        }

        protected override ISmoServer SmoServer
        {
            get
            {
                Mock<ISmoServer> mockSmoServer = new Mock<ISmoServer>();
                mockSmoServer.SetupAllProperties();
                mockSmoServer.SetupProperty(m => m.Server, new Server());
                return mockSmoServer.Object;
            }
        }
    }
}