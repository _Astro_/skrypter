﻿using ErrorsAndExceptions.AbstractServices;
using InterfaceToDatabase.InterfceToDatabaseFactory;
using InterfaceToDatabase.InterfceToDatabaseFactory.DbConnectFactory;
using InterfaceToDatabase.InterfceToDatabaseFactory.MSSQLSmoServerFactory;
using LocalizationData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;

namespace InterfaceToDatabase.Tests.UnitTests
{
    /// <summary>
    /// Generic abstract interface to database base test class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class GenericInterfaceToDatabaseTests<T> where T : AResultService, IInterfaceToDatabase
    {
        protected abstract IDatabaseModel ValidDatabaseModel { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutClientName { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutServerAddress { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutDatabaseName { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutDatabaseAliasName { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutUserName { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutPassword { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutDatabasePath { get; }
        protected abstract IDatabaseModel DatabaseModelWithoutConnectionTimeout { get; }
        protected abstract bool IsClientNameImportant { get; }
        protected abstract bool IsServerAddressImportant { get; }
        protected abstract bool IsDatabaseNameImportant { get; }
        protected abstract bool IsDatabaseAliasNameImportant { get; }
        protected abstract bool IsUserNameImportant { get; }
        protected abstract bool IsPasswordImportant { get; }
        protected abstract bool IsDatabasePathImportant { get; }
        protected abstract bool IsConnectionTimeoutImportant { get; }
        protected abstract string VALID_CONNECTION_STRING { get; }
        protected abstract IDbConnection DbConnection { get; }
        protected abstract ISmoServer SmoServer { get; }
        public const IDatabaseModel NULL_DATABASE_MODEL = null;
        public const string EMPTY_STRING = "";
        public const string NULL_STRING = null;
        public string NULL_DATABASE_OBJECT = ResourceStrings.InvalidDatabaseModelObjectNull;

        protected T GetInterfaceToDatabase()
        {
            return (T)Activator.CreateInstance(typeof(T));
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            AResultService.ClearErrorsAndExceptions();
            ClearFactories();
        }

        [TestMethod]
        public void GetConnectionString_NullModelClass_ReturnEmptyString()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();

            var resultString = interfaceToDatabase.GetConnectionString(NULL_DATABASE_MODEL);

            Assert.AreEqual(EMPTY_STRING, resultString);
        }

        [TestMethod]
        public void GetConnectionString_NullModelClass_ReportExceptionString()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel databaseModel = NULL_DATABASE_MODEL;

            interfaceToDatabase.GetConnectionString(databaseModel);
            var actualExceptionString = interfaceToDatabase.GetException();

            Assert.AreNotEqual(EMPTY_STRING, actualExceptionString);
        }

        [TestMethod]
        public void GetConnectionString_NullModelClass_ReportNullObjectString()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel databaseModel = NULL_DATABASE_MODEL;

            interfaceToDatabase.GetConnectionString(databaseModel);
            var actualExceptionString = interfaceToDatabase.GetException();

            Assert.AreEqual(NULL_DATABASE_OBJECT, actualExceptionString);
        }

        [TestMethod]
        public void GetConnectionString_NoClientName_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutClientName;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsClientNameImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoServerAddress_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutServerAddress;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsServerAddressImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoDatabaseName_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutDatabaseName;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsDatabaseNameImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoDatabaseAliasName_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutDatabaseAliasName;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsDatabaseAliasNameImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoUserName_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutUserName;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsUserNameImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoPassword_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutPassword;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsPasswordImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoDatabasePath_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutDatabasePath;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsDatabasePathImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_NoConnectionTimeout_CheckIsRequired()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = DatabaseModelWithoutConnectionTimeout;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(IsConnectionTimeoutImportant, string.IsNullOrEmpty(resultString));
        }

        [TestMethod]
        public void GetConnectionString_ValidModel_ReturnValidConnectionString()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            IDatabaseModel mockDatabaseModel = ValidDatabaseModel;

            var resultString = interfaceToDatabase.GetConnectionString(mockDatabaseModel);

            Assert.AreEqual(VALID_CONNECTION_STRING, resultString);
        }

        [TestMethod]
        public void OpenConnection_EmptyConnectionString_ReturnFalse()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();

            var openConnectionResult = interfaceToDatabase.OpenConnection(EMPTY_STRING);

            Assert.IsFalse(openConnectionResult);
        }

        [TestMethod]
        public void OpenConnection_NullConnectionString_ReturnFalse()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();

            var openConnectionResult = interfaceToDatabase.OpenConnection(NULL_STRING);

            Assert.IsFalse(openConnectionResult);
        }

        //[TestMethod]
        //public void OpenConnection_FakeDbConnectAndServer_ReturnTrue()
        //{
        //    T interfaceToDatabase = GetInterfaceToDatabase();
        //    string anyConnectionString = "AnyConnectionString";
        //    var mockDbConnection = DbConnection;
        //    // Fake DbConnect
        //    DbConnectionFactory.SetDbConnection(mockDbConnection);
        //    var mockSmoServer = SmoServer;
        //    // Fake Server
        //    SmoServerFactory.SetSmoServer(mockSmoServer);

        //    var openConnectionResult = interfaceToDatabase.OpenConnection(anyConnectionString);

        //    Assert.IsTrue(openConnectionResult);
        //}

        [TestMethod]
        public void OpenConnection_InvalidConnectionString_ReturnFalse()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            string anyConnectionString = "InvalidConnectionString";

            var openConnectionResult = interfaceToDatabase.OpenConnection(anyConnectionString);

            Assert.IsFalse(openConnectionResult);
        }

        [TestMethod]
        public void OpenConnection_ConnectionStringToNotExistingDatabase_ReturnFalse()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            string validConnectionString = VALID_CONNECTION_STRING;

            var openConnectionResult = interfaceToDatabase.OpenConnection(validConnectionString);

            Assert.IsFalse(openConnectionResult);
        }

        [TestMethod]
        public void OpenConnection_ConnectionStringToNotExistingDatabase_ReportException()
        {
            T interfaceToDatabase = GetInterfaceToDatabase();
            string validConnectionString = VALID_CONNECTION_STRING;

            interfaceToDatabase.OpenConnection(validConnectionString);
            var actualException = interfaceToDatabase.GetException();

            Assert.IsFalse(string.IsNullOrEmpty(actualException));
        }

        private void ClearFactories()
        {
            DbConnectionFactory.SetDbConnection(null);
            SmoServerFactory.SetSmoServer(null);
        }
    }
}