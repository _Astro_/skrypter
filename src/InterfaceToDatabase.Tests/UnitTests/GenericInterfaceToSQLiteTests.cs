﻿using InterfaceToDatabase.InterfceToDatabaseFactory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data;

namespace InterfaceToDatabase.Tests.UnitTests
{
    [TestClass]
    public class GenericInterfaceToSQLiteTests : GenericInterfaceToDatabaseTests<InterfaceToSQLite>
    {
        protected override IDatabaseModel ValidDatabaseModel
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutClientName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, string.Empty)
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutServerAddress
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabaseName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, string.Empty)
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabaseAliasName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutUserName
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutPassword
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, string.Empty)
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutDatabasePath
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, string.Empty);

                return mockDatabaseModel.Object;
            }
        }

        protected override IDatabaseModel DatabaseModelWithoutConnectionTimeout
        {
            get
            {
                Mock<IDatabaseModel> mockDatabaseModel = new Mock<IDatabaseModel>();
                mockDatabaseModel.SetupProperty(m => m.ClientName, "ClientName")
                    .SetupProperty(m => m.DatabaseName, "DatabaseName")
                    .SetupProperty(m => m.Password, "Password")
                    .SetupProperty(m => m.DatabasePath, "DatabasePath");

                return mockDatabaseModel.Object;
            }
        }

        protected override bool IsClientNameImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsServerAddressImportant
        {
            get
            {
                return false;
            }
        }

        protected override bool IsDatabaseNameImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsDatabaseAliasNameImportant
        {
            get
            {
                return false;
            }
        }

        protected override bool IsUserNameImportant
        {
            get
            {
                return false;
            }
        }

        protected override bool IsPasswordImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsDatabasePathImportant
        {
            get
            {
                return true;
            }
        }

        protected override bool IsConnectionTimeoutImportant
        {
            get
            {
                return false;
            }
        }

        protected override string VALID_CONNECTION_STRING
        {
            get
            {
                return "data source=DatabasePath\\DatabaseName;password=Password;version=3";
            }
        }

        protected override IDbConnection DbConnection
        {
            get
            {
                Mock<IDbConnection> mockDbConnection = new Mock<IDbConnection>();
                mockDbConnection.SetupAllProperties();
                return mockDbConnection.Object;
            }
        }

        protected override ISmoServer SmoServer
        {
            get
            {
                return null;
            }
        }
    }
}