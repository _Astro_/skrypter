﻿using System;
using System.Windows;

namespace WPFViewService
{
    /// <summary>
    /// WPF view service class
    /// </summary>
    public static class ViewService
    {
        private static WindowCollection _windowCollection;

        /// <summary>
        /// Focus on owner of T view class instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void FocusOnViewOwner<T>()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                _windowCollection = Application.Current.Windows;

                if (_windowCollection != null && _windowCollection.Count > 0)
                {
                    foreach (Window window in _windowCollection)
                    {
                        if (window is T)
                        {
                            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                window.Owner.Focus();
                            }));
                        }
                    }
                }
            }));
        }

        /// <summary>
        /// Focus on T view class instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void FocusOnView<T>()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                _windowCollection = Application.Current.Windows;

                if (_windowCollection != null && _windowCollection.Count > 0)
                {
                    foreach (Window window in _windowCollection)
                    {
                        if (window is T)
                        {
                            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                window.Focus();
                            }));
                        }
                    }
                }
            }));
        }
    }
}