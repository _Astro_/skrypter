﻿using ErrorsAndExceptions;
using InterfaceToDatabase;
using LocalizationData;
using Scripts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports
{
    /// <summary>
    /// Report model service class
    /// </summary>
    public static class ReportModelService
    {
        /// <summary>
        /// Execute report on database
        /// </summary>
        /// <param name="databaseModel"></param>
        /// <param name="scriptModel"></param>
        /// <returns></returns>
        public static bool ExecuteReport(IDatabaseModel databaseModel, IScriptModel scriptModel)
        {
            var executionString = BuildExecutionString(databaseModel, scriptModel);

            if (!string.IsNullOrEmpty(executionString))
            {
                var processResult = string.Empty;

                ProcessStartInfo processStartInfo = new ProcessStartInfo();
                SetProcessStartInfoParms(processStartInfo, executionString);

                Process process = new Process();
                process.StartInfo = processStartInfo;
                bool processStarted = process.Start();

                if (processStarted)
                {
                    process.StandardInput.Flush();
                    process.StandardInput.Close();
                    processResult += process.StandardError.ReadToEnd();
                    process.WaitForExit();

                    if (processResult != string.Empty)
                        return ErrorsAndExceptionsService.Instance.ReportFail(processResult);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Set parameters for process
        /// </summary>
        /// <param name="processStartInfo"></param>
        /// <param name="connectionCommand"></param>
        private static void SetProcessStartInfoParms(ProcessStartInfo processStartInfo, string connectionCommand)
        {
            processStartInfo.Arguments = connectionCommand;
            processStartInfo.CreateNoWindow = true;
            processStartInfo.FileName = ResourceStrings.ReportServerExecName;
            processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.RedirectStandardInput = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;
        }

        /// <summary>
        /// Return execution string from SSRS model
        /// </summary>
        /// <param name="databaseModel"> Input database model </param>
        /// <returns></returns>
        private static string BuildExecutionString(IDatabaseModel databaseModel, IScriptModel scriptModel)
        {
            if (databaseModel == null || databaseModel.DatabaseType != DatabaseType.SSRS) return string.Empty;

            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format(@"-i ""{0}{1}"" ", AppDomain.CurrentDomain.BaseDirectory, ResourceStrings.SSRSReportDeploymentFileName));
            builder.Append(string.Format(@"-s ""{0}"" ", databaseModel.ServerAddress.Replace(@"/", @"//").Replace(@"////", @"//")));
            builder.Append(string.Format(@"-u ""{0}"" ", databaseModel.UserName));
            builder.Append(string.Format(@"-p ""{0}"" ", databaseModel.Password));
            builder.Append(string.Format(@"-v filePath=""{0}"" ", new FileInfo(scriptModel.FullPath).Directory.FullName));
            builder.Append(string.Format(@"-v reportName=""{0}"" ", scriptModel.Name));
            builder.Append(string.Format(@"-v parentFolder=""{0}"" ", databaseModel.DatabaseName));

            return builder.ToString();
        }
    }
}
